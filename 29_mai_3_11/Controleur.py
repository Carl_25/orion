# -*- encoding: ISO-8859-1 -*-
from Modele import *#modele
from UniqueId import  *
from VueControleur import *
from Vaisseau import *
from Planete import *
from SystemeSolaire import *
from threading import Timer
import Pyro4
#Du controlleur serveur
import random
from subprocess import Popen
import os
import socket
import platform
import ftplib
import sys
from helper import Helper

class Controleur(object):
    def __init__(self):
        
        #Obtenir l'ip local
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        self.monip=s.getsockname()[0]
        s.close()
        
        #rand=os.urandom(8)
        self.uniqueId =UniqueID(self)
        self.serveur=0
        self.serveurList=[]
        self.nom=""#Nom du joueur, nous sert de ID
        self.cadre=0#a rapport a la boucle d'action dans le timer
        self.actions=[]#actions faites et a faire
        self.serveurLocal=0
        self.serveurIP=""
        self.ftpList=[]
        self.IDS=0
        self.listeJoueurs = []
        #couleur et civilisation des joueurs
        self.clientsParam = None
        
        #cree la vue et le modele
        self.modele=Modele(self)
        self.vue=Vue(self)
        
        #MarcAndreBrodeur - Modification
        self.inscrit = False
        
    
    def creerServeur(self):#trouve le chemin vers not python et ouvre pyro selon la version
        if (os.path.isdir("C://Python33")):
            pid = Popen(["C:\\Python33\\Python.exe", "C_orion_2014_serveur.py"]).pid
            print ("python33", os.path.isdir("C://Python33"))
        elif (os.path.isdir("C://Python32")):
            pid = Popen(["C:\\Python32\\Python.exe", "C_orion_2014_serveur.py"]).pid
            print ("python32", os.path.isdir("C://Python32"))
        elif(os.path.exists("/Library/Frameworks/Python.framework")):
            pid = Popen(["Python", "C_orion_2014_serveur.py"]).pid
            print ("pythonOSX", os.path.exists("/Library/Frameworks/Python.framework"))
        self.serveurLocal=1 # Etat du client -> Hote = 1, Client = 0
        self.enregistreServeur()
        return pid

    def enregistreServeur(self):
        #verifie si dossier serveur est present sinon le cree
        if not os.path.exists('./serveur'):
            os.makedirs('./serveur')
        #cree un fichier r�f�rence - IP serveur
        filename = "./serveur/"+self.monip              #creer le fichier
        target = open ( filename, 'w')                  #ouvrir le fichier
        target.write(self.monip)                        
        target.close()
        #changer le repertoire
        os.chdir('./serveur')
        #ouverture de session ftp sur serveur distant
        session = ftplib.FTP('ftp.fodae.info','orion@fodae.info','orion')
        file = open(self.monip,'rb')                    # fichier serveur a envoyer
        session.storbinary('STOR '+self.monip, file)    # envoie du fichier
        self.ftpList =(session.nlst())
        file.close()                                    # fermer le fichier
        session.quit()                                  # fermer la session ftp
        #remettre le repertoire par default
        os.chdir('../')
        #efface le fichier r�f�rence du serveur sur ordi local
        os.remove("./serveur/"+self.monip)
        self.serveurActif()

    def serveurActif(self):
        #obtenir les parties cree
        session = ftplib.FTP('ftp.fodae.info','orion@fodae.info','orion')
        self.ftpList =(session.nlst())
        session.quit()                                  # fermer la session ftp
        if len(self.ftpList) > 3:
            self.serveurIP = self.ftpList[3]
            i = 0
            while i < len(self.ftpList)-3:
                self.serveurList.append(self.ftpList[i+3])
                i+=1
        #afficher la list des serveurs
        return self.serveurList

    def effacerServeurListe (self):
        #effacer fichier serveur si existe sur serveur distant
        for i in self.ftpList:
            try:#si je ne peux pas supprimer mon ip, je pass
                if i == self.monip and self.serveurLocal == 1:
                    session = ftplib.FTP('ftp.fodae.info','orion@fodae.info','orion')
                    session.delete(self.monip)
                    session.quit()
            except:
                pass
 
    def getListeJoueurs(self):
        rep = self.serveur.faitAction([self.nom,self.cadre,[]])
        if rep[0]==0:
            self.modele.listeJoueurs=rep[2]
            return rep[2]
        pass
    
    def getRdseed(self):
        return self.modele.rdseed
    
    def setRdseed(self, noSeed) :
        self.modele.setRdseed(noSeed)
        
    def jeQuitte(self):
        #effacer fichier serveur si existe sur serveur distant
        self.effacerServeurListe()
        if self.serveur:#0 si je suis le server
            self.serveur.jeQuitte(self.nom)
     
    def inscritClient(self,nom,leip, civ):
        
        if not self.inscrit:
            ad="PYRO:controleurServeur@"+leip+":54440"
            self.serveur=Pyro4.core.Proxy(ad)
            Pyro4.socketutil.setReuseAddr(self.serveur)
            self.inscrit = True
        
        if self.vue.vueLogin.popupExiste == 1:
           self.vue.vueLogin.detruirePopup() 
                
        rep=self.serveur.inscritClient(nom, civ)
        if rep[0]:
            self.modele.rdseed=rep[2]
            self.nom=nom
            self.vue.creerLobby(leip)
            self.timerAttend()            
        else:
            self.vue.vueLogin.popupCorrigerNom( self, leip, civ )
            
    def serveurLocal(self):
        return self.serveurLocal#TO SEE
    
    def demarrePartie(self):#ca start quand on clique sur le bouton demarrer
        #obtenir la liste des joueurs, leur couleur et civilisation
        #dictionnaire la cle: nom du joueur: [0]: oculeur, [1]: civilisation
        self.clientsParam = self.serveur.getClientParam()
        #effacer le fichier serveur si existe sur serveur distant
        self.effacerServeurListe()
        #rep = 1 quand la partie est d�marr�e        
        rep=self.serveur.demarrePartie() 
        return rep

    def stopServeur(self):
        self.effacerServeurListe()
        rep=self.serveur.quitter()
        self.serveur=0
        input("FERMER")
        
    def timerAttend(self):
        #print ("Controleur timerAttend")
        if self.serveur:
            rep=self.serveur.faitAction([self.nom,self.cadre,[]])
            self.listeJoueurs = rep[2]
            if rep[0]:
                print(rep)
                self.modele.initPartie(rep[2][1][0][1])
                
                if self.serveurLocal==0:
                    self.vue.demarrePartie()
                #self.parent.vue.canevasEspace.bind("<Button>",self.parent.vue.changeCible)
                self.vue.root.after(10,self.timerJeu)
            elif rep[0]==0:
                #Afficher dans la vueLobby la liste des joueurs en attente
                if self.vue.vueLobbyExiste == 1:
                    self.vue.vueLobby.afficheListeJoueurs(rep[2])
                self.vue.root.after(10,self.timerAttend)
        else:
            print("Aucun serveur attache")
        
    def timerJeu(self):
        if self.serveur:
            self.cadre=self.cadre+1#augmente le cadre courant
            self.modele.prochaineAction(self.cadre)#on passe au cadre suivant avec la liste de nos actions
            self.vue.vuePartie.updateVue()#on a updat� le modele? on update la vue!
            if self.actions:#on ca chercher les actions en envoyant celles qu'on a fait
                rep=self.serveur.faitAction([self.nom,self.cadre,self.actions])
            else:#on a pas fait d'action? fine, vla 0!
                rep=self.serveur.faitAction([self.nom,self.cadre,0])
            self.actions=[]#ma liste d'actions 
            
            if rep[0]:#si on a une action en mm temps qu'un autre joueur au mm cadre, on l'append
                for i in rep[2]:#la liste d'action, c'est un diction
                    if i in self.modele.actionsAFaire.keys():
                        for k in rep[2][i]:
                            for m in k:
                                self.modele.actionsAFaire[i].append(m)
                    else:#on fait l'action
                        for k in rep[2][i]:#i c'est le cadre, m kes actions, k le joueur
                            for m in k:#on passe dans les actions du joueur, sarah a boug� un vaisseau par ex
                                self.modele.actionsAFaire[i]=[m]
                print("ACTIONS",self.cadre,"\nREP",rep,"\nACTIONAFAIRE",self.modele.actionsAFaire)  
            if rep[1]=="attend":
                self.cadre=self.cadre-1 
            self.vue.root.after(50,self.timerJeu)
        else:
            print("Aucun serveur connu")
        
    def bougerVaisseau(self,idVaisseau,xArrivee,yArrivee):
        self.actions.append([self.nom,"bougerVaisseau",[idVaisseau,xArrivee,yArrivee]])
        
    def attaqueSpecifique(self, idVaisseauEnnemi, idVaisseauAttaquant):
            self.actions.append([self.nom,"bougerVaisseau",[idVaisseauEnnemi,idVaisseauAttaquant]])
            
    def poursuivreObjet (self, idSuiveur, idSuivi):
        self.actions.append([self.nom,"poursuivreObjet",[idSuiveur, idSuivi]])
        
    def construireBatisse (self,idPlanete,batisse):
        self.actions.append([self.nom,"construireBatisse",[idPlanete,batisse,"gas"]])
        
    def queuingVaisseau (self,idPlanete,idBatisse,vaisseau):
        self.actions.append([self.nom,"queuingVaisseau",[idPlanete,idBatisse,vaisseau]])
            
if __name__ == '__main__':
    c=Controleur()
    c.vue.root.mainloop()
