# -*- coding: utf-8 -*-
###BATISSES
from Vaisseau import *
import dictBatisses
import dictVaisseaux

#to do : ajouter prochaineAction() dans toutes les batisses
#faire le dict batisse
#faire les inits du parent
class Batisse:
    def __init__(self,joueur,planete, dictionnaire):
        self.parent=planete# planete
        self.joueur=joueur
        self.joueur.parent.ids =  self.joueur.parent.unID.nextId(self.joueur.parent.ids)
        self.id = self.joueur.parent.ids
        self.coutMinerai = dictionnaire["coutMinerai"]
        self.coutGas = dictionnaire["coutGas"]
        self.coutEnergie = dictionnaire["coutEnergie"]
        self.vie = dictionnaire["vie"]
        self.tempsPourUpgrader = dictionnaire["tempsPourUpgrader"]
        self.niveau = 1
        self.coutEnergie=50
        self.upgradeValid = False
        self.dictionnaireVaisseaux = {"VaisseauLaser":VaisseauLaser,
                "VaisseauMissile":VaisseauMissile,
                "VaisseauMitraillette":VaisseauMitraillette,
                "VaisseauCargo":VaisseauCargo,
                "VaisseauCroiseur":VaisseauCroiseur,
                "VaisseauDiplomate":VaisseauDiplomate,
                "VaisseauScout":VaisseauScout,
                "VaisseauFurtif":VaisseauFurtif,
                "VaisseauSniper":VaisseauSniper,
                "VaisseauBombardier":VaisseauBombardier,
                "VaisseauAmiral":VaisseauAmiral}
        
        self.dictBatisse = {"Capitale":VaisseauAmiral,
                "StationExtraction":StationExtraction,
                "StationEnergie":StationEnergie,
                "PortSpaciale":PortSpaciale,
                "SatelliteDefense":SatelliteDefense,
                "Radar":Radar,
                "GenerateurChampForce":GenerateurChampForce,
                "Laboratoire":Laboratoire,
                "Temple":Temple,
                "Colisee":Colisee}
        

    def repair(self):
        mineraiDuJoueur = self.parent.parent.minerai#Chemin
        mineraiDuJoueur -= self.prix/4
        
    
    def sell(self):
        mineraiDuJoueur = self.parent.parent.minerai
        mineraiDuJoueur += self.prix/2
    
    def shutDown(self,etat): #un boolean
        if etat == True:
            self.parent.listeBatimensInactif.append()
            self.parent.listeBatiment.remove()
        else:            
            self.parent.listeBatiment.append()
            self.parent.listeBatimentsInactif.remove()
            
    def upgrade(self):
        self.upgradeValid =True

class Capitale(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.Capitale)
        
        self.type = "Capitale"
        self.missionD =0
        self.nomJoueurAllie =""
        self.qteRessources =0
        self.compteurTempsVassal = 100000
        self.compteurVoyageDiplomate = 5000
        self.donnerRessources = False
        self.soumettreJoueur = False
        #SARAH affiche la liste d'allié possible
        #enlever des pts de diplomatie quand les vaisseaux attaquent PATRICK
        #mettre les joueurs dans allié possible quand on visite leurs systeme solaire PATRICK
        #condition de cash et enlever
        
        
    def upgradeCivilisation(self):
        pass
    
    def missionDiplomatique(self,nomJoueur,missionD,qteRessources):   
        #joueurD est le joueur qu'on vise par la demande
        #missionD        #1gas        #2minerais        #3soumettre
        self.missionD = missionD
        self.nomJoueurAllie = nomJoueur
        self.qteRessources = qteRessources
        if missionD ==3:
            self.soumettreJoueur = True
        else:
            self.donnerRessources = True
        if missionD ==1: 
            self.joueur.gas-=qteRessources
        if missionD ==2:
            self.joueur.minerai-=qteRessources  
    
    def prochaineAction(self):
        #A FAIRE v�rifier la l'aggro et la liste de joueur ennemi
        if self.donnerRessources == True:
            if self.compteurVoyageDiplomate == 0:
                if self.nomJoueurAllie in self.joueur.listeJoueursRencontres:
                    self.joueur.listeJoueursRencontres.remove(self.nomJoueurAllie)
                if self.nomJoueurAllie not in self.joueur.listeAllie:
                    self.joueur.listeAllie.append(self.nomJoueurAllie)
                self.compteurVoyageDiplomate = 5000
                for v in self.joueur.possessions["vaisseau"]:
                    if v.type == "Diplomate":
                        self.joueur.possessions["vaisseau"].remove(v)
                
            else:#durée de temps pour que le diplomate ce rendre à l'autre civilisation
                self.compteurVoyageDiplomate -=1
            
        if self.soumettreJoueur == True:
            if self.compteurTempsVassal == 0:
                if self.nomJoueurAllie in self.joueur.listeAllie:
                    self.joueur.listeAllie.remove(self.nomJoueurAllie)
                self.joueur.joueurVassal.append(self.nomJoueurAllie)
                self.compteurVoyageDiplomate = 5000
                self.joueur.diplomatie -= 20000
                self.joueur.parent.joueurs[self.nomJoueurAllie].listeAllie.append(self.joueur.nom)
                for v in self.joueur.possessions["vaisseau"]:#enleve le vaisseau diplomate
                    if v.type == "Diplomate":
                        self.joueur.possessions["vaisseau"].remove(v)
            else:#durée de temps pour que le diplomate ce rendre à l'autre civilisation
                self.compteurTempsVassal -=1
                
    #�TAPE 5
    def donnerRessources(self):
        joueurPaye = self.joueur.parent.joueurs[self.nomJoueurAllie]
        if self.missionD == 1: #on donne du gas        
            joueurPaye.gas+=self.qteRessources
            self.joueur.diplomatie+=(0.1*self.qteRessources)
        elif self.missionD == 2: #on donne du minerai
            joueurPaye.minerai+=self.qteRessources
            self.joueur.diplomatie+=(0.1*self.qteRessources)       
        else:
            pass

class StationExtraction(Batisse):
    def __init__(self,joueur,planete,typeDeRessource):
        Batisse.__init__(self,joueur,planete,dictBatisses.StationExtraction)
        self.compteurUpgrade = self.tempsPourUpgrader
        self.typeDeRessource =typeDeRessource
        self.type = "StationExtraction"
        self.compteurMine = 41
        self.ressourceEnMinage = 85+self.niveau*15
        self.ressourceMiner =0
    
    def prochaineAction (self):#miner
        if self.upgradeValid == True:
                if compteurUpgrade == 0:
                    self.niveau+=1
                    self.upgradeValid = False
                    self.compteurUpgrade = self.tempsPourUpgrader
                else:
                    self.compteurUpgrade-=1
        
        elif self.compteurMine == 0:
            if self.typeDeRessource == "minerai":
                if self.parent.minerai >0:
                    if self.ressourceEnMinage+(.01*self.joueur.moral) < self.parent.minerai:
                        self.ressourceMiner += self.ressourceEnMinage+(1*self.joueur.moral)                
                        self.joueur.minerai+=self.ressourceEnMinage+(1*self.joueur.moral)
                        self.parent.minerai-=self.ressourceEnMinage+(1*self.joueur.moral)
                        self.compteurMine = 41-(self.niveau*1)
                    else:#quand on mine plus que la planete, on la vide
                        self.ressourceMiner += self.parent.minerai           
                        self.joueur.minerai+=self.parent.minerai 
                        self.parent.minerai-=self.parent.minerai 
                        self.compteurMine = 41-(self.niveau*1)   
                                  
            elif self.typeDeRessource == "gas":
                if self.parent.gas >0:
                    if self.ressourceEnMinage+(.01*self.joueur.moral) < self.parent.gas:
                        self.ressourceMiner += self.parent.gas+(1*self.joueur.moral)            
                        self.joueur.gas+=self.ressourceEnMinage+(1*self.joueur.moral)
                        self.parent.gas-=self.ressourceEnMinage+(1*self.joueur.moral)
                        self.compteurMine = 41-(self.niveau*1)
                    else:
                        self.ressourceMiner += self.parent.gas                
                        self.joueur.gas+=self.parent.gas
                        self.parent.gas-=self.parent.gas
                        self.compteurMine = 41-(self.niveau*1)
                           
        else:
            self.compteurMine-=1

class StationEnergie(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.StationEnergie)
        self.type = "StationEnergie"
        self.energie = 90
        self.compteurUpgrade = 100
        self.joueur.energie+=90
        
    def prochaineAction(self):
        if self.upgradeValid == True:
            if compteurUpgrade == 0:
                self.niveau+=1
                self.upgradeValid = False
                self.compteurUpgrade = 100
                self.joueur.energie+=(self.niveau*10)
            else:
                self.compteurUpgrade-=1
        
class PortSpaciale(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.PortSpaciale)
        self.type = "PortSpaciale"
        self.vitesseProd = 10-self.niveau        
        self.vaisseauProd =[]
        self.compteurId =0
        self.compteurProd = self.vitesseProd 
        self.compteurUpgrade = 100
        self.nouveauVaisseau =True
        
    def prochaineAction(self):
        if self.upgradeValid == True:
            if compteurUpgrade ==0:
                self.niveau +=1
                self.upgradeValid = False
                self.compteurUpgrade = 100
            else:
                self.compteurUpgrade-=1
        else:
            self.construireVaisseau()
            
    def queuingVaisseau(self,vDemande):
        paramVaisseaux = {"VaisseauLaser":dictVaisseaux.VaisseauLaser,
                "VaisseauMissile":dictVaisseaux.VaisseauMissile,
                "VaisseauMitraillette":dictVaisseaux.VaisseauMitraillette,
                "VaisseauCargo":dictVaisseaux.VaisseauCargo,
                "VaisseauCroiseur":dictVaisseaux.VaisseauCroiseur,
                "VaisseauDiplomate":dictVaisseaux.VaisseauDiplomate,
                "VaisseauScout":dictVaisseaux.VaisseauScout,
                "VaisseauFurtif":dictVaisseaux.VaisseauFurtif,
                "VaisseauSniper":dictVaisseaux.VaisseauSniper,
                "VaisseauBombardier":dictVaisseaux.VaisseauBombardier,
                "VaisseauAmiral":dictVaisseaux.VaisseauAmiral}
            
        self.dicVessel= {"VaisseauLaser":1,
                        "VaisseauMissile":2,
                        "VaisseauMitraillette":3,
                        "VaisseauCargo":4,
                        "VaisseauCroiseur":5,
                        "VaisseauDiplomate":6,
                        "VaisseauScout":7,
                        "VaisseauFurtif":8,
                        "VaisseauSniper":9,
                        "VaisseauBombardier":10,
                        "VaisseauAmiral":11}
        # boucle pour trouver si l'upgrade est fait
                  
        if self.dicVessel[vDemande] <=7:
            self.vaisseauProd.append(self.dictionnaireVaisseaux[vDemande](self.joueur,self.parent))
        elif self.dicVessel[vDemande] == 8:
            self.vaisseauProd.append(self.dictionnaireVaisseaux[vDemande](self.joueur,self.parent))
        elif self.dicVessel[vDemande]==9:
            self.vaisseauProd.append(self.dictionnaireVaisseaux[vDemande](self.joueur,self.parent))
        elif self.dicVessel[vDemande]==10:
            self.vaisseauProd.append(self.dictionnaireVaisseaux[vDemande](self.joueur,self.parent))
        elif self.dicVessel[vDemande]==11:
            self.vaisseauProd.append(self.dictionnaireVaisseaux[vDemande](self.joueur,self.parent))
        #enleve le cout du vaisseau ici
        self.joueur.minerai -= paramVaisseaux[vDemande]["coutMinerai"]
        self.joueur.gas -= paramVaisseaux[vDemande]["coutGas"]
        
        
    def construireVaisseau(self):
       
        if len(self.vaisseauProd) > 0 :
            if self.nouveauVaisseau:
                self.nouveauVaisseau=False
                self.compteurProd = self.vaisseauProd[0].tempsProd
                
            if self.compteurProd == 0:
                self.joueur.possessions["vaisseau"].append(self.vaisseauProd[0])
                self.vaisseauProd.pop()
                self.nouveauVaisseau=True
            else :
                self.compteurProd-=1
                    
class SatelliteDefense(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.SatelliteDefense)
        self.type = "SatelliteDefense"
        self.range = 300+(self.niveau *10)
        self.dmg = 8
        self.vitessedetir = 2.00-(self.niveau*0.2)
        self.compteurUpgrade = 100
        
    def calcDistance(self,i):
        #i liste de vaisseau du joueur
        cx = abs(i.x - self.parent.x)
        cy = abs(i.y - self.parent.y)
        c = math.sqrt(cx**2 + cy**2)
        return c      
        
    def prochaineAction(self):
        if self.upgradeValid == True:
            if self.upgradeValid == 0:
                self.niveau+=1
                self.upgradeValid = False
                self.compteurUpgrade ==100
            else:
                self.compteurUpgrade-=1
        else :
            for i in self.joueur.parent.joueurs.keys():
                for j in self.joueur.parent.joueurs[i].possessions["vaisseau"]:#j c'est le vaisseau
                    d = self.calcDistance(j)      
                    if d < self.range and self.joueur != j.parent:
                        j.vie=j.vie-(self.dmg+(self.niveau*2))
                
class Radar(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.Radar)
        self.type="Radar"
        self.range = 480+(self.niveau *20)
        self.vaisseauInRange =[]
        self.compteurUpgrade = 100
        
    def calcDistance(self,i):
        cx = abs(i.x - self.parent.parent.x)
        cy = abs(i.y - self.parent.parent.y)
        c = math.sqrt(cx**2 + cy**2)
        return c    
    
    def prochaineAction (self):
        if self.upgradeValid == True:
            if self.compteurUpgrade == 0:
                self.niveau +=1
                self.upgradeValid  = False
                self.compteurUpgrade =100
            else:
                self.compteur-=1
        else:
            self.balayage()
            
    def balayage(self):
        self.vaisseauInRange =[]
        for i in self.joueur.parent.joueurs.keys():
                for j in self.joueur.parent.joueurs[i].possessions["vaisseau"]:#j c'est le vaisseau
                    d = self.calcDistance(j)            
                    if d < self.range:
                        self.vaisseauInRange.append(j)
                
class GenerateurChampForce(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self, joueur,planete,dictBatisses.StationExtraction)
        self.type = "GenerateurChampForce"
        self.regeneration = 2.00
        self.bouclierVie = 0
        self.bouclierActuel = 0
        self.compteurUpgrade = 100
        
    def genererBouclier(self):
        for i in self.parent.batisses:
            self.bouclierVie+=10
        self.bouclierActuel= self.bouclierVie
        self.parent.boulcierExistant = True
    
    def prochaineAction(self):
        if self.upgradeValid == True:
            if self.compteurUpgrade == 0:
                self.niveau+=1
                self.regeneration = 2.00+(self.niveau*0.2)
                genererBouclier()
                self.upgradeValid = False
                self.compteurUpgrade = 100
            else:
                self.compteurUpgrade -=1
        else:
            self.regen()
            
    def regen(self):
        if self.bouclierActuel < self.bouclierVie:
            self.bouclierActuel+=self.regeneration
            
class Laboratoire(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.Laboratoire)
        self.niveauLab =1
        self.type = "Laboratoire"
        self.niveauRecherche = 1
        self.compteurUpgrade =100
        self.compteurRecherche = 100
        self.race = self.joueur.race
        self.rechercheEnCours = False
        
    def upgrade(self): #upgradeDemander = Boolean
        if self.niveauLab < 7:
            self.upgradeValid = True
        
    def recherche(self, recherche):
        self.rechercheEnCours = True
        self.recherche = rechercheVaisseaux #disons qu'on met armor ou attaque
        
    def prochaineAction(self):
        if self.upgradeValid == True:
            if self.compteurUpgrade == 0:
                self.niveauLab =+1
                self.upgradeValid = False
                self.compteurUpgrade = 100
            else:
                self.compteur-=1
        else:
            self.recherche()
                
    def recherche(self):
        if self.rechercheEnCours == True:
            if self.compteurUpgrade == 0:
                self.niveauRecherche =+1
                self.rechercheEnCours = False
                self.compteurUpgrade = 100
                self.applicationRecherche()
                #selon le nivau de la recherche, on upgrade de quoi genre tout les vaisseaux vont
                #plus vite ou fesse plus fort, c'est poas compliqué a faire avec le dictio
            else:
                self.compteur-=1
                
    def applicationRecherche(self):
        if self.race == "Jedi":
            for i in self.dictionnaireVaisseaux.keys():
                if len(dictionnaireVaisseaux[i])==8:#les vaissaeux de combats on 8 params
                    dictionnaireVaisseaux[i]["vitesse"] *= 0.075
            for i in self.joueur.possessions["vaisseau"]:
                i.vitesse*= .075
        if self.race == "Robot":
            for i in self.dictionnaireVaisseaux.keys():
                if len(dictionnaireVaisseaux[i])==8:#les vaissaeux de combats on 8 params
                    dictionnaireVaisseaux[i]["vie"] *= .075
            for i in self.joueur.possessions["vaisseau"]:
                i.vie*= .075
        if self.race == "Space squids":
            for i in self.dictionnaireVaisseaux.keys():
                if len(dictionnaireVaisseaux[i])==8:#les vaissaeux de combats on 8 params
                    dictionnaireVaisseaux[i]["puissance"] *= .075
            for i in self.joueur.possessions["vaisseau"]:
                i.puissance*= .075

class Temple (Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete,dictBatisses.Temple)
        self.type = "Temple"
        self.artefactsDetenus = []
        self.compteurUpgrade= 100
        
    def prochaineAction(self):
        if self.upgradeValid == True:
            if self.compteurUpgrade == 0:
                self.niveau +=1
                self.upgradeValid = False
                self.compteurUpgrade = 100
            else:
                self.compteurUpgrade-=1
                
    def afficherListeArtefacts(self):
        return self.artefactsDetenus
        
    def entreposerArtefacts(self, artefact):
        if self.niveau > len(self.artefactsDetenus):
            self.artefactsDetenus.append(artefact)
        if len(self.artefactsDetenus) > 15:
            pass#ON GAGNE!

class Colisee(Batisse):
    def __init__(self,joueur,planete,paramBatisse):
        Batisse.__init__(self,joueur,planete)
        self.type = "Colisee"
        self.compteurFormation = 10
        self.compteurCombat = 1000
        self.monstre = 0
        self.monstreEnchainer = False
        self.gladiateur =  0# pour compliquer on pourrait faire un object
        self.gladiateurEnFormation = 0
        self.moraleObtenus = 0
        self.filedattente = 0
      
    def prochaineAction(self):
        pass
    
    def formerGladiateur(self,gladiateurDemander): # lui passer un int
        if gladiateurDemander >0:
            self.filedattente +=gladiateurDemander
            if compteurFormation == 0:
                self.gladiateur+=2
                self.filedattente-=2
                self.compteurFormation = 10 
            else:
                self.compteurFormation-=1
                
    def combatGG(self):
        if self.compteurCombat == 0:
            self.gladiateur = self.gladiateur/2
            self.moraleObtenus = (self.gladiateur * 5)
            self.parent.parent.Joueur.moral +=self.moraleObtenus
        else:
            self.compteurCombat-=1
            
    def combatGM(self):
        if self.compteurCombat == 0:
            self.gladiateur = self.gladiateur/6
            self.monstre -=1
            self.moraleObtenus = (self.gladiateur * 10)
            self.parent.parent.Joueur.moral +=self.moraleObtenus
        else:
            self.compteurCombat-=1
    
    
