#GATE
# -*- encoding: ISO-8859-1 -*-
import random
        
class Gate(object):
    def __init__(self,parent,x,y):
        self.parent=parent#le sys solaire
        self.x=x
        self.y=y
        self.galaxy = parent.id
        self.liaison = None
        self.range = 15
        self.etat = 0
        self.positif = True
        self.vaisseauxATeleporter = []
        self.tag="gate"
        
    def prochaineAction(self):
        self.teleportation()
        self.changementEtat ()
        
    def changementEtat (self):
        if self.positif:
            self.etat += 1
        else:
            self.etat -= 1
            
        if self.etat == 10:
            self.positif = False
        elif self.etat == 0:
            self.positif = True
        
    def teleportation (self):
        listeNomJoueurs = self.parent.parent.joueurs.keys()
        for i in listeNomJoueurs:
            #Traverse la liste de tous les joueurs
            j = self.parent.parent.joueurs[i]
            #Parcourir la liste des vaisseaux des autres joueurs
            for k in j.possessions["vaisseau"]:
                #Si le vaisseau est dans la meme galaxy
                if (k.galaxy == self.galaxy):
                    #Si le vaisseau est dans le range
                    self.calculerDistance (k)
                    
        for i in self.vaisseauxATeleporter:
            #chiffre random
            xRandom = random.randrange(20, 35)
            yRandom = random.randrange(20, 35)
            
            #positif negatif random
            dirx=random.randrange(2)-1
            if dirx==0:
                dirx=1
            diry=random.randrange(2)-1
            if diry==0:
                diry=1
            
            #position random en tenant compte du negatif ou positif    
            xRandom *= dirx
            yRandom *= diry
            
            #nouvelle localisation du vaisseau
            i.x = self.liaison.x + xRandom
            i.y = self.liaison.y + yRandom
            i.galaxy = self.liaison.galaxy
            
        self.vaisseauxATeleporter = []
        
                    
    def calculerDistance (self, vaisseau):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseau.x)**2 + (self.y - vaisseau.y)**2)
        if (distance < self.range):
            self.vaisseauxATeleporter.append(vaisseau)






















