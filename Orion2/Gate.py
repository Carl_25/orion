#GATE
# -*- encoding: ISO-8859-1 -*-
import random
from helper import *
        
class Gate(object):
    def __init__(self,parent,x,y):
        self.parent=parent#le sys solaire
        self.x=x
        self.y=y
        self.systemeSolaire = parent
        self.liaison = None
        self.range = 15
        self.etat = 0
        self.positif = True
        self.vaisseauxATeleporter = []
        self.vaisseauxARemover = []
        self.tag="gate"
        
    def prochaineAction(self):
        self.teleportation()
        self.changementEtat ()
        
    def cooldownTeleportation (self):
        for i in self.vaisseauxATeleporter:
            if i.cooldownTeleportation != 0:
                i.cooldownTeleportation -= 1
                self.vaisseauxARemover.append(i)
        
        for i in self.vaisseauxARemover:
            self.vaisseauxATeleporter.remove(i)
        
        self.vaisseauxARemover = []
        
    def changementEtat (self):
        if self.positif:
            self.etat += 1
        else:
            self.etat -= 1
            
        if self.etat == 10:
            self.positif = False
        elif self.etat == 0:
            self.positif = True
        
    def teleportation (self):
        listeNomJoueurs = self.parent.parent.joueurs.keys()
        for i in listeNomJoueurs:
            #Traverse la liste de tous les joueurs
            j = self.parent.parent.joueurs[i]
            #Parcourir la liste des vaisseaux des autres joueurs
            for k in j.possessions["vaisseau"]:
                #Si le vaisseau est dans le meme systemeSolaire
                if (k.systemeSolaire == self.systemeSolaire):
                    #Si le vaisseau est dans le range
                    self.calculerDistance (k)
        
        self.cooldownTeleportation()
                    
        for i in self.vaisseauxATeleporter:
            print("Vaisseau teleporter", i, i.cooldownTeleportation)
            i.cooldownTeleportation = 500
            #chiffre random
            xRandom = random.randrange(20, 35)
            yRandom = random.randrange(20, 35)
            
            #positif negatif random
            dirx=random.randrange(2)-1
            if dirx==0:
                dirx=1
            diry=random.randrange(2)-1
            if diry==0:
                diry=1
            
            #position random en tenant compte du negatif ou positif    
            xRandom *= dirx
            yRandom *= diry
            
            #nouvelle localisation du vaisseau
            i.arreter()
            i.x = self.liaison.x
            i.y = self.liaison.y
            i.systemeSolaire = self.liaison.systemeSolaire
            i.ajoutSysSolairesEnvahi(self.liaison.systemeSolaire)
        self.vaisseauxATeleporter = []
        
                    
    def calculerDistance (self, vaisseau):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseau.x)**2 + (self.y - vaisseau.y)**2)
        if (distance < self.range):
            self.vaisseauxATeleporter.append(vaisseau)






















