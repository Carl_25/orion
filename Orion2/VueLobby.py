# -*- encoding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.tix as Tix


class VueLobby():
    def __init__(self, parent, ipadress):
        # le parent -> Controleur
        self.parent = parent
        self.ipadress = self.parent.ipLocale
        self.ipserveur = ipadress
         
        # Changer l'apparence de la fen�tre
        self.parent.root.config(bg = "black")
        self.parent.root.title("Orion - Lobby")
        
        # le frame contenant ma fen�tre
        self.f_Lobby = CadreCustom(self.parent.root)
        self.f_Lobby.config(bg = "black")
        self.f_Lobby.grid()
        
        # -----------------------------------------------------------------------------------------------------------------
        #Cr�ation de l'interface, c�t� gauche
        self.f_espaceGauche = CadreCustom(self.f_Lobby, padx=10, pady=5)
        self.f_espaceGauche.grid( row = 0, column = 0 )

        # Bouton Pr�t / Lancer la partie
        self.b_demarrer = BoutonCustom(self.f_espaceGauche, text = "D�marrer la partie", width = 30,  command=self.demarrePartie)
        self.afficherBDemarrerSiServeur()

        # -----------------------------------------------------
        #Cadre et ADRESSE IP du SERVEUR
        self.lf_AdresseServeur = CadreEtiquetteCustom(self.f_espaceGauche , text = "Adresse IP du serveur")
        self.lf_AdresseServeur.pack( fill=BOTH, side=TOP )
        # Adresse IP (Label)
        self.l_AdresseIP = EtiquetteCustom(self.lf_AdresseServeur, text = str(self.ipserveur))
        self.l_AdresseIP.pack()
        
        # -------------------------------------------
        #Cadre et ADRESSE IP du Joueur
        self.lf_AdresseLocale = CadreEtiquetteCustom(self.f_espaceGauche , text = "Adresse IP du joueur")
        self.lf_AdresseLocale.pack( fill=BOTH, side=TOP )
        # Adresse IP (Label)
        self.l_AdresseIPLocale = EtiquetteCustom(self.lf_AdresseLocale, text = str(self.ipadress))
        self.l_AdresseIPLocale.pack()
        
        # -----------------------------------------------------
        #Cadre et INFORMATIONS sur JOUEURSCONNECTES
        self.lf_JoueursConnectes = CadreEtiquetteCustom(self.f_espaceGauche, text = "Joueurs connect�s", padx=10, pady=5 )
        self.lf_JoueursConnectes.pack( fill=BOTH, side=TOP )
        #Affichage des joueurs connect� non dynamique
        self.lbox_JoueurConnect = Listbox(self.lf_JoueursConnectes, height=15, selectmode=SINGLE)
        # Mettre La liste sur la page
        self.lbox_JoueurConnect.pack(fill=BOTH, expand=1)
    
        # -------------------------------------------
        # Liste dynamique des joueurs connect�
        self.afficheListeJoueurs(self.parent.afficheListeJoueurs())

        # -------------------------------------------
        # Mettre une image Logo
        self.img_Logo = PhotoImage(file="image/banniereLobby.gif")                            
        self.l_logoMenu = EtiquetteCustom(self.f_Lobby, image = self.img_Logo)    
        self.l_logoMenu.configure(highlightthickness = 0)                          
        self.l_logoMenu.grid( row = 0, column = 1 )
        
    
    # JF demarrer la partie bouton    
    def demarrePartie(self):
        self.parent.demarrePartie()
      
    # Le bouton demarrer est disponible seulement pour le joueur qui a d�marr� le serveur  
    def afficherBDemarrerSiServeur(self):
        etat = self.parent.parent.serveurLocal
        if (etat == 1):
            self.b_demarrer.pack( padx=5, pady=10, side = BOTTOM )
 
    def afficheListeJoueurs (self, lj):
        self.lbox_JoueurConnect.delete(0,'end') 
        for i in lj:
            self.lbox_JoueurConnect.insert('end', i)   
                

        
# Les sous-classes pour une interface personnalis�e

class BoutonCustom(Button):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)
        
class EtiquetteCustom(Label):
    def __init__(self,parent, **kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="black"
        Label.__init__(self,parent,**kw)
        
class CadreCustom(Frame):
    def __init__(self,parent,**kw):
        kw["bg"]="black"
        Frame.__init__(self,parent,**kw)
        
class CadreEtiquetteCustom(LabelFrame):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"] = f
        kw["fg"]="white"
        kw["bg"]="black"
        LabelFrame.__init__(self,parent,**kw)

   
     
