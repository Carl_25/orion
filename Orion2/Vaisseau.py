# -*- encoding: ISO-8859-1 -*-
import random
import dictVaisseaux
from helper import *
from AttaquesVaisseaux import *

class Vaisseau(object):
    def __init__(self,parent,p,dictVaisseaux):
        self.parent=parent#Joueur le parent
        self.parent.parent.ids =  self.parent.parent.unID.nextId(self.parent.parent.ids)
        self.id = self.parent.parent.ids 
        self.nom=parent.nom
        dirx=random.randrange(2)-1
        if dirx==0:
            dirx=1
        diry=random.randrange(2)-1
        if diry==0:
            diry=1
        self.x=p.x+ (random.randrange(15,30)* dirx)
        self.y=p.y+ (random.randrange(15,30) * diry)
        
        self.cooldownTeleportation = 0
        self.vitesse=dictVaisseaux["vitesse"]
        self.angle=0
        self.cible=[]
        self.systemeSolaire = p.parent#le id du sys solaire
        self.coutMinerai = dictVaisseaux["coutMinerai"]
        self.coutGas = dictVaisseaux["coutGas"]
        self.vie = dictVaisseaux["vie"]
        self.tempsProd = dictVaisseaux["tempsProd"]
        self.angle=0
        self.objetAPoursuivre = None
        self.typeObjetAPoursuivre = ""
        self.distanceAPoursuivre = 0
        self.patrouilleActif = False
        #si patrouille vers l'arrive = false, si vers depart true
        self.patversArrive = False  
        self.pointPatXdepart = 0
        self.pointPatYdepart = 0
        self.pointPatXarrive = 0
        self.pointPatYarrive = 0        
        self.tag="vaisseau" 
        
    def ajoutSysSolairesEnvahi(self,sysSol):
        ajouter = True
        for i in self.parent.systemesSolairesEnvahis:
            if i.id == sysSol.id:
                ajouter = False
        if ajouter:
            self.parent.systemesSolairesEnvahis.append(sysSol)    
        
    def changeCible(self,x,y):
       if int(self.systemeSolaire.id) == int(self.parent.sysSolaireVue):
           print('je change cible',x,y)
           self.objetAPoursuivre = None
           self.typeObjetAPoursuivre = None
           self.cible=[x,y]
           self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])  
        
    def changeCiblePoursuite(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
    
    def prochaineAction(self):
        #print("DO SOMETING")
        self.deplacement()
        self.cooldownTeleport()
        
    def cooldownTeleport (self):
        if self.cooldownTeleportation != 0:
            self.cooldownTeleportation -= 1

    def fonctObjetAPoursuivre (self):
        if (self.objetAPoursuivre != None):
            if (self.typeObjetAPoursuivre == "vaisseau"):
                if (self.systemeSolaire == self.objetAPoursuivre.systemeSolaire):
                    self.calculerDistancePoursuite( self.objetAPoursuivre )
                    if (self.distanceAPoursuivre > 50):
                        self.changeCiblePoursuite(self.objetAPoursuivre.x, self.objetAPoursuivre.y)
                else:
                    self.objetAPoursuivre = None
                    self.typeObjetAPoursuivre = None
            else:
                self.calculerDistancePoursuite( self.objetAPoursuivre )
                if (self.distanceAPoursuivre > 50):
                    self.changeCiblePoursuite(self.objetAPoursuivre.x, self.objetAPoursuivre.y)
                    
    def deplacement (self):
        self.fonctObjetAPoursuivre()
        if (self.objetAPoursuivre != None):
            if (self.distanceAPoursuivre > 50):
                if self.cible:
                    #calcul de l'angle de la direction
                    x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
                    self.x=x
                    self.y=y
                    d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
        else:
            if self.cible:
                #calcul de l'angle de la direction
                x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
                self.x=x
                self.y=y
                d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
                #si arrive a destination.
                if d<self.vitesse:
                    if (self.patrouilleActif):
                        #verifie si le vaisseau se deplacer vers le point d'arrive
                        #si oui, change pour point de depart, redonne nouvelles coordonne
                        #inverse pour le else.
                        if(self.patversArrive):
                            changerCible(self.pointPatXdepart, self.pointPatYdepart)
                            self.patversArrive = False
                        else:
                            changeCible(self.pointPatXarrive, self.pointPatYarrive)
                            self.patversArrive = True
                    else:    
                        self.cible=[]
   
    def deplacementVersAutreVaisseau (self, objetAPoursuivre, typeObjetAPoursuivre):
        self.objetAPoursuivre = objetAPoursuivre
        self.typeObjetAPoursuivre = typeObjetAPoursuivre
        self.calculerDistancePoursuite(objetAPoursuivre)
        
    def calculerDistancePoursuite (self, vaisseau):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseau.x)**2 + (self.y - vaisseau.y)**2)  
        self.distanceAPoursuivre = distance
        
    def arreter (self):
        self.cible = []
        self.objetAPoursuivre = None
        self.distanceAPoursuivre = 0
        self.patrouilleActif = False
        
class VaisseauCombat (Vaisseau):
    def __init__(self, parent, p, dictVaisseaux):
        Vaisseau.__init__(self, parent,  p, dictVaisseaux)
        self.puissance = dictVaisseaux["puissance"]
        self.range = dictVaisseaux["range"]
        self.vitesse = 10
        self.vitesseAttaque = dictVaisseaux["vitesseAttaque"]
        self.preparationAttaque = 20 - self.vitesseAttaque
        self.objetAAttaquer = None
        self.distanceVaisseauAttaquer = self.range + 10
        
    def prochaineAction(self):
        #print("DO SOMETING")
        if self.vie >=0:
            self.deplacement()
            self.attaquer()
            self.cooldownTeleport()
        else:
            self.parent.detruireVaisseau(self)
        
    def attaquer (self):
        if self.preparationAttaque == 0:
            #Regarder si il y a un ennemi autour
            #Trouver l'ennemi le plus proche
            #Attaquer l'ennemi
            
            # Par d�faut, valide est faux
            valide = False 
            
            # �tape 1 : v�rification de la cible
            # Si on a un objet � poursuivre, l'attaque est potentiellement valide
            if (self.objetAPoursuivre != None):
                valide = True
                
                if (self.typeObjetAPoursuivre == "vaisseau"):
                    # On ne veut pas tirer sur nos propres vaisseaux
                    for i in self.parent.possessions["vaisseau"]:
                        if (self.objetAPoursuivre == i):
                            valide = False
                            break
                    for i in self.parent.listeAllie:
                        for j in i.possessions["vaisseau"]:
                            if (self.objetAPoursuivre == j):
                                valide = False
                                break
                        
                # On v�rifie si c'est une plan�te      
                elif self.typeObjetAPoursuivre == "planete":
                    valide = False
                    listeNomJoueurs = self.parent.parent.joueurs.keys()
                    for i in listeNomJoueurs:
                        #Traverse la liste de tous les joueurs
                        j = self.parent.parent.joueurs[i]
                        #Si nous somme le joueur ou que le joueur est un alli�, passer
                        if (j.nom != self.parent.nom):
                            validationNonAllie = True
                            for k in self.parent.listeAllie:
                                if (j.nom == k.nom):
                                    validationNonAllie = False
                            #Si le joueur n'est pas un allie ni nous
                            if validationNonAllie:
                                #Si la planete a poursuivre fait partie des planetes colonis�es d'un ennemi
                                for l in j.possessions["planetesColonisees"]:
                                    if self.objetAPoursuivre == l:
                                        valide = True
                                
            
            # �tape 2-a : attaque automatique dans les alentours
            if valide == False:
                
                # On chercher le vaisseau ennemi le plus proche
                listeNomJoueurs = self.parent.parent.joueurs.keys()
                for i in listeNomJoueurs:
                    #Traverse la liste de tous les joueurs
                    j = self.parent.parent.joueurs[i]
                    #Si nous somme le joueur, passer (on ne veut pas attaquer nos vaisseaux)
                    if (j.nom != self.parent.nom):
                        #Parcourir la liste des vaisseaux des autres joueurs
                        for k in j.possessions["vaisseau"]:
                            #Si le vaisseau est dans le meme systemeSolaire
                            if (k.systemeSolaire == self.systemeSolaire):
                                #Si le vaisseau est dans le range
                                self.calculerDistance (k)
                                    
                #Si il y a un vaisseau � attaquer dans la distance d'attaque de notre vaisseau, faire les dommages
                if (self.objetAAttaquer != None):
                    self.preparationAttaque = 50 - self.vitesseAttaque
                    self.dommage(valide)
            
            # �tape 2-b : attaquer l'objet � poursuivre      
            elif (valide == True):
                self.calculerDistancePoursuiteAttaque(self.objetAPoursuivre)
                if (self.objetAAttaquer != None):
                    self.preparationAttaque = 20 - self.vitesseAttaque
                    self.dommage(valide)
        else:
            self.preparationAttaque -= 1
                

            
    def calculerDistancePoursuiteAttaque (self, vaisseau):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseau.x)**2 + (self.y - vaisseau.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            self.objetAAttaquer = vaisseau
        
        #Note la distance de l'objectif    
        self.distanceAPoursuivre = distance
    
    def calculerDistance (self, vaisseauEnnemi):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseauEnnemi.x)**2 + (self.y - vaisseauEnnemi.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            #Compare distance la plus petite pour le vaisseau � attaquer
            if (distance < self.distanceVaisseauAttaquer):
                self.distanceVaisseauAttaquer = distance
                self.objetAAttaquer = vaisseauEnnemi
                
    def dommage(self, valide):
        # Attaque sp�cifique
        self.parent.listeAttaquesFaites.append(Laser(self, self.objetAAttaquer.x, self.objetAAttaquer.y))
        if valide:
            if self.typeObjetAPoursuivre == "vaisseau":
                self.joueur.diplomatie -= 1
                #Enleve la vie au vaisseau ennemi
                self.objetAAttaquer.vie -= self.puissance
                #Remet les attributs d'attaque du vaisseau a default
                self.objetAAttaquer = None
                self.distanceVaisseauAttaquer = self.range + 10
            else:
                self.joueur.diplomatie -= 1
                #Enleve la vie a la planete ennemi
                self.objetAAttaquer.dommage += self.puissance
                #Remet les attributs d'attaque du vaisseau a default
                self.objetAAttaquer = None
                self.distanceVaisseauAttaquer = self.range + 10
        
        # Attaque automatique        
        else:
            self.joueur.diplomatie -= 1
            #Enleve la vie au vaisseau ennemi
            self.objetAAttaquer.vie -= self.puissance
            #Remet les attributs d'attaque du vaisseau a default
            self.objetAAttaquer = None
            self.distanceVaisseauAttaquer = self.range + 10
        
        
    def patrouille(self, pointX, pointY):
        #permet � un vaisseau de se placer d'un point X � un point Y sans arr�t.
        self.pointPatXarrive = pointX
        self.pointPatYarrive = pointY
        self.pointPatXdepart = self.x 
        self.pointPatYdepart = self.y
        self.patversArrive = True
        self.changerCible(pointX, pointY)
    def __init__(self, parent, p, dictVaisseaux):
        Vaisseau.__init__(self, parent, p, dictVaisseaux)
        self.puissance = dictVaisseaux["puissance"]
        self.range = dictVaisseaux["range"]
        self.vitesse = 10
        self.vitesseAttaque = dictVaisseaux["vitesseAttaque"]
        self.preparationAttaque = 20 - self.vitesseAttaque
        self.objetAAttaquer = None
        self.distanceVaisseauAttaquer = self.range + 10
        
    def attaquer (self):
        if self.preparationAttaque == 0:
            #Regarder si il y a un ennemi autour
            #Trouver l'ennemi le plus proche
            #Attaquer l'ennemi
            
            # Par d�faut, valide est faux
            valide = False 
            
            # �tape 1 : v�rification de la cible
            # Si on a un objet � poursuivre, l'attaque est potentiellement valide
            if (self.objetAPoursuivre != None):
                valide = True
                
                if (self.typeObjetAPoursuivre == "vaisseau"):
                    # On ne veut pas tirer sur nos propres vaisseaux
                    for i in self.parent.possessions["vaisseau"]:
                        if (self.objetAPoursuivre == i):
                            valide = False
                            break
                    for i in self.parent.listeAllie:
                        for j in i.possessions["vaisseau"]:
                            if (self.objetAPoursuivre == j):
                                valide = False
                                break
                        
                # On v�rifie si c'est une plan�te      
                elif self.typeObjetAPoursuivre == "planete":
                    valide = False
                    listeNomJoueurs = self.parent.parent.joueurs.keys()
                    for i in listeNomJoueurs:
                        #Traverse la liste de tous les joueurs
                        j = self.parent.parent.joueurs[i]
                        #Si nous somme le joueur ou que le joueur est un alli�, passer
                        if (j.nom != self.parent.nom):
                            validationNonAllie = True
                            for k in self.parent.listeAllie:
                                if (j.nom == k.nom):
                                    validationNonAllie = False
                            #Si le joueur n'est pas un allie ni nous
                            if validationNonAllie:
                                #Si la planete a poursuivre fait partie des planetes colonis�es d'un ennemi
                                for l in j.possessions["planetesColonisees"]:
                                    if self.objetAPoursuivre == l:
                                        valide = True
                                
            
            # �tape 2-a : attaque automatique dans les alentours
            if valide == False:
                
                # On chercher le vaisseau ennemi le plus proche
                listeNomJoueurs = self.parent.parent.joueurs.keys()
                for i in listeNomJoueurs:
                    #Traverse la liste de tous les joueurs
                    j = self.parent.parent.joueurs[i]
                    #Si nous somme le joueur, passer (on ne veut pas attaquer nos vaisseaux)
                    if (j.nom != self.parent.nom):
                        #Parcourir la liste des vaisseaux des autres joueurs
                        for k in j.possessions["vaisseau"]:
                            #Si le vaisseau est dans la meme systemeSolaire
                            if (k.systemeSolaire == self.systemeSolaire):
                                #Si le vaisseau est dans le range
                                self.calculerDistance (k)
                                    
                #Si il y a un vaisseau � attaquer dans la distance d'attaque de notre vaisseau, faire les dommages
                if (self.objetAAttaquer != None):
                    self.preparationAttaque = 50 - self.vitesseAttaque
                    self.dommage(valide)
            
            # �tape 2-b : attaquer l'objet � poursuivre      
            elif (valide == True):
                self.calculerDistancePoursuiteAttaque(self.objetAPoursuivre)
                if (self.objetAAttaquer != None):
                    self.preparationAttaque = 20 - self.vitesseAttaque
                    self.dommage(valide)
        else:
            self.preparationAttaque -= 1
                

            
    def calculerDistancePoursuiteAttaque (self, vaisseau):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseau.x)**2 + (self.y - vaisseau.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            self.objetAAttaquer = vaisseau
        
        #Note la distance de l'objectif    
        self.distanceAPoursuivre = distance
    
    def calculerDistance (self, vaisseauEnnemi):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - vaisseauEnnemi.x)**2 + (self.y - vaisseauEnnemi.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            #Compare distance la plus petite pour le vaisseau � attaquer
            if (distance < self.distanceVaisseauAttaquer):
                self.distanceVaisseauAttaquer = distance
                self.objetAAttaquer = vaisseauEnnemi
                
    def dommage(self, valide):
        # Attaque sp�cifique
        self.parent.listeAttaquesFaites.append(Laser(self, self.objetAAttaquer.x, self.objetAAttaquer.y))
        if valide:
            if self.typeObjetAPoursuivre == "vaisseau":
                self.parent.diplomatie -= 1
                #Enleve la vie au vaisseau ennemi
                self.objetAAttaquer.vie -= self.puissance
                #Remet les attributs d'attaque du vaisseau a default
                self.objetAAttaquer = None
                self.distanceVaisseauAttaquer = self.range + 10
            else:
                self.parent.diplomatie -= 1
                #Enleve la vie a la planete ennemi
                self.objetAAttaquer.dommage += self.puissance
                #Remet les attributs d'attaque du vaisseau a default
                self.objetAAttaquer = None
                self.distanceVaisseauAttaquer = self.range + 10
        
        # Attaque automatique        
        else:
            self.parent.diplomatie -= 1
            #Enleve la vie au vaisseau ennemi
            self.objetAAttaquer.vie -= self.puissance
            #Remet les attributs d'attaque du vaisseau a default
            self.objetAAttaquer = None
            self.distanceVaisseauAttaquer = self.range + 10
        
        
    def patrouille(self, pointX, pointY):
        #permet � un vaisseau de se placer d'un point X � un point Y sans arr�t.
        self.pointPatXarrive = pointX
        self.pointPatYarrive = pointY
        self.pointPatXdepart = self.x 
        self.pointPatYdepart = self.y
        self.patversArrive = True
        self.changerCible(pointX, pointY)
        
class VaisseauLaser (VaisseauCombat):
    def __init__(self, parent,  p):
        VaisseauCombat.__init__(self, parent,  p, dictVaisseaux.VaisseauLaser) #puissance range vitesse valeur a d�finir
        self.type = "Laser"
    
        
class VaisseauMissile (VaisseauCombat):
    def __init__(self, parent,  p):
        VaisseauCombat.__init__(self, parent,  p, dictVaisseaux.VaisseauMissile) #puissance range vitesse valeur a d�finir
        self.type = "Missile"
        
    def dommage(self, valide):
        # Attaque sp�cifique
        self.parent.listeAttaquesFaites.append(Missile(self, self.objetAAttaquer))
        self.objetAAttaquer = None
        self.distanceVaisseauAttaquer = self.range + 10
     
class VaisseauFurtif (VaisseauCombat):
    def __init__(self, parent,  p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauFurtif) #puissance range vitesse valeur a d�finir
        self.rangeBonus = 0
        self.type = "Furtif"
        
class VaisseauMitraillette (VaisseauCombat):
    def __init__(self, parent,  p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauMitraillette) #puissance range vitesse valeur a d�finir
        self.type = "Mitraillette"
        
class VaisseauCroiseur (VaisseauCombat):
    def __init__(self, parent, p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauCroiseur) #puissance range vitesse valeur a d�finir
        self.type = "Croiseur"
        
class VaisseauSniper (VaisseauCombat):
    def __init__(self, parent, p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauSniper) #puissance range vitesse valeur a d�finir
        self.type = "Sniper"
        
class VaisseauAmiral (VaisseauCombat):
    def __init__(self, parent, p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauAmiral) #puissance range vitesse valeur a d�finir
        self.rangeBonus = 0
        self.objetAAttaquer = []
        self.type = "Amiral"
        
    def attaquer (self):
        if self.preparationAttaque == 0:
            #Regarder si il y a un ennemi autour
            #Trouver l'ennemi le plus proche
            #Attaquer l'ennemi
    
            # On chercher le vaisseau ennemi le plus proche
            listeNomJoueurs = self.parent.parent.joueurs.keys()
            for i in listeNomJoueurs:
                #Traverse la liste de tous les joueurs
                j = self.parent.parent.joueurs[i]
                #Si nous somme le joueur, passer (on ne veut pas attaquer nos vaisseaux)
                if (j.nom != self.parent.nom):
                    #Parcourir la liste des vaisseaux des autres joueurs
                    for k in j.possessions["vaisseau"]:
                        #Si le vaisseau est dans la meme systemeSolaire
                        if (k.systemeSolaire == self.systemeSolaire):
                            #Si le vaisseau est dans le range
                            self.calculerDistance (k, "vaisseau")
                            
            for i in self.parent.parent.systemeSolaires:
                if i.nom == self.systemeSolaire:
                    for j in i.planetes:
                        valide = True
                        for k in self.parent.possessions[planetesColonisees]:
                            if j == k:
                                valide = False
                        if valide:
                            calculerDistance(j, "planete")
                                    
            #Si il y a un vaisseau � attaquer dans la distance d'attaque de notre vaisseau, faire les dommages
            if (self.objetAAttaquer != None):
                self.preparationAttaque = 20 - self.vitesseAttaque
                self.dommage()
        else:
            self.preparationAttaque -= 1
            
    def calculerDistance (self, objetEnnemi, typeObjet):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - objetEnnemi.x)**2 + (self.y - objetEnnemi.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            objetDansRange = []
            objetDansRange.append(objetEnnemi)
            objetDansRange.append(typeObjet)
            self.objetAAttaquer.append(objetDansRange)
        
    def dommage(self):
        for i in self.objetAAttaquer:
            self.parent.listeAttaquesFaites.append(Missile(self, i[0]))
        self.objetAAttaquer = []
        self.distanceVaisseauAttaquer = self.range + 10
        
class VaisseauBombardier (VaisseauCombat):
    def __init__(self, parent, p):
        VaisseauCombat.__init__(self, parent, p, dictVaisseaux.VaisseauBombardier) #puissance range vitesse valeur a d�finir
        self.puissancePlanete = 0
        self.type = "Bombardier"
        
    def dommage(self):
        # Attaque sp�cifique
        self.parent.listeAttaquesFaites.append(Bombe(self, self.objetAAttaquer.x, self.objetAAttaquer.y))
        self.objetAAttaquer = None
        self.distanceVaisseauAttaquer = self.range + 10

#Classes des vaisseaux de bases     
class VaisseauCargo (Vaisseau):
    def __init__(self, parent, p):
        Vaisseau.__init__(self, parent, p, dictVaisseaux.VaisseauCargo) 
        self.minerai = 0
        self.gold = 0
        self.gas = 0
        self.artefacts = []
        #chaque ressource va avoir une valeur differente de capacitedechargement
        self.capaciteMax = dictVaisseaux.VaisseauCargo["capaciteMax"]
        self.capacite = dictVaisseaux.VaisseauCargo["capacite"]
        self.intensionDecharger = False
        self.joueurCible = None
        self.type = "Cargo"
        
    def charger (self, element,type):
        #type 1 = artefact
        #2 = gas
        #3 = gold
        #4 = minerai
        
        if(type == 1):
            if(self.capacite + 20 <= self.capaciteMax):
                self.capacite += 20
                self.artefacts.append(element)
        elif(type == 2):
            if(self.capacite + element <= self.capaciteMax):
                self.capacite += element
                self.gas += element
                
        elif(type == 3):
            if(self.capacite + element <= self.capaciteMax):
                self.capacite += element
                self.gold += element
                
        elif(type == 4):
            if(self.capacite + element <= self.capaciteMax):
                self.capacite += element
                self.minerai += element
        
    
    def decharger (self, planete):
        #V�rifie si la planete possede une capitale
        for i in planete.listeBatiments:
            if (isinstance(i, Capitale)):
                #Trouve le joueur qui possede cette capitale
                for j in (self.parent.parent.parent.parent.parent.listeJoueurs):
                    if (j.listePlaneteColoniser[0] == planete):
                        #Enregistre le joueur
                        self.joueurCible = j
                        break
                break
                        
                        
           
    def deplacement (self):
        if self.cible:
            x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
            self.x=x
            self.y=y
            d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            #si arrive a destination.
            if d<self.vitesse:
                if (self.intensionDecharger):
                    self.joueurCible.gas += self.gas
                    self.gas = 0
                    
                    self.joueurCible.gold += self.gold
                    self.gold = 0
                    
                    self.joueurCible.minerai += self.minerai
                    self.minerai = 0
                    
                    for i in self.artefacts:
                        self.joueurCible.listeArtefacts.append(artefacts)
                    for i in range (self.artefacts.size()):
                        self.artefacts.pop()
                    
                    self.capacite = 0
                    self.intensionDecharger = False
                    self.joueurCible = None
                    
                else:    
                    self.cible=[]
        
    
class VaisseauDiplomate (Vaisseau):
    def __init__(self, parent,p):
        Vaisseau.__init__(self, parent, p, dictVaisseaux.VaisseauDiplomate)
        self.type = "Diplomate" 
    
class VaisseauScout (Vaisseau):
    def __init__(self, parent, p):
        Vaisseau.__init__(self, parent, p, dictVaisseaux.VaisseauScout) 
        self.indiceExploration = 5
        self.type = "Scout"
        
    def explorerPlanete (self, planete):
        if (planete.etat == None):
            if (self.indiceExploration == 0):
                self.indiceExploration = 5
                self.parent.listePlaneteExplorer.append(planete)
                return false #Arrete l'exploration
            else:
                self.indiceExploration -= 1
                return true #Continue l'exploration
        else:
            pass