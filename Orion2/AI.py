# -*- encoding: ISO-8859-1 -*-
#Logique g�n�rale
#
#
#If(objectif == true)
#    --> on fait l'objectif
#    
#elif(under Attack == true)
#    --> on r�agit, suspend l'objectif en cours
#    
#elif(objectif == false)
#    --> d�terminer un objectif: 3 types: --�conomique
#                                         --offensif
#                                         --defensif
# Construire ou am�liorer une batisse: 10 choix
# Construire un vaisseau: 12 choix
# Attaquer une cible
# Explorer




#                                         
#        dans �conomique: --produire unit� d'exploration
#                         --explorer
#                         --contruire portspatial
#                         
#        offensif:        --construire portSpatial/labo
#                         --production vaisseau
#                         --radar
#                         
#        defensif:        --radar
#                         --satelitte de d�fense
#                         --port spatial
#                         --production unit�










import random 
from Joueur import *

class Ia(Joueur):
    def __init__(self,parent,nom,e,p,couleur): 
        #c = comportement 10 = aggressif. 1 = passif.
        self.comportements = random.randrange(4,8);
        #self.energieConsommer =  self.calculEnergieUtilise()
        
        #valeur utilisee pour prendre decision selon race, et random.
        self.valeurDecisionnelleC = 0 #valeur decisionnelle de construction.
        self.valeurDecisionnelleG = 0
        self.valeurDecisionnelleP = 0
        self.valeurDecisionnelleE = 0
        
        self.commencer = True #au debut, l'AI execute quelques methodes de maniere
        #automatique pour ne pas qu'il tombe sans argent.
        
        
        
        
        
        print ("je suis n�e AI!")
        self.commencer() 
           
    def commencer(self):
        print("AI commence")
        if self.commencer == True:
            #if(self.energie < calculEnergieUtilise()+20):
                #if(validerConstruction(stationEnergie)):
                    #self.construire(stationEnergie)
            #else:
            print("AI commence")
            if(self.validerConstruction(stationExtraction("minerai"))): #la string en parametre permet d'identifier si extraction
                self.construire(stationExtraction("minerai"))           #de gas ou de minerai
                self.commencer == false
        
        else:
            self.decisiongenerale()  
                              
#         elif():
#             for i in self.planetesColonisees:
#                 if stationExtraction("minerai") in i.batisses:  #for j in i.batisses:
#                     pass
#                 else:

    def decisiongenerale(self):
        self.valeurDecisionnelleG = random.randrange(1,10)
        
        print("AI decision generale")
        
        if self.valeurDecisionnelleG <= 3:
            self.decisionConstruction()
        
        elif self.valeurDecisionnelleG <= 7 and self.valeurDecisionnelleG > 3:
            self.decisionProductionUnite()
        
        elif self.valeurDecisionnelleG <= 9 and self.valeurDecisionnelleG > 7:
            self.decisionExploration()
            
        elif self.valeurDecisionnelleG == 10:
            self.decisionColonisation()
            
        
    def decisionExploration(self):
        
        print("AI decide d'explorer")
        pass
    
#         for i in self.possessions["vaisseau"]:
#             if i.nom == VaisseauScout:
#                 vaisseau = True;
#                 
#         if vaisseau == True:
            
        
        
        
            
            #for i in self.possessions["vaisseau"]:
#              if int(idSuiveur) == i.id:
#                  i.deplacementVersAutreVaisseau( vaisseauSuivi,
    
    def decisionProductionUnite(self):
        self.valeurDecisionnelleP = random.randrange(1,8)
        
        print ("AI produit un unit")
        
        if self.valeurDecisionnelleP == 1:
            if self.validerProduction(VaisseauLaser):
                print("Ai produit un vaisseauLaser")
                self.portSpatiale.construireVaisseau(VaisseauLaser)
            
        elif self.valeurDecisionnelleP == 2:
            if self.validerProduction(VaisseauMissile):
                print("Ai produit un vaisseauMissile")
                self.portSpatiale.construireVaisseau(VaisseauMissile)
                
        elif self.valeurDecisionnelleP == 3:
            if self.validerProduction(VaisseauFurtif):
                print("Ai produit un vaisseauFurtif")
                self.portSpatiale.construireVaisseau(VaisseauFurtif)
        
        elif self.valeurDecisionnelleP == 4:
            if self.validerProduction(VaisseauMitraillette):
                print("Ai produit un vaisseauMitraillette")
                self.portSpatiale.construireVaisseau(VaisseauMitraillette)
        
        elif self.valeurDecisionnelleP == 5:
            if self.validerProduction(VaisseauCroiseur):
                print("Ai produit un vaisseauLCroiseur")
                self.portSpatiale.construireVaisseau(VaisseauCroiseur)
                
        elif self.valeurDecisionnelleP == 6:
            if self.validerProduction(VaisseauSniper):
                print("Ai produit un vaisseauSniper")
                self.portSpatiale.construireVaisseau(VaisseauSniper)
                
        elif self.valeurDecisionnelleP == 7:
            if self.validerProduction(VaisseauAmiral):
                print("Ai produit un vaisseauAmiral")
                self.portSpatiale.construireVaisseau(VaisseauAmiral)
                
        elif self.valeurDecisionnelleP == 8:
            if self.validerProduction(VaisseauBombardier):
                print("Ai produit un vaisseauBombardier")
                self.portSpatiale.construireVaisseau(VaisseauBombardier)
        
        
    
    
    def decisionConstruction(self):
        # 0 = passif | 400 = agressif 
        self.decision = nonprise;
        self.valeurDecisionnelleC = random.randrange(1,50);
        self.valeurDecisionnelleC = self.valeurDecisionnelle*self.comportements
        
        #tr�s passif        
        if(self.valeurDecisionnelleC <= 100):
            
            if self.verificationEnergie(self.energie):
                self.decision = prise
            #le param�tre est pour pr�venir le manque d'�nergie futur. 
            #Pour ne pas qu'on construise une batisse puis manquer d'energie immediatement apres.
            
            #si le joueur n'a qu'une planete. Doit construire portSpatial avant qu'il est atteint
            #la limite de construction. Sinon... fourrer!
            #if decision == nonprise:
            elif(self.verificationBug()):
                self.decision = prise
            
            #construire une mine
            if self.decision == nonprise:
                self.constructionMine()
                self.decision = prise    
                
        #relativement passif      
        elif(self.valeurDecisionnelleC <= 200 and self.valeurDecisionnelle > 100):
        # 0 = passif | 400 = agressif 
        #verifier energie
            if self.verificationEnergie(20):
                self.decision = prise
            
            #if decision == nonprise:
            #verifier si port spatial present avec 0 espaces.
            elif(self.verificationBug()):
                self.decision = prise
                
            #algorithme de calcul de batiment de nature militaire/economique    
            elif(self.decision != prise):
                self.batimentTotal = self.nbBatimentTotal()
                self.nbBatimentEconomique = self.nbBatEconomique()
                self.nbBatimentMilitaire = self.nbPortSpatial()
                if(self.nbBatimentEconomique/self.batimentTotal < 0.4):
                    #objectif = construire mine
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
                        
                #regarde si portSpatial sur chaque plan�te, sinon construit un port spatial sur une Plan�te X.
                elif(self.decision != prise):
                    self.totalPlan = 0
                    for i in self.planetesColonisees:
                        self.totalPlan =  self.totalPlan + 1
                    if(self.totalPlan > self.nbBatimentMilitaire):
                        if(self.validerConstruction(portSpaciale)):
                            self.construireBatisse(portSpaciale,planetei)
                            self.decision == prise
                            
                elif(self.decision != prise):
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
                                               
         #relativement aggressif   
        elif(self.valeurDecisionnelleC <= 300 and self.valeurDecisionnelle > 200):
        # 0 = passif | 400 = agressif 
        #verifier energie
            if self.verificationEnergie(20):
                self.decision = prise
            
            #if decision == nonprise:
            #verifier si port spatial present et verifie l'espace.
            elif(self.verificationBug()):
                self.decision = prise    
                
                #algorithme de calcul de batiment de nature militaire/economique    
            elif(self.decision != prise):
                self.batimentTotal = self.nbBatimentTotal()
                self.nbBatimentEconomique = self.nbBatEconomique()
                self.nbBatimentMilitaire = self.nbPortSpatial()
                if(self.nbBatimentEconomique/self.batimentTotal < 0.3):
                    #objectif = construire mine
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
                        
                #regarde si portSpatial sur chaque plan�te, sinon construit un port spatial sur une Plan�te X.
                elif(self.decision != prise):
                    self.totalPlan = 0
                    for i in self.planetesColonisees:
                        self.totalPlan =  self.totalPlan + 1
                    if(self.totalPlan > self.nbBatimentMilitaire):
                        if(self.validerConstruction(portSpaciale)):
                            self.construireBatisse(portSpaciale,planetei)
                            self.decision == prise
                            
                elif(self.decision != prise):
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
                
        elif(self.valeurDecisionelleC <= 400 and self.valeurDecisionnelle > 300):
        # 0 = passif | 400 = agressif 
        #verifier energie
            if self.verificationEnergie(20):
                self.decision = prise
            
            #if decision == nonprise:
            #verifier si port spatial present et verifie l'espace.
            elif(self.verificationBug()):
                self.decision = prise    
                
                #algorithme de calcul de batiment de nature militaire/economique    
            elif(self.decision != prise):
                self.batimentTotal = self.nbBatimentTotal()
                self.nbBatimentEconomique = self.nbBatEconomique()
                self.nbBatimentMilitaire = self.nbPortSpatial()
                if(self.nbBatimentEconomique/self.batimentTotal < 0.2):
                    #objectif = construire mine
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
                        
                #regarde si portSpatial sur chaque plan�te, sinon construit un port spatial sur une Plan�te X.
                elif(self.decision != prise):
                    self.totalPlan = 0
                    for i in self.planetesColonisees:
                        self.totalPlan =  self.totalPlan + 1
                    if(self.totalPlan > self.nbBatimentMilitaire):
                        if(self.validerConstruction(portSpaciale)):
                            self.construireBatisse(portSpaciale,planetei)
                            self.decision == prise
                            
                elif(self.decision != prise):
                    if(self.validerConstruction(StationExtraction("minerai"))):
                        self.construireBatisse(StationExtraction("minerai"),planetei)
                        self.decision == prise
    
    
#     for i in self.possessions["vaisseau"]:
#              if int(idSuiveur) == i.id:
#                  i.deplacementVersAutreVaisseau( vaisseauSuivi, "Vaisseau" )
    
    def nbPortSpatial(self):
        total = 0
        for i in self.possessions["batiment"]:
            if(i.type == PortSpaciale):
                total = total + 1
        return total
                
    def nbBatEconomique(self):
        total = 0
        for i in self.possessions["batiment"]:
            if(i.type == StationExtraction):
                total = total + 1
        return total
    
    def construireBatisse (self,par):#quand c'est une station d'extraction, choisir le type
        idPlanete,batisse,paramBatisse = par#type du batiment
        for i in self.possessions["planetesColonisees"]:
            if i.id == int(idPlanete):
                if self.validerConstruction(batisse, planete):
                    i.batisses.append(self.dictBatisses[batisse](self,i,paramBatisse))
                    return true
                else:
                    return false
    
    def validerProduction(self, vaisseau):
        
        if vaisseau.coutMinerai > self.minerai or vaisseau.coutGas > self.gas:
            return false
        
        
            
    def validerConstruction(self,batisse, planete): 
        #pour le moment, la variable prix represente le cout en minerai.
        if(batisse.prix > self.minerai):
            return false
        #passer la planete en parametre aiderait beaucoup.
        
        #Methode pour valider s'il y a assez d'espace sur une planete pour construire. 
        #ca devrait marcher, sinon belle boucle for dans la liste batisses.
        total = self.planetesColonisees.batisse.__sizeof__()
        #tailleMaximale n'existe pas, faudrait faire une variable. Sinon remplacer par 10.
        if(total >= self.planete.tailleMaximale):
            return false
        return true   
    
        
    def verificationEnergie(self,energiePreventive):
        
        if(self.energie < self.calculEnergieUtilise()+energiePreventive):
                if(self.validerConstruction(stationEnergie)):
                    self.construire(stationEnergie)
                    return true
        else:
            return false
                    
    def verificationBug(self):
        total = 0;
        #si le joueur n'a qu'une planete. Doit construire portSpatial avant qu'il est atteint 
        #la limite de construction. Sinon... fourrer!
        for j in self.planetesColonisee:
                    if portSpaciale in j.batisses:
                        pass
                    else:
                        if self.planetesColonisees__sizeof__() == 1:
                            for i in self.planetesColonisees.batisses:
                                total += total 
                            if total >= 8: #la limite etant 10. a 9 il doit absoluement construire un port spatiale.
                                if(self.validerConstruction(portSpaciale)):
                                    construire(portSpaciale)
                                    return true
                                
        return false
        
    def calculEnergieUtilise(self):
        total = 0
        
        for i in self.possessions["batiment"]:
            total = total + i.coutEnergie  
            
        return total      
        
        
        
    
    
                
            
            
    