# -*- encoding: ISO-8859-1 -*-
################################################
#  Le petit Larousse du b�timent non illustr�  #
################################################

Capitale = {"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 20}

StationExtraction ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 100}

StationEnergie={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 200}

PortSpaciale ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 150}

SatelliteDefense ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 300}

Radar ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 20}

GenerateurChampForce ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 200}

Laboratoire ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 20}

Temple ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 20}

Colisee ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 20}
