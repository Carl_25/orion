# -*- encoding: ISO-8859-1 -*-
import random
        
class Planete(object):
    def __init__(self,parent,x,y,t,id):
        self.parent=parent
        self.x=x
        self.y=y
        self.taille=t
        self.id = id
        self.minerai=500000
        self.gas=500000
        self.energie=random.randrange(self.taille)*1000000
        self.artefacts={"batiment":[],
                       "extracteur":[]
                       }
        self.etat="vide"
        self.range=50
        self.dictPositions={}#pas besoin vu qu'on passe les x et y en param?
        self.proprio=""
        self.explore = False
        self.artefact=0
        self.hp=0
        self.dommage=0
        self.batisses=[]
        self.tag="planete"
        self.boulcierExistant = False
        self.bouclier = 0
                
    def prochaineAction(self):#appel� par le joueur
        if len(self.batisses) > 0:
            for i in self.batisses:
                i.prochaineAction()
    
    def calculHp(self):
        for i in self.batisses:
            self.hp +=i.vie
            if self.hp <= 0:
                self.parent.parent.joueurs[proprio].defaite = True
        if self.boulcierExistant:
            print (self.hp,"la vie")
            self.hp += self.bouclier
            
        self.hp -= self.dommage

    
        
    def repair(self):
        pass 
