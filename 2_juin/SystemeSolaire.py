#NOUVEAU SYSTEME SOLAIRE
# -*- encoding: ISO-8859-1 -*-
import random
from Planete import *
import math

class SystemeSolaire(object):
    def __init__(self,parent,id,x,y):
        self.parent=parent
        self.id=id#c'est le i du du modele, ca donne le tag
        self.proprio=""
        self.x=x
        self.y=y
        self.largeur = 600
        self.hauteur =600
        self.taille=random.randrange(100)+10
        self.planetes=[]#liste de planetes
        self.creePlanetes()
        self.nom = ""
        self.nbGates = 0
        self.gates = []
                
    def creePlanetes(self):        
        #nbSystemesSolaires = self.parent.getListeJoueurs.size() * 3
        nbPlanetes = random.randrange(5,20)
        #Boucle selon le nombre de systemes solaires a creer
        for i in range (nbPlanetes):
            #Si la liste de systemes solaires est vide
            if len(self.planetes)==0:
                #Donne un x et y random
                x = random.randrange(15, self.largeur - 15)
                y = random.randrange(15, self.hauteur - 15)
                self.parent.ids = self.parent.unID.nextId(self.parent.ids)
                #cr�e une taille at random
                tmin=random.randrange(3,5)
                tmax=random.randrange(5,12)
                planete = Planete (self,x, y,50,self.parent.ids)
                self.planetes.append(planete)
            #Si la liste des systemes solaires n'est pas vide
            else:
                valide = False
                #Continu a chercher une position tant que le systeme solaire ne
                #se trouve pas a au moins 30 pixels des autres systemes solaires
                while (valide == False):
                    x = random.randrange(15, self.largeur - 15)
                    y = random.randrange(15, self.hauteur - 15)
                    valide = self.calculDistance(x, y)
                    if (valide):
                        self.parent.ids = self.parent.unID.nextId(self.parent.ids)
                        tmin=random.randrange(3,5)
                        tmax=random.randrange(5,12)
                        t = random.randrange(tmin,tmax)
                        planete = Planete (self,x, y,t,self.parent.ids)
                        self.planetes.append(planete)
                        
    def calculDistance (self, x, y): 
        for i in self.planetes: 
            #Calcule distance entre les deux systemes solaires 
            distance = math.sqrt((x - i.x)**2 + (y - i.y)**2) 
            if (distance < 30): 
                return False
        return True

