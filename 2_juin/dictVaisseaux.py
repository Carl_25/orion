######################################################
#VAISSEAUX DE COMBAT
######################################################
VaisseauLaser = {"coutMinerai": 100,
                  "coutGas": 25,
                  "vie": 200,
                  "tempsProd":10,
                  "puissance": 10,
                  "range": 100,
                  "vitesseAttaque": 10,
                  "vitesse":30   }

VaisseauMissile = {"coutMinerai": 100,
                  "coutGas": 25,
                  "vie": 175,
                  "tempsProd":10,
                  "puissance": 15,
                  "range": 150,
                  "vitesseAttaque": 7,
                  "vitesse":30   }

VaisseauFurtif  = {"coutMinerai": 175,
                   "coutGas": 50,
                   "vie": 225,
                   "tempsProd":10,
                   "puissance": 20,
                   "range": 80,
                   "vitesseAttaque": 7,
                   "vitesse":30   }

VaisseauMitraillette = {"coutMinerai": 100,
                        "coutGas": 100,
                        "vie": 250,
                        "tempsProd":10,
                        "puissance": 20,
                        "range": 80,
                        "vitesseAttaque": 15,
                        "vitesse": 20, }

VaisseauCroiseur = {  "coutMinerai": 200,
                      "coutGas": 70,
                      "vie": 300,
                    "tempsProd":10,
                       "puissance": 20,
                        "range": 80,
                        "vitesseAttaque": 7,
                        "vitesse": 10,  }

VaisseauSniper =      { "coutMinerai": 300,
                        "coutGas": 0,
                        "vie": 75,
                  "tempsProd":10,
                          "puissance": 20,
                          "range": 200,
                          "vitesseAttaque": 7,
                          "vitesse":30   }

VaisseauAmiral =          {"coutMinerai": 400,
                          "coutGas": 100,
                          "vie": 500,
                          "tempsProd":10,
                          "puissance": 20,
                          "range": 80,
                          "vitesseAttaque": 7,
                          "vitesse":10   }

VaisseauBombardier = {"coutMinerai": 500,
                      "coutGas": 200,
                      "vie": 600,
                      "tempsProd":10,
                      "puissance": 60,
                          "range": 300,
                          "vitesseAttaque": 7,
                          "vitesse":30   }

#############################################################
#VAISSEAUX DE BASE
#############################################################

VaisseauCargo = {     "coutMinerai": 150,
                      "coutGas": 50,
                      "vie": 500,
                      "tempsProd":10,
                      "capaciteMax": 100,
                      "capacite": 0,
                      "vitesse": 10    }

VaisseauDiplomate = { "coutMinerai": 10,
                      "coutGas": 0,
                      "vie": 10,
                    "tempsProd":10,
                      "vitesse": 10    }

VaisseauScout = {     "coutMinerai": 10,
                      "coutGas": 0,
                      "vie": 10,
                      "tempsProd":10,
                      "vitesse": 10    }

