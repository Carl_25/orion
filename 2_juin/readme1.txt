Orion

Configurations Support�es:
PC:
-Python 3.2 , avec Pyro
-Python 3.3 , avec Pyro
OSX:
-Python 3.3 , avec Pyro et Tix d'install�


Comment Jouer:
-double-cliquer sur le fichier Controleur.py
-Avant de se connecter ou de cr�er un r�seau local, il faut choisir obligatoirement une race, le nom est optionel 
	(attribution de nom automatique si le champ est non modifi�.)
-Pour se connecter � un r�seau local existant, cliquer sur l'adresse ip dans la fen�tre de droite
-Zone d'attente des joueurs


Informations sur le jeu:
-Sys�t�me Solaire: pour voir sa base initiale et les gates
-Univers: voir tous les syst�mes solaires.  Double-cliquer sur un syst�me solaire pour y entrer
		
-Plan��te: Construire b�tisses, unit�s (lorsqu'un port spatial est construit,
					on peut alors constuire
					diff�rents vaisseaux),
				ramasser des ressources, etc
-Gates: permet de se promener d'un syst��me solaire � un autre.


Fonctionnalit�s:
-Gestion des cr�ations de r�seau local, disposition automatique des r�seaux cr�es
-