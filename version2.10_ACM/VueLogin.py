# -*- encoding: ISO-8859-1 -*-
from tkinter.font import *
from tkinter import *
import random


class VueLogin():
    def __init__(self, parent, serveurList, monip):
        self.parent = parent
        self.root= self.parent.root
        self.root.title("Orion")
        
        self.largeur = self.root.winfo_screenwidth()-400
        self.hauteur = self.root.winfo_screenheight()-400
        self.root.geometry("%dx%d+0+0" % (self.largeur, self.hauteur))
        self.root.resizable(0, 0)
        
        self.cadreActif=0
        self.canevasCourant=None
        self.serveurList=serveurList
        self.monip = monip
        self.connecte = False
        self.creerliste = False
        self.leServeur=None
        self.Civ=None
        self.popupExiste = 0
        self.nom="DefaultName" + str(random.randrange(0, 100, 1))
        self.leip=" "
        self.cadreLogin= CadreCustom(self.root)
        self.creeCadreLogin()
        
    #MarcAndre - Modification
    def creeCadreLogin(self):
        self.lCiv = ('Jedi', 'Space squids', 'Robots')
        self.cadreActif = self.cadreLogin

        # Le logo du jeu
        self.img_Banniere = PhotoImage(file="image/banniereLogin.gif")  
        self.banniereLogin = EtiquetteCustom(self.cadreLogin, image = self.img_Banniere )
        self.banniereLogin.configure(background='black') 
        self.banniereLogin.pack()

        # Titre
        self.root.title('Orion - Login')

        #combobox des civilisations
        self.lf1 = CadreEtiquetteCustom(self.cadreLogin, text="�tape 1. Choix d'une civilisation")
        self.lbChoixCiv = Listbox(self.lf1, height=5, width=20, selectmode=SINGLE)
        self.lbChoixCiv.pack(side=LEFT, fill=BOTH, expand=1)
        self.s1 = Scrollbar(self.lf1, orient=VERTICAL, command=self.lbChoixCiv.yview)
        self.s1.pack(side=RIGHT, fill=Y)
        self.lbChoixCiv['yscrollcommand'] = self.s1.set
        self.lf1.pack(fill=BOTH, expand=YES, side=LEFT, padx=10, pady=10)
        self.lbChoixCiv.bind('<<ListboxSelect>>', self.on_select_civilisation)
       
        #LabelFrame pour entrer le nom du joueur
        self.lf2 = CadreEtiquetteCustom(self.cadreLogin, text="�tape 2. Choix d'une nom (optionnel)")
        
        # Bouton cr�er serveur
        self.demarrerPartie=BoutonCustom(self.lf2, text="D�marrer une partie", command=self.creerServeur, padx = 100, pady = 20)
        self.demarrerPartie.pack(padx=5, pady=10, side = BOTTOM)
        
        # Champ
        self.l_champEtindication = Label(self.lf2, bg = "black")
        self.l_champEtindication.pack( padx=5, pady=10)
        
        self.labelChampNom = EtiquetteCustom( self.l_champEtindication, text = "Votre nom de joueur :")
        self.labelChampNom.pack(padx=5, pady=10, side=LEFT)
        
        self.champNomJoueur = Entry(self.l_champEtindication )
        self.champNomJoueur.pack(padx=5, pady=10, side=LEFT)   
        self.lf2.pack(fill=BOTH, side=LEFT, padx=10, pady=10)     
        
        
        #combobox des serveurs
        self.lf3 = CadreEtiquetteCustom(self.cadreLogin, text="Se connecter � un serveur")
        self.lbServIP = Listbox(self.lf3, height=5, selectmode=SINGLE)
        self.lbServIP.pack(side=LEFT, fill=BOTH, expand=1)
        self.s = Scrollbar(self.lf3, orient=VERTICAL, command=self.lbServIP.yview)
        self.s.pack(side=RIGHT, fill=Y)
        self.lbServIP['yscrollcommand'] = self.s.set
        self.lf3.pack(fill=BOTH, expand=YES, side=LEFT, padx=10, pady=10)
        self.lbServIP.bind('<<ListboxSelect>>', self.on_select)

        # Remplir le ListBox
        self.listeDesServeurs()

        # Remplir le ListBox de civilisations
        for i in self.lCiv:
            self.lbChoixCiv.insert('end', i)

    def listeDesServeurs(self):
        # Remplir le ListBox
        #if self.creerliste == True:
        #    print("je loop")
        #    self.lbServIP.delete(0, 'end')
        #self.creerliste = True
        try:
            print("try try")
            self.lbServIP.delete(0, 'end')
        except:
            pass
            
        for i in self.serveurList:
            self.lbServIP.insert('end',  i)
            
        if self.connecte == False:
            #self.serveurList=""
            self.serveurList=self.parent.parent.serveurActif()
            print(self.serveurList)
            self.parent.root.after(1000,self.listeDesServeurs)

    def on_select(self, event):#on se connecte a un server
        self.parent.serverLocal=False
        iServ = event.widget.curselection()[0]
        Serv = self.lbServIP.get(iServ)
        self.leServeur = Serv
        self.leip = Serv
        print("la selection", Serv)
        if self.Civ:
            self.connecterServeur(Serv)

    def on_select_civilisation(self, event):
        print("Vue on_select_civilisation")
        iCiv = event.widget.curselection()[0]
        Civ = self.lbChoixCiv.get(iCiv)
        print ("Index: " + iCiv)
        print ("Valeur: " + Civ)
        self.Civ = Civ

    def creerServeur(self):#on est le server
        self.connecte = True
        # Changer pour un nom random 
        self.parent.serverLocal=True
        #self.nom=("testNom" + str(random.randrange(0, 100, 1)))
        ip=self.monip
        self.leServeur = ip
        if self.Civ:
            self.parent.creerServeur()
            self.parent.creerLobby(self.monip)

    #MarcAndre - Modification
    def connecterServeur(self, ip):
        self.connecte = True
        print("4-Login", ip)
 
        # Un nom personnalis�  (si le joueur en a �crit un)
        # Sinon, nom al�atoire
        if ( len(self.champNomJoueur.get()) > 0 ):
            self.nom = self.champNomJoueur.get()

        if self.nom:
            self.parent.inscritClient(ip)


    # MarcAndre - Modification
    def popupCorrigerNom(self, parent, leip, civ):
        self.popupCorriger = Toplevel()
        self.popupExiste = 1
        self.parentPopupCorriger = parent
        self.leIpASeRappeler = leip
        self.LaCivASeRappeler = civ
            
        self.f_popup = CadreEtiquetteCustom(self.popupCorriger, text = "Ce nom existe d�j� : Veuillez entrer un nouveau nom SVP")
        self.f_popup.grid()
        self.labelEntrerNom = EtiquetteCustom(self.f_popup, text="Nouveau nom :")
        self.labelEntrerNom.grid( row = 0, column = 0 )
            
        self.leNom = ""
        self.champNom = Entry( self.f_popup, textvariable = self.leNom )
        self.champNom.grid( row = 0, column = 1 )
            
        self.boutonOK = BoutonCustom( self.f_popup , text = "Soumettre")
        self.boutonOK.grid( row = 1, column = 0)
            
        self.champNom.focus_get()
            
        self.boutonOK.bind("<Button-1>", self.envoyerNouvNom )

        
    #MarcAndre - Modification   
    def envoyerNouvNom(self, evt):
        if self.validationNom():
            self.parentPopupCorriger.inscritClient( self.leNom , self.leIpASeRappeler, self.LaCivASeRappeler)

    def validationNom(self):
        self.leNom = self.champNom.get()
        self.leNom.strip()
        if len(self.leNom) > 0:
            return True;
        else:
            return False;
    
    def detruirePopup(self):
        self.popupExiste = 0
        self.popupCorriger.destroy()
 
         
# Les sous-classes pour une interface personnalis�e       
class BoutonCustom(Button):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)

class EtiquetteCustom(Label):
    def __init__(self,parent, **kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="black"
        Label.__init__(self,parent,**kw)
        
class CadreCustom(Frame):
    def __init__(self,parent,**kw):
        kw["bg"]="black"
        Frame.__init__(self,parent,**kw)
        
class CadreEtiquetteCustom(LabelFrame):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="black"
        kw["padx"]=10
        kw["pady"]=5
        kw["relief"]=GROOVE
        LabelFrame.__init__(self,parent,**kw)
 
        
