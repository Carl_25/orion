#-*- coding: latin-1 -*- 
from tkinter import *
import tkinter.tix as Tix 
import math 
from Joueur import *
from SystemeSolaire import *
from Vaisseau import *
from Modele import *

  
  
class VuePartie (): 
    def __init__ (self, parent, x, y, root): 
        self.parent = parent 
        self.x = x  
        self.y = y 
        self.modele = self.parent.parent.modele 
        self.courant = "" 
        self.nVieCap = "" 
        self.nArgent = "" 
        self.nArtefacts = "" 
        self.listeNomJoueurs = self.modele.joueurs.keys()
        self.couleurJoueurCourant=""
        self.couleurCanvasJoueur = ""
        self.timer = 0
        
        self.ressource = "gas"
            
        #Aller chercher les infos du joueur courant 
        for i in self.listeNomJoueurs: 
            if self.modele.joueurs[i].nom == self.parent.parent.nom: 
                self.nVieCap = self.modele.joueurs[i].possessions["planetesColonisees"][0].hp 
                self.nArgent = self.modele.joueurs[i].gold 
                self.nArtefacts = len(self.modele.joueurs[i].artefactsDecouverts) 
                  
        self.nomJoueurCourant = self.parent.parent.nom 
          
        self.selSurCanevas = "" 
        self.idVSel = "" 
        self.idPlSel = "" 
        self.nomBatSel = "" 
        self.idBatSel = "" 
        self.nomVSel = "" 
        self.listeIdBat = [] 
        self.etatSysteme = self.nomJoueurCourant
                  
        self.frameAfficher = False
          
        self.cptExt = 0
        self.cptEne = 0
        self.cptSpa = 0
        self.cptDef = 0
        self.cptLab = 0
        self.cptTem = 0
        self.cptCol = 0
        self.cptBout = 1
        self.cptGen = 0
        self.cptRad = 0
          
        self.root = root 
        self.root.title("ORION - Better than anything") 
  
        self.larg = math.floor(self.root.winfo_screenwidth()) * 0.75
        self.haut = math.floor(self.root.winfo_screenheight()) * 0.75  
        
        self.tl_StationExtraction = None 
          
#-------------------------------------------------------------- 
# D�claration des conteneurs 
#-------------------------------------------------------------- 
          
        #Pour l'onglet plan�te 
        self.pw_Planete = PanedWindow(self.root, orient=HORIZONTAL) 
        self.pw_Planete.columnconfigure(0, weight = 6) 
        self.pw_Planete.columnconfigure(1, weight = 1) 
        self.pw_Planete.pack(fill=BOTH, expand=1) 
          
        self.f_Gauche = Frame (self.pw_Planete, bg = "white", width = self.x*0.75) 
        self.f_Droit = Frame (self.pw_Planete, bg = "black", width = self.x*0.25) 
          
  
        #Pour L'onglet Univers 
        self.f_Univers = LabelFrame ( self.f_Gauche, width = 900, height = self.y*0.9, text = "Onglet Univers" ) 
        self.c_Univers = Canvas(self.f_Univers, bg = "black", width = self.x*0.70, height = self.y*0.90) 
          
        #Pour l'onglet planete 
        self.f_Planete = Frame (self.f_Gauche, bg = "white",  width = 1200, height = 1200) 
          
        #Pour l'onglet SystSol 
        self.f_SystSol = Frame(self.f_Gauche, width=700, height=self.y*0.9) 
        #, bg = "blue" 
          
                 
        self.c_SystSol = Tix.Canvas (self.f_SystSol, bg = "black", bd=0, highlightthickness = 0) 
           
      
      
        #Pour le Message Log 
        self.f_MLog = LabelFrame (self.f_Droit, bg = "gray", text = "Messages") 
  
        #Pour l'overview 
        self.f_overview = LabelFrame (self.f_Droit, bg = "black",fg = "white", width = (self.x*0.25*0.25), height = (self.y*0.25*0.25), text = "Overview")  
        #Pour s�lection courante 
        self.f_selection = LabelFrame (self.f_Droit, bg = "gray", width = (self.x*0.25*0.25), height = (self.y*0.50*0.5), text = "S�lection") 
          
        #Pour le frame planete 
        self.f_CatBatisses = LabelFrame (self.f_Planete, text = "B�tisses existantes") 
        self.f_AfficherBatisses = LabelFrame (self.f_Planete, text = "B�tisses possibles") 
        self.f_BatissesDeCetteCat = LabelFrame (self.f_Planete, text = "B�tisses de cette cath�gorie") 
        self.frameCourant = self.f_AfficherBatisses 
        """ 
        self.tl_finDePartie = Tix.Toplevel() 
        self.tl_quitter = Tix.Toplevel() 
        """
  
        #Frame du bas pour les boutons 
        self.f_Boutons = Frame (self.f_Gauche, bg = "gray") 
        self.b_quitter = Tix.Button (self.f_Boutons, text = "Quitter") 
        self.b_SystSol = Tix.Button (self.f_Boutons, text = "Syst�me Solaire")    
        self.b_Univers = Tix.Button (self.f_Boutons, text = "Univers") 
        self.b_Planete = Tix.Button (self.f_Boutons, text = "Plan�te") 
          
        self.canvasCouleur = Canvas(self.f_overview, width = 20, height = 20)
#---------------------------------------------------------------- 
# D�claration des labels pour les vaisseaux 
#---------------------------------------------------------------- 
  
        self.l_VScout = Label (self.f_Gauche, text = "Vaisseau Scout") 
        self.l_VDiplomate = Label (self.f_Gauche, text = "Vaisseau Diplomate") 
        self.l_VCargo = Label (self.f_Gauche, text = "Vaisseau Cargo") 
        self.l_VLaser = Label (self.f_Gauche, text = "Vaisseau Laser") 
        self.l_VMissile = Label (self.f_Gauche, text = "Vaisseau Missile") 
        self.l_VFurtif = Label (self.f_Gauche, text = "Vaisseau Furtif") 
        self.l_VMig = Label (self.f_Gauche, text = "Vaisseau MIG") 
        self.l_VCroiseur = Label (self.f_Gauche, text = "Vaisseau Croiseur") 
        self.l_VSniper = Label (self.f_Gauche, text = "Vaisseau Sniper") 
        self.l_VAmiral = Label (self.f_Gauche, text = "Vaisseau Amiral") 
        self.l_VBombardier = Label (self.f_Gauche, text = "Vaisseau Bombardier") 
          
#---------------------------------------------------------------- 
#D�claration des labels pour l'onglet Planete 
#---------------------------------------------------------------- 
          
        self.l_TitrePlanete = Label ( self.f_Planete, text = "Plan�te ", font = '"Times" 18') 
        self.l_NomPlanete = Label (self.f_Planete, text = self.idPlSel, font = '"Times" 18' ) 
          
          
        self.b_ConstuireBatisse = Button ( self.f_Planete, text = "Construire" ) 
        self.b_AutoDestruciton = Button ( self.f_Planete, text = "AutoDestruction" ) 
     
#---------------------------------------------------------------- 
# D�claration des labels pour le MessageLog 
#----------------------------------------------------------------  
        # Chat
        
        self.listBox_messageJoueurs = Listbox( self.f_MLog, selectmode=SINGLE, width = 60 )
        self.listBox_messageJoueurs.grid( row = 0, column = 0, sticky = E+W)
        
        self.scroll_messageJoueurs = Scrollbar(self.f_MLog, orient=VERTICAL, command=self.listBox_messageJoueurs.yview )
        self.scroll_messageJoueurs.grid( row = 0, column = 1, sticky = N+S)

        self.listBox_messageJoueurs.config( yscrollcommand = self.scroll_messageJoueurs.set )
        
        self.unMessage = StringVar()
        self.champMessageJoueur = Entry( self.f_MLog, textvariable = self.unMessage )
        self.champMessageJoueur.grid( row = 1, column = 0, sticky = E+W, pady = 5 )
        
        self.champMessageJoueur.bind('<Return>', self.envoyerMessage )
          
#---------------------------------------------------------------- 
# D�claration des labels pour Overview 
#----------------------------------------------------------------  
        self.l_TitrePtsVieCap = Label ( self.f_overview, text = "Points de vie de la capitale: ", fg = "white", bg = "black") 
        self.l_TitrePop = Label ( self.f_overview, text = "Population: ", fg = "white", bg = "black" ) 
        self.l_Hab = Label ( self.f_overview, text = " habitants", fg = "white", bg = "black" ) 
        self.l_TitreArgent = Label ( self.f_overview, text = "Argent: ", fg = "white", bg = "black" ) 
        self.l_SigneDePiastre = Label ( self.f_overview, text = "$", fg = "white", bg = "black" ) 
        self.l_TitreArtefacts = Label ( self.f_overview, text = "Artefacts trouv�s: ", fg = "white", bg = "black") 
        self.l_TitreRessources = Label ( self.f_overview, text = "Ressources amass�es: ", fg = "white", bg = "black" ) 
        self.l_TitreNiveau = Label ( self.f_overview, text = "Niveau: ", fg = "white", bg = "black") 
        self.l_TitreMoral = Label ( self.f_overview, text = "Moral: ", fg = "white", bg = "black") 
        self.l_TitreTechno = Label ( self.f_overview, text = "Technologie: ", fg = "white", bg = "black")
        ###############################################################################################
          
        #********************************************************************* 
        #    Les labels qui contiennent l_U[...] sont les labels � updater 
        #********************************************************************* 
        self.l_UPtsVieCap = Label ( self.f_overview, text = self.nVieCap ) 
        self.l_UArgent = Label ( self.f_overview, text = self.nArgent ) 
        self.l_UArtefacts = Label ( self.f_overview, text = self.nArtefacts ) 
          
#-------------------------------------------------------------- 
# D�claration des boutons pour les batisses 
#-------------------------------------------------------------- 
        self.b_BCapitale = Button(self.f_CatBatisses, text = "Capitale") 
        self.b_BExtraction = Button(self.f_CatBatisses, text = "Station d'extraction") 
        self.b_BPortSpaciale = Button(self.f_CatBatisses, text = "Port Spatial") 
        self.b_BStationEnergie = Button(self.f_CatBatisses, text = "Station �nergie") 
        self.b_BSatelliteDefense = Button(self.f_CatBatisses, text = "Satellite d�fense") 
        self.b_BRadar = Button(self.f_CatBatisses, text = "Radar") 
        self.b_BGenerateur = Button(self.f_CatBatisses, text = "G�n�rateur de champ de force") 
        self.b_BLabo = Button(self.f_CatBatisses, text = "Laboratoire") 
        self.b_BTemple = Button(self.f_CatBatisses, text = "Temple") 
        self.b_BColisee = Button(self.f_CatBatisses, text = "Colisee") 
          
        self.l_QteExt = Label (self.f_CatBatisses, text = self.cptExt) 
        self.l_QteEne = Label (self.f_CatBatisses, text = self.cptEne) 
        self.l_QteSpa = Label (self.f_CatBatisses, text = self.cptSpa) 
        self.l_QteDef = Label (self.f_CatBatisses, text = self.cptDef) 
        self.l_QteLab = Label (self.f_CatBatisses, text = self.cptLab) 
        self.l_QteTem = Label (self.f_CatBatisses, text = self.cptTem) 
        self.l_QteCol = Label (self.f_CatBatisses, text = self.cptCol)  
        self.l_QteGen = Label (self.f_CatBatisses, text = self.cptGen) 
        self.l_QteRad = Label (self.f_CatBatisses, text = self.cptRad) 
          
        self.l_Qte = Label(self.f_CatBatisses, text = "X")     
          
        self.b_Cap = Button(self.f_AfficherBatisses, text="Capitale") 
        self.b_Ext = Button(self.f_AfficherBatisses, text = "Station d'extraction") 
        self.b_PortSp = Button(self.f_AfficherBatisses, text = "Port Spatial") 
        self.b_StationEn = Button(self.f_AfficherBatisses, text = "Station �nergie") 
        self.b_SatDef = Button(self.f_AfficherBatisses, text = "Satellite d�fense") 
        self.b_Radar = Button(self.f_AfficherBatisses, text = "Radar") 
        self.b_Gen = Button(self.f_AfficherBatisses, text = "G�n�rateur de champ de force") 
        self.b_Labo = Button(self.f_AfficherBatisses, text = "Laboratoire") 
        self.b_Temple = Button(self.f_AfficherBatisses, text = "Temple") 
        self.b_Colisee = Button(self.f_AfficherBatisses, text = "Colisee") 
          
#---------------------------------------------------------------- 
# D�claration des labels pour S�lection courante 
#----------------------------------------------------------------     
        self.l_TitreSelectionCourante = Label (self.f_selection, text = "S�LECTION") 
        self.l_TitreSelectionCourante.grid(row = 0, column = 0) 
          
        self.l_SelectionCourante = Label(self.f_selection) 
        #self.l_Vai = Label (self.f_selection) 
        #self.l_PlSel = Label ( self.f_selection) 
        
        self.b_Coloniser = Button (self.f_selection, text = "Coloniser")
          
          
  
#---------------------------------------------------------------- 
# Grid 
#----------------------------------------------------------------  
        #self.l_TitreOverview.grid(row = 0, column = 0, rowspan = 8, columnspan = 2) 
        self.f_Gauche.rowconfigure(0, weight = 4) 
        self.f_Gauche.rowconfigure(1, weight = 0) 
        self.f_Gauche.columnconfigure(0, weight = 5) 
        self.f_Gauche.columnconfigure(1, weight = 0) 
        self.f_Droit.rowconfigure(0, weight = 1) 
        self.f_Droit.rowconfigure(1, weight = 1) 
        self.f_Droit.rowconfigure(2, weight = 3) 
        self.f_Droit.columnconfigure(0, weight = 1) 
          
        self.f_MLog.grid(row = 0, column = 0, sticky = N+S+E+W) 
        self.f_overview.grid(row=1, column = 0, sticky = N+S+E+W) 
        self.f_selection.grid(row=2, column = 0, sticky = N+S+E+W) 
        self.b_quitter.grid(row=0, column=0) 
        self.b_SystSol.grid (row=0, column=3) 
        self.b_Univers.grid(row = 0, column=2) 
        self.b_Planete.grid(row = 0, column = 4)         
        self.f_Boutons.grid(row=1, sticky = N+S+W+E) 
          
        # apr�s avoir .pack() les labels et boutons, on fait le add du frame 
        self.pw_Planete.add(self.f_Gauche, width = 700) 
        self.pw_Planete.add(self.f_Droit) 
        
        self.canvasCouleur.grid(row = 0, column = 0)
        self.l_TitrePtsVieCap.grid(row=1, column=0, sticky=W)
        self.l_TitrePop.grid(row=2, column=0, sticky=W)
        self.l_TitreRessources.grid(row=3, column=0, sticky=W)
        self.l_TitreMoral.grid(row=4, column=0, sticky=W)
        self.l_TitreArtefacts.grid(row=5, column=0, sticky=W)
         
        ######################################################################
        self.label_PtsDeVie = Label (self.f_overview, fg = "white", bg = "black")
        self.label_PtsDeVie.grid(row=1, column=1, sticky=E)
        #####################################################################
        self.label_pop = Label (self.f_overview, fg = "white", bg = "black")
        self.label_pop.grid(row=2, column=1, sticky=E)
        #####################################################################
        self.label_ressources = Label (self.f_overview, fg = "white", bg = "black")
        self.label_ressources.grid(row=3, column=1, sticky=E)
        #####################################################################
        self.label_moral = Label (self.f_overview, fg = "white", bg = "black")
        self.label_moral.grid(row=4, column=1, sticky=E)
        #####################################################################
        self.label_nbArtefacts = Label (self.f_overview, fg = "white", bg = "black")
        self.label_nbArtefacts.grid(row=5, column=1, sticky=E) 
  
          
#---------------------------------------------------------------- 
# Binding 
#----------------------------------------------------------------  
        self.b_SystSol.bind("<ButtonRelease>", self.drawSystemeSolaire) 
        self.b_Univers.bind("<ButtonRelease>", self.drawUnivers) 
        self.b_Planete.bind("<ButtonRelease>", self.drawOngletPlanete) 
          
        self.c_SystSol.bind("<Button-1>", self.cliquerCanevas)         
        self.c_SystSol.bind("<Button-3>", self.cliquerCanevas) 
          
        self.c_Univers.bind("<Double-Button-1>", self.cliquerCanevas) 
          
        self.b_AutoDestruciton.bind("<ButtonRelease>", self.boutonAutoDestr) 
        self.b_ConstuireBatisse.bind("<ButtonRelease>", self.constructionBatisse) 
        self.b_quitter.bind("<ButtonRelease>", self.quitterPartie) 
        # Chat
        self.champMessageJoueur.bind('<Return>', self.envoyerMessage )
        self.b_Coloniser.bind("<ButtonRelease>", self.coloniserPlanete)
    
        
    # Chat
    def envoyerMessage(self, evt):
        if self.validationMessage() :
            self.parent.parent.envoyerMsg( self.unMessage )
    
    # Chat
    def validationMessage(self):
        self.unMessage= self.champMessageJoueur.get()
        self.unMessage.strip()
        if len(self.unMessage) > 0:
            return True;
        else:
            return False;
        
    def coloniserPlanete (self, evt):
        if self.timer > 60 or self.timer <= 0:
            self.timer = 1
            for i in self.modele.listeSystemesSolaires:
                for j in i.planetes:
                    if str(j.id) == str(self.idPlSel):
                        j.explore = True
                        j.proprio = self.nomJoueurCourant
            self.parent.parent.coloniserPlanete(self.idPlSel)
         
    def drawPlanetes (self, x, y, idP, proprio, expl, mere, coul): 
        if mere == True: 
            self.c_SystSol.create_oval(x - 25, y -25, x + 25, y + 25, fill = coul, tags = ("planete", str(idP), str(proprio), str(expl))) 
        else: 
            self.c_SystSol.create_oval(x - 5, y -5, x + 5, y + 5, fill = "white", tags = ("planete", str(idP), str(proprio), str(expl))) 
  
    def drawSystemeSolaire (self, evt): 
        
        if evt.widget == self.c_Univers:
            pass
        else:
            idSysteme = None
            for i in self.modele.listeSystemesSolaires: 
                if i.proprio == self.nomJoueurCourant:
                    idSysteme = i.id
            self.modele.joueurs[self.nomJoueurCourant].sysSolaireVue = idSysteme
        
        if self.courant == "univers": 
            self.f_Univers.grid_forget() 
        elif self.courant == "planete": 
            self.f_Planete.grid_forget() 
            
        print(self.modele.joueurs[self.nomJoueurCourant].sysSolaireVue)
          
        self.courant = "systeme"
        
        self.f_SystSol.rowconfigure(0, weight = 3) 
        self.f_SystSol.rowconfigure(1, weight = 0) 
        self.f_SystSol.columnconfigure(0, weight = 3) 
        self.f_SystSol.columnconfigure(1, weight=0) 
        
        self.f_SystSol.grid(row=0, column=0, sticky=N+S+W+E) 
        self.c_SystSol.grid(row = 0, column = 0, sticky=N+S+W+E) 
        
        
        idSys = 0
    
        
        for i in self.modele.listeSystemesSolaires: 
            n = 1
            if i.proprio == self.etatSysteme:
                for j in i.planetes:
                    if n == 1: 
                        self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, True, "yellow" ) 
                        n = n+1
                    else: 
                        self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, False, "white")
                    self.drawGates(i.gates) 
        for i in self.listeNomJoueurs:
            if idSys==0:
                if i == self.nomJoueurCourant:
                    idSys = self.modele.joueurs[i].systemeSolaire.id
            for j in self.modele.joueurs[i].possessions["vaisseau"]:
                if j.systemeSolaire.id == idSys:
                    self.drawVaisseaux(j, i, self.modele.joueurs[i].couleur, j.type)
            #******Redessiner les attaques******#          
            for k in self.modele.joueurs[i].listeAttaquesFaites :  
                    #******Attaque Laser******#         
                if k.attackType == "laser" : 
                    self.drawLasers ( k.vaisseauAttaquantID, k.departX, k.departY, k.targetX , k.targetY, k.attackColor )   
                    self.modele.joueurs[k].listeAttaquesFaites.remove(k)
  
    def drawGates(self, gates): 
        self.c_SystSol.delete("gate") 
        for i in gates: 
            x=i.x 
            y=i.y 
            r=i.range
            e=i.etat%10
            i.etat = i.etat + 1
            self.c_SystSol.create_oval(x-r, y-r, x+r, y+r, fill = "red", tags=("gate",)) 
            self.c_SystSol.create_oval(x-r+e, y-r+e, x+r-e, y+r-e, outline = "yellow", tags=("gate",)) 

    def drawUnivers (self, evt): 

        if self.courant == "systeme": 
            self.f_SystSol.grid_forget() 
        elif self.courant == "planete": 
            self.f_Planete.grid_forget() 

        self.courant = "univers"
        self.f_Univers.grid(row = 0, column = 0) 
        self.c_Univers.grid(row = 0, column = 0) 

        for i in self.modele.listeSystemesSolaires: 
            largeurModele = self.modele.largeur 
            hauteurModele = self.modele.hauteur 
            x=i.x/largeurModele*500
            y=i.y/hauteurModele*500
            self.c_Univers.create_oval(x-10, y-10, x, y, fill = "white", tags = ("Galaxie", str(i.id), str(i.proprio)))


    def drawOngletPlanete (self, evt): 
        if self.courant == "univers": 
            self.f_Univers.grid_forget() 
        elif self.courant == "systeme": 
            self.f_SystSol.grid_forget() 

        self.f_AfficherBatisses.destroy()

        if self.frameCourant == self.f_BatissesDeCetteCat: 
            self.f_BatissesDeCetteCat.forget()
            
        

        self.courant = "planete"

        self.f_BatissesDeCetteCat.destroy() 
        self.f_BatissesDeCetteCat = LabelFrame (self.f_Planete, text = "B�tisses de cette cath�gorie") 

        self.frameCourant = self.f_AfficherBatisses
        
        self.f_Planete.grid(row = 0, column = 0, sticky=N+S+E+W) 
         
        self.f_Planete.rowconfigure(0, weight = 0) #Titre & nom plan�te 
        self.f_Planete.rowconfigure(1, weight = 0) #Cath�gorie:  
        self.f_Planete.rowconfigure(2, weight = 6) #Frame g�n�ral 
        self.f_Planete.rowconfigure(3, weight = 1) #boutons 
          
        self.f_Planete.columnconfigure(0, weight = 1) #Titre 
        self.f_Planete.columnconfigure(1, weight = 2) 
        self.f_Planete.columnconfigure(2, weight = 3) 
          
        self.l_NomPlanete.config(text = self.idPlSel) 
        self.l_TitrePlanete.grid(row = 0, column = 0) 
        self.l_NomPlanete.grid(row = 0, column =1, sticky = W)   
        self.b_ConstuireBatisse.grid(row = 3, column = 1) 
        self.b_AutoDestruciton.grid (row = 3, column = 2) 
        self.f_CatBatisses.rowconfigure(0, weight = 1) 
        self.f_CatBatisses.rowconfigure(1, weight = 1) 
        self.f_CatBatisses.rowconfigure(2, weight = 1) 
        self.f_CatBatisses.rowconfigure(3, weight = 1) 
        self.f_CatBatisses.rowconfigure(4, weight = 1) 
        self.f_CatBatisses.rowconfigure(5, weight = 1) 
        self.f_CatBatisses.rowconfigure(6, weight = 1) 
        self.f_CatBatisses.rowconfigure(7, weight = 1) 
        self.f_CatBatisses.rowconfigure(8, weight = 1) 
        self.f_CatBatisses.rowconfigure(9, weight = 1) 
           
        self.afficherLeFrame()   
    

                
        self.f_CatBatisses.grid(row = 2, column = 0, sticky = N+S+E+W) 
        
        
        if self.frameAfficher: 
                  
            self.f_AfficherBatisses = LabelFrame (self.f_Planete, text = "B�tisses possibles")
            
            self.b_Ext = Button(self.f_AfficherBatisses, text = "Station d'extraction") 
            self.b_PortSp = Button(self.f_AfficherBatisses, text = "Port Spatial") 
            self.b_StationEn = Button(self.f_AfficherBatisses, text = "Station �nergie") 
            self.b_SatDef = Button(self.f_AfficherBatisses, text = "Satellite d�fense") 
            self.b_Radar = Button(self.f_AfficherBatisses, text = "Radar") 
            self.b_Gen = Button(self.f_AfficherBatisses, text = "G�n�rateur de champ de force") 
            self.b_Labo = Button(self.f_AfficherBatisses, text = "Laboratoire") 
            self.b_Temple = Button(self.f_AfficherBatisses, text = "Temple") 
            self.b_Colisee = Button(self.f_AfficherBatisses, text = "Colisee") 

            self.b_BCapitale.grid(row = 0, column = 0) 
            self.b_BCapitale.bind("<Button-1>", self.afficherDetails) 
            self.l_Qte.grid(row = 0, column = 1) 
            l_QteCap = Label (self.f_CatBatisses, text = "1") 
            l_QteCap.grid(row = 0, column = 2) 
              
            self.b_Ext.grid(row = 2, column = 0, padx = 10, pady = 20) 
            self.b_Ext.bind("<Button-1>", self.reponse) 
            self.b_PortSp.grid(row = 3, column = 0, padx = 10, pady = 20) 
            self.b_PortSp.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_StationEn.grid(row = 4, column = 0, padx = 10, pady = 20) 
            self.b_StationEn.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_SatDef.grid(row = 5, column = 0, padx = 10, pady = 20) 
            self.b_SatDef.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_Radar.grid (row = 1, column = 1, padx = 10, pady = 20) 
            self.b_Radar.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_Gen.grid(row = 2, column =1, padx = 10, pady = 20) 
            self.b_Gen.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_Labo.grid(row = 3, column =1, padx = 10, pady = 20) 
            self.b_Labo.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_Temple.grid(row = 4, column=1, padx = 10, pady = 20) 
            self.b_Temple.bind("<Button-1>", self.selectionnerBatisse) 
            self.b_Colisee.grid(row = 5, column=1, padx = 10, pady = 20) 
            self.b_Colisee.bind("<Button-1>", self.selectionnerBatisse) 
            self.f_AfficherBatisses.grid(row=2, column = 1, sticky = N+S+E+W)
    
            
            
    def afficherLeFrame(self):
        self.frameAfficher = False
        
        for i in self.listeNomJoueurs:
            if i == self.nomJoueurCourant:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    if str(j.id) == str(self.idPlSel):
                        self.frameAfficher = True
                        self.drawCategories()   
                        
             
              
    def selectionnerBatisse (self, evt): 
        self.nomBatSel = evt.widget.cget("text") 
              
    def drawCategories (self): 

        
        for i in self.listeNomJoueurs: 
            if self.modele.joueurs[i].nom == self.nomJoueurCourant: 
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]: 
                    if int(j.id) == int(self.idPlSel): 
                        for k in j.batisses: 
                            if k.type == "StationExtraction": 
                                if self.cptExt == 1: 
                                    self.b_BExtraction.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteExt.config(text = self.cptExt) 
                                    self.l_QteExt.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BExtraction.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptExt = self.cptExt+1
                                else: 
                                    self.l_QteExt.config(text = self.cptExt-1) 
                                      
                            elif k.type == "PortSpaciale": 
                                if self.cptSpa == 1: 
                                    self.b_BPortSpaciale.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteSpa.config(text = self.cptSpa) 
                                    self.l_QteSpa.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BPortSpaciale.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptSpa = self.cptSpa+1
                                else: 
                                    self.l_QteSpa.config(text = self.cptSpa-1) 
                                self.listeIdBat.append(k.id)    
  
                            elif k.type == "StationEnergie": 
                                if self.cptEne ==1: 
                                    self.b_BStationEnergie.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteEne.config(text = self.cptEne) 
                                    self.l_QteEne.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BStationEnergie.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptEne=self.cptEne+1
                                else: 
                                    self.l_QteEne.config(text = self.cptEne-1) 
  
                            elif k.type == "SatelliteDefense": 
                                if self.cptDef == 1: 
                                    self.b_BSatelliteDefense.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteDef.config(text = self.cptDef) 
                                    self.l_QteDef.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BSatelliteDefense.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptDef = self.cptDef+1
                                else: 
                                    self.l_QteDef.config(text = self.cptDef-1) 
                              
                            elif k.type == "Radar": 
                                if self.cptRad == 1: 
                                    self.b_BRadar.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    l_QteRad.config(text = self.cptRad) 
                                    l_QteRad.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BRadar.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptRad=self.cptRad+1
                                else: 
                                    self.l_QteRad.config(text = self.cptRad-1) 
  
                            elif k.type == "GenerateurChampForce": 
                                if self.cptGen == 1: 
                                    self.b_BGenerateur.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteGen.config(text = self.cptGen) 
                                    self.l_QteGen.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BGenerateur.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptGen=self.cptGen+1
                                else: 
                                    self.l_QteGen.config(text = self.cptGen-1) 
  
                            elif k.type == "Laboratoire": 
                                if self.cptLab == 1: 
                                    self.b_BLabo.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteLab.config(text = self.cptLab) 
                                    self.l_QteLab.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BLabo.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptLab=self.cptLab+1
                                else: 
                                    self.l_QteLab.config(text = self.cptLab-1) 
  
                            elif k.type == "Temple": 
                                if self.cptTem == 1: 
                                    self.b_BTemple.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteTem.config(text = self.cptTem) 
                                    self.l_QteTem.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BTemple.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptTem=self.cptTem+1
                                else: 
                                    self.l_QteTem.config(text = self.cptTem-1) 
  
                            elif k.type == "Colisee": 
                                if self.cptCol == 1: 
                                    self.b_BColisee.grid(row = self.cptBout-1, column = 0) 
                                    self.l_Qte.grid(row = self.cptBout-1, column = 1) 
                                    self.l_QteCol.config(text = self.cptCol) 
                                    self.l_QteCol.grid(row = self.cptBout-1, column = 2) 
                                    self.b_BColisee.bind("<ButtonRelease>", self.afficherDetails) 
                                    self.cptCol=self.cptCol+1
                                else: 
                                    self.l_QteCol.config(text = self.cptCol-1) 
                              
     
    def afficherDetails(self, evt): 
          
        self.f_BatissesDeCetteCat.destroy() 
        self.f_BatissesDeCetteCat = LabelFrame (self.f_Planete, text = "Batisses de cette cat�gorie") 
          
        self.frameCourant.forget() 
        self.frameCourant = self.f_BatissesDeCetteCat 
          
        self.afficherLesBoutons(evt) 
  
          
          
    def afficherLesBoutons(self, evt):    
          
        if str(evt.widget.cget("text")) == "Capitale": 
            batisse = "Capitale"
        elif str(evt.widget.cget("text")) == "Station d'extraction": 
            batisse = "StationExtraction"
        elif str(evt.widget.cget("text")) == "Port Spatial": 
            batisse = "PortSpaciale"
        elif str(evt.widget.cget("text")) == "Station �nergie": 
            batisse ="StationEnergie"
        elif str(evt.widget.cget("text")) == "Satellite d�fense": 
            batisse ="SatelliteDefense"
        elif str(evt.widget.cget("text")) == "Radar": 
            batisse = "Radar"
        elif str(evt.widget.cget("text")) == "G�n�rateur de champ de force": 
            batisse ="GenerateurChampForce"
        elif str(evt.widget.cget("text")) == "Laboratoire": 
            batisse ="Laboratoire"
        elif str(evt.widget.cget("text")) == "Temple": 
            batisse ="Temple"
        elif str(evt.widget.cget("text")) == "Colisee": 
            batisse ="Colisee"
              
        self.nomBatSel = evt.widget.cget("text") 
          
        #Fak il faut afficher un bouton pour chaque  
        #batisse de cette sorte 
        for i in self.listeNomJoueurs: 
            if self.modele.joueurs[i].nom == self.nomJoueurCourant: 
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]: 
                    if int(j.id) == int(self.idPlSel): 
                        compteur = 0
                        for k in j.batisses: 
                            if str(k.type) == batisse: 
                                compteur = compteur +1
                                b_Batisse = Button (self.f_BatissesDeCetteCat, text = compteur) 
                                if compteur < 9: 
                                    b_Batisse.grid(row = compteur, column = 1) 
                                else: 
                                    b_Batisse.grid(row = compteur-8, column = 3) 
                                b_Batisse.bind("<Button-1>", self.selectionnerUneBatisse) 
        b_Retour = Button (self.f_BatissesDeCetteCat, text = "Retour") 
        b_Retour.grid(row = compteur +1, column = 2) 
        b_Retour.bind("<Button-1>", self.drawOngletPlanete) 
        self.f_BatissesDeCetteCat.grid(row=2, column = 1, sticky = N+S+E+W)         
          
    def drawVaisseaux (self, v, j, coul, type): 
        self.c_SystSol.create_polygon(v.x, v.y, v.x+15, v.y+15, v.x, v.y-10, fill= coul, tags = ("vaisseau", str(v.id), str(j), str(type))) 
        self.c_SystSol.create_rectangle(v.x, v.y + 20, v.x + (v.vie/5), v.y + 25, fill="green", tag="vieVaisseau") 
  
  
  
    def cliquerCanevas (self, evt): 
          
        if self.courant == "systeme": 
            self.selCanevasSystSol(evt) 
        elif self.courant == "planete": 
            self.selCanevasPlanete(evt) 
        elif self.courant == "univers": 
            self.selCanevasUnivers(evt) 
              
    def selCanevasSystSol(self, evt): 
          
        """ 
        ATTN: 
        t[0] = "vaisseau", "planete", etc 
        t[1] = id de l'objet 
        t[2] = id du joueur � qui appartient l'objet 
        t[3] = type de vaisseau, si plan�te bool si exploree ou pas 
        """
        self.b_Coloniser.grid_remove()
          
        #Est-ce que j'ai quelque chose en mains 
        #VAISSEAU en mains 
        if self.selSurCanevas == "V": 
            t = self.c_SystSol.gettags(CURRENT) 
               
            #O� est-ce que j'ai cliqu� 
            #Cliqu� dans le vide 
            if not t: 
                #Pour bouger, on clique bouton 3: 
                if evt.num == 3: 
                    self.parent.parent.bougerVaisseau( self.idVSel, evt.x, evt.y ) 
                elif evt.num == 1: 
                    self.selSurCanevas = "" 
                    self.l_SelectionCourante.config(text = "") 
                    self.idVSel = "" 
            #Cliqu� sur un vaisseau 
            elif t[0] == "vaisseau": 
                #Pour bouger, on clique sur bouton3: 
                if evt.num == 3: 
                    #J'envoie au contr�leur (id de mon vaisseau, id vaisseau � suivre) 
                    self.parent.parent.poursuivreObjet( self.idVSel, t[1] ) 
                elif evt.num == 1: 
                    self.selSurCanevas = "" 
                    self.l_SelectionCourante.config(text = "") 
                    self.l_SelectionCourante.grid() 
                    self.idVSel = "" 
            #Cliqu� sur une plan�te 
            elif t[0] == "planete":
                #Clic gauche 
                if evt.num == 1: 
                    #Est-ce que c'est une plan�te � moi? 
                    if t[2] == self.nomJoueurCourant: 
                        self.idPlSel = t[1] 
                        self.l_SelectionCourante.config(text = "Ma plan�te: "+t[1]) 
                        self.l_SelectionCourante.grid() 
                        self.selSurCanevas="" 
                        self.idVSel="" 
                    elif t[2] != self.nomJoueurCourant: 
                        if t[3] == "True":
                            self.l_SelectionCourante.config(text = "Plan�te colonis�e: " + t[1])
                        else:
                            self.l_SelectionCourante.config(text = "Plan�te non colonis�e: " + t[1])
                            self.b_Coloniser.grid(row = 7, column = 0)
                        #Seulement montrer les infos planete 
                        self.selSurCanevas="P"
                        self.idVSel="" 
                        self.idPlSel = t[1] 
                        self.l_SelectionCourante.grid() 
                        
                #Clic droit 
                elif evt.num == 3: 
                    #C'est pour attaquer une planete ennemie 
                    if t[2] != self.nomJoueurCourant: 
                        #ANTOINE: �a me prend cette m�thode dans le contr�leur 
                        self.parent.parent.poursuivreObjet(self.idVSel, t[1]) 
                    elif t[2] == self.nomJoueurCourant: 
                        #C'est � moi, qu'est-ce que je veux faire avec �a? 
                        #Mode d�placement... 
                        pass
                      
        #RIEN en mains 
        elif self.selSurCanevas == "": 
            t = self.c_SystSol.gettags(CURRENT) 
               
            #O� est-ce que j'ai cliqu� 
            #Dans le vide 
            if not t: 
                pass
            #Cliqu� sur un vaisseau 
            elif t[0] == "vaisseau": 
                #Si le vaisseau est � moi: 
                if t[2] == self.nomJoueurCourant: 
                    self.selSurCanevas = "V"
                    self.l_SelectionCourante.config(text = "Mon vaisseau:  " + t[3]) 
                    self.l_SelectionCourante.grid() 
                    self.idVSel = t[1] 
                #Si le vaisseau n'est pas � moi 
                elif t[2] != self.nomJoueurCourant: 
                    self.l_SelectionCourante.config(text = "Vaisseau ennemi: " + t[3]) 
                    self.l_SelectionCourante.grid() 
              
            #Cliqu� sur une plan�te 
            elif t[0] == "planete":
                self.selSurCanevas = "P"
                self.idPlSel = t[1] 
                #Si c'est une plan�te � moi 
                if t[2] == self.nomJoueurCourant: 
                    self.l_SelectionCourante.config(text = "Ma Plan�te " + t[1]) 
                    self.l_SelectionCourante.grid() 
                #Si la plan�te n'est pas � moi 
                if t[2] != self.nomJoueurCourant: 
                    #Est-ce que la plan�te a �t� explor�e par moi? 
                    if t[3] == "True": 
                        #Afficher les infos de la plan�te: 
                        self.l_SelectionCourante.config(text = "") 
                        self.l_SelectionCourante.config(text = "Une plan�te explor�e: " + t[1]) 
                        self.l_SelectionCourante.grid() 
                    #Non explor�e 
                    else: 
                        self.l_SelectionCourante.config(text = "") 
                        self.l_SelectionCourante.config(text = "Une plan�te n-explor�e: " + t[1]) 
                        self.l_SelectionCourante.grid() 
                        self.b_Coloniser.grid(row = 7, column = 0)
                          
        #Si j'ai une plan�te de s�lectionn�                 
        elif self.selSurCanevas == "P": 
            t = self.c_SystSol.gettags(CURRENT) 
              
            if evt.num == 1: 
                if not t: 
                    self.selSurCanevas = "" 
                    self.l_SelectionCourante.config(text = "") 
                    self.l_SelectionCourante.grid() 
                elif t[0] == "vaisseau": 
                    #Si le vaisseau est � moi: 
                    if t[2] == self.nomJoueurCourant: 
                        self.selSurCanevas = "V"
                        self.l_SelectionCourante.config(text = "Mon vaisseau:  " + t[3]) 
                        self.l_SelectionCourante.grid() 
                        self.idVSel = t[1] 
                    #Si le vaisseau n'est pas � moi 
                    elif t[2] != self.nomJoueurCourant: 
                        self.l_SelectionCourante.config(text = "Vaisseau ennemi: " + t[3]) 
                        self.l_SelectionCourante.grid() 
                elif t[0] == "planete":                   
                    self.selSurCanevas = "P"
                    self.idPlSel = t[1] 
                    #Si c'est une plan�te � moi 
                    if t[2] == self.nomJoueurCourant: 
                        self.l_SelectionCourante.config(text = "Ma Plan�te " + t[1]) 
                        self.l_SelectionCourante.grid() 
                        #Si la plan�te n'est pas � moi 
                    if t[2] != self.nomJoueurCourant: 
                        #Est-ce que la plan�te a �t� explor�e par moi? 
                        if t[3] == "True": 
                            #Afficher les infos de la plan�te: 
                            self.l_SelectionCourante.config(text = "") 
                            self.l_SelectionCourante.config(text = "Une plan�te explor�e: " + t[1]) 
                            self.l_SelectionCourante.grid() 
                        #Non explor�e 
                        else: 
                            self.l_SelectionCourante.config(text = "") 
                            self.l_SelectionCourante.config(text = "Une plan�te n-explor�e: " + t[1]) 
                            self.l_SelectionCourante.grid() 
                            self.b_Coloniser.grid(row = 7, column = 0)
                    
    def constrVaisseau (self, evt): 
        if evt.widget.cget("text") == "Vaisseau Scout": 
            self.nomVSel = "VaisseauScout"
        elif evt.widget.cget("text") == "Vaisseau Diplomate": 
            self.nomVSel = "VaisseauDiplomate"
        elif evt.widget.cget("text") == "Vaisseau Cargo": 
            self.nomVSel = "VaisseauCargo"
        elif evt.widget.cget("text") == "Vaisseau Laser": 
            self.nomVSel =  "VaisseauLaser"
        elif evt.widget.cget("text") == "Vaisseau Missile": 
            self.nomVSel =  "VaisseauMissile"
        elif evt.widget.cget("text") == "Vaisseau Furtif": 
            self.nomVSel =  "VaisseauFurtif"
        elif evt.widget.cget("text") == "Vaisseau MIG": 
            self.nomVSel =  "VaisseauMitraillette"
        elif evt.widget.cget("text") == "Vaisseau Croiseur": 
            self.nomVSel =  "VaisseauCroiseur"
        elif evt.widget.cget("text") == "Vaisseau Sniper": 
            self.nomVSel =  "VaisseauSniper"
        elif evt.widget.cget("text") == "Vaisseau Amiral": 
            self.nomVSel =  "VaisseauAmiral"
        elif evt.widget.cget("text") == "Vaisseau Bombardier": 
            self.nomVSel = "VaisseauBombardier"
              
        if len(self.listeIdBat) == 1: 
            self.idBatSel = self.listeIdBat[0] 
        if self.modele.ressourceDisponible(self.nomVSel):
            self.parent.parent.queuingVaisseau(self.idPlSel, self.idBatSel, self.nomVSel) 


    def selectionnerUneBatisse(self, evt):
        
        self.tl_batisse = Tix.Toplevel(width = 600, height = 900)
        #self.tl_batisse.minsize(500, 500)
    
        #########################
        #D�CLARATION DES FRAMES
        ########################
        self.f_batisse = Frame ( self.tl_batisse, width = 300, height = 500 ) #frame sur le Top-Level
        self.f_batimentChoisi = Frame ( self.f_batisse )
        self.f_stats = Frame ( self.f_batisse )
        self.f_vaisseaux = Frame ( self.f_batisse, height = 500 )
        self.f_boutons = Frame ( self.f_batisse )
        self.f_details = Frame ( self.f_batisse )
        """
        , width = 200, height = 600
        , width = 600, height = 600
        , width = 300, height = 600
        , width = 1000, height = 300
        """
        
        #########################
        #D�CLARATION DES labels
        ########################
        
        self.l_batimentChoisi = Label (self.f_batimentChoisi, text = "Batiment choisi : ") 
        self.l_stats = Label (self.f_stats, text = "") 
        self.l_detail = Label (self.f_details, text = "") 
        self.l_vaisseauxConstruits = Label (self.f_vaisseaux, text = "")
        self.l_vaisseauxDispos = Label (self.f_vaisseaux, text = "")
        
        ########################
        #D�CLARATION DES BOUTONS
        ########################
        self.b_upgrade = Button(self.f_boutons, text = "UPGRADE")
        #self.b_reparer =  Button(self.f_boutons, text = "REPARER")
        #self.b_vendre =  Button(self.f_boutons, text = "VENDRE")
        #self.b_arreter = Button(self.f_boutons, text = "ARRETER")
        
        #########################
        #CONFIGURE
        #########################
        self.f_batisse.rowconfigure(0, weight=1)
        self.f_batisse.rowconfigure(1, weight=10)
        self.f_batisse.rowconfigure(2, weight=3)
        self.f_batisse.columnconfigure(0, weight = 2)
        self.f_batisse.columnconfigure(1, weight = 6)
        self.f_batisse.columnconfigure(2, weight = 3)
        
        #########################
        #GRID des frames
        #########################
        self.f_batisse.grid(row=0, column=0, sticky = N+S+E+W)
        
        self.f_batimentChoisi.grid(row=0, column=0, sticky = N+S+E+W, columnspan=3)
        self.f_stats.grid(row=1, column=0, sticky = N+S+E+W)
        #self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
        self.f_boutons.grid(row=1, column=2, sticky = N+S+E+W)
        self.f_details.grid(row=2, column=0, sticky = N+S+E+W, columnspan=3)
        
        #########################
        #GRID des labels
        #########################
        self.l_batimentChoisi.grid(row = 0, column=0)
        self.l_stats.grid(row=0, column=0)
        self.l_vaisseauxConstruits.grid(row=0, column=0)
        self.l_vaisseauxDispos.grid(row=0, column=1)
        self.l_detail.grid(row=0, column=0)
        
        #########################
        #GRID des boutons
        #########################
        self.b_upgrade.grid(row=0, column=0)
        #self.b_reparer.grid(row=1, column=0)
        #self.b_vendre.grid(row=2, column=0)
        #self.b_arreter.grid(row=3, column=0)
        
        #--------------------------------------------------------------
        # D�claration des boutons pour les vaisseaux
        #--------------------------------------------------------------

        self.b_VScout = Tix.Button(self.f_vaisseaux, text = "Vaisseau Scout")
        self.b_VDiplomate= Tix.Button(self.f_vaisseaux, text = "Vaisseau Diplomate")
        self.b_VCargo= Tix.Button(self.f_vaisseaux, text = "Vaisseau Cargo")
        self.b_VLaser= Tix.Button(self.f_vaisseaux, text = "Vaisseau Laser")
        self.b_VMissile= Tix.Button(self.f_vaisseaux, text = "Vaisseau Missile")
        self.b_VFurtif= Tix.Button(self.f_vaisseaux, text = "Vaisseau Furtif")
        self.b_VMig= Tix.Button(self.f_vaisseaux, text = "Vaisseau MIG")
        self.b_VCroiseur= Tix.Button(self.f_vaisseaux, text = "Vaisseau Croiseur")
        self.b_VSniper= Tix.Button(self.f_vaisseaux, text = "Vaisseau Sniper")
        self.b_VAmiral= Tix.Button(self.f_vaisseaux, text = "Vaisseau Amiral")
        self.b_VBombardier= Tix.Button(self.f_vaisseaux, text = "Vaisseau Bombardier")
        
        
        
        #------------------------------------------------------------
        #        Conditions d'affichage
        #------------------------------------------------------------
        if self.nomBatSel == "Port Spatial":
            
            self.l_qteVaisseauxConstruits = Label (self.f_vaisseaux, text = "Quantit� de vaisseaux construits: ")
            self.l_vPourConstruire = Label (self.f_vaisseaux, text = "Vaisseaux existant:")
            qteConstruits = 0
            
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "PortSpaciale":
                            qteConstruits = k.compteurId

            self.l_qteVaisseaux = Label (self.f_vaisseaux, text = qteConstruits)
            

            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)     
            
            self.l_qteVaisseauxConstruits.grid(row = 0, column = 0, sticky = N+S+E+W)
            self.l_qteVaisseaux.grid(row = 0, column = 1, sticky = N+S+E+W)
            self.l_vPourConstruire.grid(row = 2, column = 0, sticky = N+S+E+W)
            self.b_VAmiral.grid (row = 2, column = 1, sticky = N+S+E+W)
            self.b_VBombardier.grid(row = 2, column = 2, sticky = N+S+E+W)
            self.b_VCargo.grid (row = 3, column = 1, sticky = N+S+E+W)
            self.b_VCroiseur.grid(row = 3, column = 2, sticky = N+S+E+W)
            self.b_VDiplomate.grid(row = 4, column = 1, sticky = N+S+E+W)
            self.b_VFurtif.grid(row = 4, column = 2, sticky = N+S+E+W)
            self.b_VLaser.grid (row = 5, column = 1, sticky = N+S+E+W)
            self.b_VMig.grid(row = 5, column = 2, sticky = N+S+E+W)
            self.b_VMissile.grid(row = 6, column = 1, sticky = N+S+E+W)
            self.b_VSniper.grid(row = 6, column = 2, sticky = N+S+E+W)
            self.b_VScout.grid(row = 7, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)                                                    
            
            self.b_VAmiral.bind("<Button-1>", self.constrVaisseau)
            self.b_VScout.bind("<Button-1>", self.constrVaisseau)
            self.b_VDiplomate.bind("<Button-1>", self.constrVaisseau)
            self.b_VCargo.bind("<Button-1>", self.constrVaisseau)
            self.b_VLaser.bind("<Button-1>", self.constrVaisseau)
            self.b_VMissile.bind("<Button-1>", self.constrVaisseau)
            self.b_VFurtif.bind("<Button-1>", self.constrVaisseau)
            self.b_VMig.bind("<Button-1>", self.constrVaisseau)
            self.b_VCroiseur.bind("<Button-1>", self.constrVaisseau)
            self.b_VSniper.bind("<Button-1>", self.constrVaisseau)
            self.b_VBombardier.bind("<Button-1>", self.constrVaisseau)
        #FIN if "Port Spatial"
        
        elif self.nomBatSel == "Capitale":
        #self.idBatSel
            #===================================================================
            # for i in self.listeNomJoueurs:
            # #******Redessiner les vaisseaux******#     
            #     for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
            #        for k in j.batisse:
            #            si ta batisse id = id batisse selectionne
            #                Va chercher les infos d'entsour
            #===================================================================
            #variableAllie1 = (Capitale...)
            #l_ListeAllies = Label (self.f_vaisseaux, text = "ton texte")
            
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Capitale" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            l_listeAlliers = Label (self.f_vaisseaux , text = "liste des alli�s")
            
            
            l_listeAlliers.grid(row =0, column=0, sticky = N+S+E+W)
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            
            listbox_Allie = Listbox(self.f_vaisseaux)
            listbox_Allie.grid(row =1, column=0, sticky = N+S+E+W)
                
            listbox_Allie.insert(END, "")   
            
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].listeAllie:
                    listbox_Allie.insert(END, j)
                    #self.varListeJoueursRencontres = k.listeJoueurRencontres
                    #self.varJoueurVassal = k.joueurVassal
                                
            
            #===================================================================
            # listeAllie
            # listeJoueursRencontres
            # joueurVassal
            #===================================================================
        elif self.nomBatSel == "Station d'extraction":
            #===================================================================
            # sortes de minerai extract�s
            # qte minerai extract�s par cette batisse
            #===================================================================
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Station d'extraction" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_RessourceEnMinage = Listbox(self.f_vaisseaux)
            listbox_RessourceEnMinage.grid(row =1, column=0, sticky = N+S+E+W)
                 
            listbox_RessourceEnMinage.insert(END, "Ressource en minage")
            #########################################################################
            listbox_TypeRessource = Listbox(self.f_vaisseaux)
            listbox_TypeRessource.grid(row =1, column=1, sticky = N+S+E+W)
                 
            listbox_TypeRessource.insert(END, "Type de ressource")      

            
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "StationExtraction":
                            listbox_RessourceEnMinage.insert(END, k.ressourceEnMinage)
                            listbox_TypeRessource.insert(END, k.typeDeRessource)  
                         
        elif evt.widget.cget("text") == "Station �nergie":
            #===================================================================
            # scrapp� pour l'instant'
            #===================================================================
            pass
        elif self.nomBatSel == "Satellite d�fense":
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Satellite d�fense" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_range = Listbox(self.f_vaisseaux)
            listbox_range.grid(row =1, column=0, sticky = N+S+E+W)
                 
            listbox_range.insert(END, "Range")
            #########################################################################
            listbox_damage = Listbox(self.f_vaisseaux)
            listbox_damage.grid(row =1, column=1, sticky = N+S+E+W)
                 
            listbox_damage.insert(END, "Damage") 
            
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "SatelliteDefense":
                            listbox_range.insert(END, k.range)
                            listbox_damage.insert(END, k.dmg)
                            
                                
            
            
        elif self.nomBatSel == "Radar":
            #===================================================================
            # range
            # vaisseaux dans son range
            #===================================================================
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Radar" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_range = Listbox(self.f_vaisseaux)
            listbox_range.grid(row =1, column=0, sticky = N+S+E+W)
                 
            listbox_range.insert(END, "Range")
            #########################################################################
            listbox_VaisseauxDetectes = Listbox(self.f_vaisseaux)
            listbox_VaisseauxDetectes.grid(row =1, column=1, sticky = N+S+E+W)
                 
            listbox_VaisseauxDetectes.insert(END, "Damage") 
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "Radar":
                            listbox_range.insert(END, k.range)
                            for a in k.vaisseauInRange:
                                listbox_VaisseauxDetectes.insert(END, a)
                                
        elif self.nomBatSel == "G�n�rateur de champ de force":
            #===================================================================
            # bouclier de vie total
            # bouclier actuel
            # taux de reg�n�ration
            #===================================================================
            l_typeBatisse = Label (self.f_batimentChoisi , text = "G�n�rateur de champ de force" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_bouclierVieTotal = Listbox(self.f_vaisseaux)
            listbox_bouclierVieTotal.grid(row =1, column=0, sticky = N+S+E+W)
            listbox_bouclierVieTotal.insert(END, "Bouclier de vie total")
            #########################################################################
            listbox_bouclierVie = Listbox(self.f_vaisseaux)
            listbox_bouclierVie.grid(row =1, column=1, sticky = N+S+E+W)
            listbox_bouclierVie.insert(END, "bouclier de vie")
            #####################################################################
            listbox_tauxRegen = Listbox(self.f_vaisseaux)
            listbox_tauxRegen.grid(row =1, column=2, sticky = N+S+E+W)
            listbox_tauxRegen.insert(END, "Taux de r�g�n�ration") 
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "GenerateurChampForce":
                            listbox_bouclierVieTotal.insert(END, k.bouclierVie)
                            listbox_bouclierVie.insert(END, k.bouclierActuel)
                            listbox_tauxRegen.insert(END, k.regeneration)
                            
        elif self.nomBatSel == "Laboratoire":
            #===================================================================
            # niveau
            # prochainVaisseauUnlock
            # rechercheEnCours
            #===================================================================
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Laboratoire" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_Niveau = Listbox(self.f_vaisseaux)
            listbox_Niveau.grid(row =1, column=0, sticky = N+S+E+W)
            listbox_Niveau.insert(END, "Niveau")
            #########################################################################
            listbox_rechercheEnCours = Listbox(self.f_vaisseaux)
            listbox_rechercheEnCours.grid(row =1, column=1, sticky = N+S+E+W)
            listbox_rechercheEnCours.insert(END, "Recherche")

            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "Laboratoire":
                            listbox_Niveau.insert(END, k.niveauLab)
                            if(k.rechercheEnCours == True):
                                listbox_rechercheEnCours.insert(END, "recherche en cours.......")
        elif self.nomBatSel == "Temple":
            #===================================================================
            # artefacts
            #===================================================================
            l_typeBatisse = Label (self.f_batimentChoisi , text = "Temple" )
            l_typeBatisse.grid(row =0, column = 1, sticky = N+S+E+W)
            
            self.f_vaisseaux.grid(row=1, column=1, sticky = N+S+E+W)
            
            self.f_vaisseaux.rowconfigure(0, weight = 1)
            self.f_vaisseaux.rowconfigure(1, weight = 1)
            self.f_vaisseaux.rowconfigure(2, weight = 2)
            self.f_vaisseaux.rowconfigure(3, weight = 2)
            self.f_vaisseaux.rowconfigure(4, weight = 2)
            self.f_vaisseaux.rowconfigure(5, weight = 2)
            self.f_vaisseaux.rowconfigure(6, weight = 2)
            self.f_vaisseaux.rowconfigure(7, weight = 2)   
            self.f_vaisseaux.columnconfigure(0, weight = 2)
            self.f_vaisseaux.columnconfigure(1, weight = 3)
            self.f_vaisseaux.columnconfigure(2, weight = 3)
            ##############################################################################
            listbox_artefacts = Listbox(self.f_vaisseaux)
            listbox_artefacts.grid(row =1, column=0, sticky = N+S+E+W)
            listbox_artefacts.insert(END, "Art�facts d�tenus")
            for i in self.listeNomJoueurs:
                for j in self.modele.joueurs[i].possessions["planetesColonisees"]:
                    for k in j.batisses:
                        if k.type == "Temple":
                            for z in k.artefactsDetenus:
                                listbox_artefacts.insert(END, z) 
        elif evt.widget.cget("text") == "Colisee":
            #===================================================================
            # extraaaaaaaaa
            #===================================================================
            pass     
  
            
    def selCanevasPlanete(self, evt): 
        pass
      
    def selCanevasUnivers(self, evt): 
          
        lesTags = self.c_Univers.gettags(CURRENT) 
        #tag 0 = "Galaxie" 
        #tag 1 = id Galaxie 
        #tag 2 = nom du joueur � qui la galaxie appartient 
        if evt.num == 1: 
            if lesTags: 
                print(lesTags[0], "/", lesTags[1], "/", lesTags[2])
                if lesTags[0] == "Galaxie": 
                    self.etatSysteme = lesTags[2]
                    self.modele.joueurs[self.nomJoueurCourant].sysSolaireVue = lesTags[1]
                    self.drawSystemeSolaire(evt) 

          
        # MAB - Dessin des lasers (lignes) 
    def drawLasers (self, id, departX, departY, targetX, targetY, attackColor): 
        # Ligne 
        self.c_SystSol.create_line( departX, departY, targetX, targetY, fill = attackColor, tags = ("attaqueLaser", id )) 
          
    # MAB - Dessin des missiles (losanges avec une croix) 
    def drawMissile (self, id, x, y, attackColor):  
        mSize = 5
        # Losange 
        self.c_SystSol.create_line(x+mSize, y, x, y-mSize, fill= attackColor, tags = ("attaqueMissile", id , "ligne1")) 
        self.c_SystSol.create_line(x, y-mSize, x-mSize, y, fill= attackColor, tags = ("attaqueMissile", id , "ligne2")) 
        self.c_SystSol.create_line(x-mSize, y, x, y+mSize, fill= attackColor, tags = ("attaqueMissile", id , "ligne3")) 
        self.c_SystSol.create_line(x, y+mSize, x+mSize, y, fill= attackColor, tags = ("attaqueMissile", id , "ligne4"))  
        # Croix 
        self.c_SystSol.create_line(x-mSize+2, y, x+mSize-2, y, fill= attackColor, tags = ("attaqueMissile", id , "ligne3")) 
        self.c_SystSol.create_line(x, y+mSize-2, x, y-mSize+2, fill= attackColor, tags = ("attaqueMissile", id , "ligne4"))  
   
    # MAB - Dessin des bombes (triangles avec cercle d'explosion) 
    def drawBombe (self, bombe): 
        bSize = 3
        # Triangle �quilat�ral 
        self.c_SystSol.create_polygon(bombe.x-bSize, bombe.y-bSize, bombe.x, bombe.y+bSize, bombe.x+bSize, bombe.y-bSize, fill= bombe.attackColor, tags = ("attaqueBombe",)) 
        self.c_SystSol.create_oval( bombe.x - bombe.rangeVisuel, bombe.y + bombe.rangeVisuel, bombe.x + bombe.rangeVisuel , bombe.y - bombe.rangeVisuel, fill = "", outline= bombe.attackColor, tags = ("attaqueBombeRange",)) 
             
  
    def boutonAutoDestr (self, evt): 
        pass
    
    def reponse(self, evt):
        
        self.nomBatSel = "Station d'extraction"
        self.tl_StationExtraction = Toplevel()
        b_Gaz = Button(self.tl_StationExtraction, text = "Gaz")
        b_Minerai = Button(self.tl_StationExtraction, text = "Minerai")
        l_extraction = Label (self.tl_StationExtraction, text = "Que voulez-vous extraire avec cette station?")
        b_Gaz.bind("<Button-1>", self.reponse1)
        b_Minerai.bind("<Button-1>", self.reponse1)
        l_extraction.pack()
        b_Gaz.pack()
        b_Minerai.pack()
        
    def reponse1 (self, evt):    
        if evt.widget.cget("text") == "Gaz":
            self.ressource = "gas"
        elif evt.widget.cget("text") == "Minerai":
            self.ressource = "minerai"
        self.tl_StationExtraction.destroy()
        
      
    def constructionBatisse (self, evt): 
        if self.nomBatSel == "Station d'extraction": 
            self.cptExt = self.cptExt+1
            if self.cptExt ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("StationExtraction"):
                self.parent.parent.construireBatisse(self.idPlSel, "StationExtraction", self.ressource)    
          
        elif self.nomBatSel == "Port Spatial": 
            self.cptSpa = self.cptSpa+1
            if self.cptSpa ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("PortSpaciale"):
                self.parent.parent.construireBatisse(self.idPlSel, "PortSpaciale", self.ressource)   

  
        elif self.nomBatSel == "Station �nergie": 
            self.cptEne = self.cptEne+1
            if self.cptEne ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("StationEnergie"):
                self.parent.parent.construireBatisse(self.idPlSel, "StationEnergie", self.ressource)     
          
        elif self.nomBatSel == "Satellite d�fense": 
            self.cptDef = self.cptDef+1
            if self.cptDef ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("SatelliteDefense"):
                self.parent.parent.construireBatisse(self.idPlSel, "SatelliteDefense", self.ressource)     
                 
        elif self.nomBatSel == "Laboratoire": 
            self.cptLab = self.cptLab+1
            if self.cptLab ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("Laboratoire"):
                self.parent.parent.construireBatisse(self.idPlSel, "Laboratoire", self.ressource)     
                
        elif self.nomBatSel == "Temple": 
            self.cptTem = self.cptTem+1
            if self.cptTem ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("Temple"):
                self.parent.parent.construireBatisse(self.idPlSel, "Temple", self.ressource)     
                 
        elif self.nomBatSel == "Colisee": 
            self.cptCol = self.cptCol+1
            if self.cptCol ==1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("Colisee"):
                self.parent.parent.construireBatisse(self.idPlSel, "Colisee", self.ressource)     
                
        elif self.nomBatSel == "Radar": 
            self.cptRad = self.cptRad+1
            if self.cptRad == 1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("Radar"):
                self.parent.parent.construireBatisse(self.idPlSel, "Radar", self.ressource)     
                
        elif self.nomBatSel == "G�n�rateur de champ de force": 
            self.cptGen = self.cptGen+1
            if self.cptGen == 1: 
                self.cptBout = self.cptBout+1
            if self.modele.ressourceDisponible("GenerateurChampForce"):
                self.parent.parent.construireBatisse(self.idPlSel, "GenerateurChampForce", self.ressource)  
    
          
      
    def quitterPartie (self, evt):
        self.parent.intercepteFermeture()
      
    def updateVue (self):
        
        self.timer = self.timer+1 
        self.updateLabel() 
        self.updateListeMessages(self.modele.messageJoueurs) # Chat
        #*********************************** 
        #  ONGLET SYSTEME SOLAIRE 
        #*********************************** 
        if self.courant == "systeme":  
                   
            self.c_SystSol.delete("planete")  
            self.c_SystSol.delete("vieVaisseau") 
            self.c_SystSol.delete("vaisseau")  
            self.c_SystSol.delete("laserBeams")     
            """                      
            #******Redessiner les plan�tes******#         
            for i in self.parent.parent.modele.listeSystemesSolaires: 
                n = 1
                for j in i.planetes: 
                    if n == 1: 
                        self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, True, "yellow") 
                        n = n+1
                    else: 
                        self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, False, "white") 
                self.drawGates(i.gates) 
                  
            for i in self.listeNomJoueurs: 
            #******Redessiner les vaisseaux******#      
                for j in self.modele.joueurs[i].possessions["vaisseau"]: 
                    self.drawVaisseaux(j, i, self.modele.joueurs[i].couleur, j.type) 
            #******Redessiner les attaques******#          
                for k in self.modele.joueurs[i].listeAttaquesFaites :  
                    #******Attaque Laser******#         
                    if k.attackType == "laser" : 
                        self.drawLasers ( k.vaisseauAttaquantID, k.departX, k.departY, k.targetX , k.targetY, k.attackColor )   
                        self.modele.joueurs[k].listeAttaquesFaites.remove(k) # Enlever le laser de la liste, car dans son cas, il est temporaire 
           """
           #�a prend une boucle for avec la table des plan�tes   
        #=======================================================================
        #     tagG = self.c_Univers.gettags(CURRENT)
        #     idSys = 0
        # 
        #     if tagG:
        #         if tagG[0] == "Galaxie":
        #             self.etatSysteme = tagG[2]
        #             idSys = tagG[1]
        #     else:
        #         self.etatSysteme = self.nomJoueurCourant
        #=======================================================================
            idSys = 0
            
              
            for i in self.modele.listeSystemesSolaires: 
                n = 1
                if i.proprio == self.etatSysteme:
                    for j in i.planetes:
                        if n == 1: 
                            self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, True, "yellow" ) 
                            n = n+1
                        else: 
                            self.drawPlanetes(j.x, j.y,j.id, j.proprio, j.explore, False, "white")
                        self.drawGates(i.gates) 
            for i in self.listeNomJoueurs:
                if idSys==0:
                    if i == self.etatSysteme:
                        idSys = self.modele.joueurs[i].systemeSolaire.id
                for j in self.modele.joueurs[i].possessions["vaisseau"]:
                    if j.systemeSolaire.id == idSys:
                        self.drawVaisseaux(j, i, self.modele.joueurs[i].couleur, j.type)
            #******Redessiner les attaques******#          
                for k in self.modele.joueurs[i].listeAttaquesFaites :  
                    #******Attaque Laser******#         
                    if k.attackType == "laser" : 
                        self.drawLasers ( k.vaisseauAttaquantID, k.departX, k.departY, k.targetX , k.targetY, k.attackColor )   
                        self.modele.joueurs[k].listeAttaquesFaites.remove(k)     
                    if k.attackType == "missile":
                        self.drawMissile ( k.vaisseauAttaquantID, k.x, k.y, k.attackColor)
                    if k.attackType == "bombe":     
                        self.drawBombe (k)
         
        #*********************************** 
        #  ONGLET PLANETE 
        #*********************************** 
        elif self.courant == "planete": 
            self.afficherLeFrame()
            if self.frameAfficher:
                self.drawCategories()      
      
    def updateLabel(self): 
  
        #Aller chercher les infos du joueur courant 
  
        for i in self.listeNomJoueurs: 
            if self.modele.joueurs[i].nom == self.parent.parent.nom: 
                self.nVieCap = self.modele.joueurs[i].possessions["planetesColonisees"][0].hp 
                self.population = len(self.modele.joueurs[i].joueurVassal)                                                         #population 
                self.nArtefacts = len(self.modele.joueurs[i].artefactsDecouverts) 
                self.minerai = self.modele.joueurs[i].minerai 
                self.moral = self.modele.joueurs[i].moral
                self.couleurJoueurCourant = self.modele.joueurs[i].couleur 
                
        ######################################################################
        self.label_PtsDeVie.config(text = str(self.nVieCap))
        self.label_pop.config(text = str(self.population))
        self.label_ressources.config(text = str(self.minerai))
        self.label_moral.config(text = str(self.moral))
        self.label_nbArtefacts.config(text = str(self.nArtefacts))
        
             
        
        if (self.couleurJoueurCourant == "red"):
            self.couleurCanvasJoueur = "red"
        elif(self.couleurJoueurCourant == "blue"):
            self.couleurCanvasJoueur = "blue"
        elif(self.couleurJoueurCourant == "green"):
            self.couleurCanvasJoueur = "green"
        elif(self.couleurJoueurCourant == "yellow"):
            self.couleurCanvasJoueur = "yellow"
        elif(self.couleurJoueurCourant == "orange"):
            self.couleurCanvasJoueur = "orange"
        elif(self.couleurJoueurCourant == "purple"):
            self.couleurCanvasJoueur = "magenta"
        elif(self.couleurJoueurCourant == "pink"):
            self.couleurCanvasJoueur = "pink"
        elif(self.couleurJoueurCourant == "cyan"):
            self.couleurCanvasJoueur = "cyan"
        elif(self.couleurJoueurCourant == "brown"):
            self.couleurCanvasJoueur = "brown"
            
        self.label_NomJoueur = Label (self.f_overview, text = self.modele.joueurs[i].nom, font = '"Times" 18', fg = "white", bg = "black")
        self.label_NomJoueur.grid(row=0, column=0, sticky=W)
        ##################################################################################################    
        self.canvasCouleur = Canvas(self.f_overview, bg = self.couleurCanvasJoueur, width = 20, height = 20)
        self.canvasCouleur.grid(row=0, column=1)
        ###################################################################################################
        self.label_NomJoueur.config(fg=self.couleurCanvasJoueur)
        
        
    # Chat    
    def updateListeMessages (self, msgJoueurs):
        if self.listBox_messageJoueurs.size() < len(msgJoueurs):
            print("Je change")
            self.listBox_messageJoueurs.delete(0,'end') 
            for i in msgJoueurs:
                self.listBox_messageJoueurs.insert('end', i)
            self.champMessageJoueur.delete(0,'end')
            self.listBox_messageJoueurs.select_set('end')
            self.listBox_messageJoueurs.yview('end') 