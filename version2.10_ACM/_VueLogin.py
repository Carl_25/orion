# -*- encoding: ISO-8859-1 -*-

from tkinter import *
from tkinter import ttk
import random
#from tkinter.ttk import *
#import tkinter.ttk as Ttk



class VueLogin():

    def __init__(self, parent, serveurList, monip):
        self.parent = parent
        #JF changer pour faire reference au root de la VueControleur
        #self.root = Tk()
        self.root= self.parent.root
        self.root.title("Orion")
        self.largeur = self.root.winfo_screenwidth()-400
        self.hauteur = self.root.winfo_screenheight()-400
        self.root.geometry("%dx%d+0+0" % (self.largeur, self.hauteur))
        self.root.resizable(0, 0)
        self.cadreActif=0
        self.canevasCourant=None
        self.serveurList=serveurList
        self.monip = monip
        self.leServeur=None
        #JF � revoir
        self.nom="testNom" + str(random.randrange(0, 100, 1))
        self.leip=" "
        self.cadreLogin= Frame(self.root)
        #############
        self.creeCadreLogin()
        

    def creeCadreLogin(self):
        #JF tranf�rer dans l'init
        #self.cadreLogin= Frame(self.root)
        self.cadreActif = self.cadreLogin
        self.canevas=Canvas(self.cadreLogin, width=100, height=100,bg="blue")
        self.canevas.pack()
        self.root.title('Orion - Login')

        # Bouton cr�er serveur
        self.demarrerPartie=Button(self.cadreLogin, text="D�marrer une partie", command=self.creerServeur)
        self.demarrerPartie.pack(padx=5, pady=10, side=LEFT)
        self.lf = LabelFrame(self.cadreLogin, text="Choix d'un serveur", padx=10, pady=5, relief=GROOVE)
        self.lbServIP = Listbox(self.lf, height=5, selectmode=SINGLE)
        self.lbServIP.pack(side=LEFT, fill=BOTH, expand=1)
        self.s = Scrollbar(self.lf, orient=VERTICAL, command=self.lbServIP.yview)
        self.s.pack(side=RIGHT, fill=Y)
        self.lbServIP['yscrollcommand'] = self.s.set
        self.lf.pack(fill=BOTH, expand=YES, side=LEFT, padx=10, pady=10)
        self.lbServIP.bind('<<ListboxSelect>>', self.on_select)

        # Remplir le ListBox
        for i in self.serveurList:
            self.lbServIP.insert('end',  i)
        #self.cadreLogin.pack()

    def on_select(self, event):#on se connectea un server
        self.parent.serverLocal=False
        iServ = event.widget.curselection()[0]
        Serv = self.lbServIP.get(iServ)
        self.leServeur = Serv
        #JF � revoir
        self.leip = Serv
        ############
        self.connecterServeur(Serv)

    def creerServeur(self):#on est le server
        # Changer pour un nom random 
        self.parent.serverLocal=True
        #self.nom=("testNom" + str(random.randrange(0, 100, 1)))
        ip=self.monip
        self.leServeur = ip
        self.parent.creerServeur()
        self.parent.creerLobby(self.monip)


    def connecterServeur(self, ip):
        #nom="testNom" + str(random.randrange(0, 100, 1))#cree un nom
        if self.nom:
            self.parent.inscritClient(ip)
            self.parent.creerLobby(ip)

