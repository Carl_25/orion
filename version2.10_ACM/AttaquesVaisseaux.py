# -*- encoding: ISO-8859-1 -*-

from helper import *

class Laser(object):
    def __init__(self, parent, targetX, targetY):
        self.parent = parent
        self.vaisseauAttaquantID = parent.id
        self.departX = parent.x
        self.departY = parent.y
        self.targetX = targetX
        self.targetY = targetY
        self.attackColor = parent.parent.couleur
        self.attackType = "laser"
        
    def prochaineAction(self):
        pass
        
class Missile(object):
    def __init__(self, parent, target):
        self.parent = parent
        self.joueur = parent.parent
        self.vaisseauAttaquantID = parent.id
        self.x = parent.x
        self.y = parent.y
        self.attackColor = parent.parent.couleur
        self.attackType = "missile"
        self.puissance = parent.puissance
        self.vitesse = 15
        self.target = target
        self.targetType = target.tag
        self.cible=[]
        self.angle=0
        self.setCible()
        
    def prochaineAction(self):
        self.deplacement()
        
    def setCible(self):
        if self.target != None:
            self.cible=[self.target.x,self.target.y]
            self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
        else:
            self.joueur.detruireMissile(self)
        
    def deplacement (self):
        self.setCible()
        if self.cible:
            #calcul de l'angle de la direction
            x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
            self.x=x
            self.y=y
            d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            #si arrive a destination.
            if d<self.vitesse:
                if self.targetType == "vaisseau":
                    self.target.vie -= self.puissance
                    self.joueur.diplomatie -= 1
                    self.joueur.detruireMissile(self)
                else:
                    self.target.dommage += self.puissance
                    self.joueur.diplomatie -= 1
                    self.joueur.detruireMissile(self)
                    
class Bombe(object):
    def __init__(self, parent, targetX, targetY):
        self.parent = parent
        self.systemeSolaire = parent.galaxy
        self.joueur = parent.parent
        self.attackColor = parent.parent.couleur
        self.attackType = "bombe"
        self.range = 50
        self.vitesse = 25
        self.x = parent.x
        self.y = parent.y
        self.rangeVisuel = 0
        self.valideExplosion = False
        self.valideAttaque = True
        self.systemeSolaire = parent.galaxy
        self.puissance = parent.puissance
        self.targetsAAttaquer = []
        self.targetX = targetX
        self.targetY = targetY
        self.cible=[]
        self.angle=0
        self.changeCible(self.targetX, self.targetY)
        
    def prochaineAction(self):
        self.deplacement()
        
    def attaqueVisuel (self):
        if (valideExplosion):
            if (self.rangeVisuel != 60):
                self.rangeVisuel += 10
            else:
                self.joueur.detruireMissile(self)
        
    def changeCible(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
        
    def deplacement (self):
        if self.cible:
            #calcul de l'angle de la direction
            x,y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
            self.x=x
            self.y=y
            d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            #si arrive a destination.
            if d<self.vitesse:
                listeNomJoueurs = self.joueur.parent.joueurs.keys()
                for i in listeNomJoueurs:
                    #Traverse la liste de tous les joueurs
                    j = self.joueur.parent.joueurs[i]
                    #Parcourir la liste des vaisseaux des autres joueurs
                    for k in j.possessions["vaisseau"]:
                        #Si le vaisseau est dans la meme galaxy
                        if (k.galaxy == self.systemeSolaire):
                            #Si le vaisseau est dans le range
                            self.calculerDistance (k, "vaisseau")
                            
                for i in self.joueur.parent.systemeSolaires:
                    if i.nom == self.systemeSolaire:
                        for j in i.planetes:
                            calculerDistance(j, "planete")
                
                
                for i in self.targetsAAttaquer:
                    self.joueur.diplomatie -= 1
                    if i[1] == "vaisseau":
                        if i[0] != None:
                            i[0].vie -= self.puissance
                    elif i[1] == "planete":
                        if i[0] != None:
                            i[0].dommage += self.puissance
                    
                self.joueur.detruireMissile(self)
                    
    def calculerDistance (self, objetAtteint, typeObjet):
        #Calcule distance entre les deux vaisseau
        distance = math.sqrt((self.x - objetAtteint.x)**2 + (self.y - objetAtteint.y)**2)
        
        #Compare la distance et le range
        if (distance <= self.range):
            objetDansExplosion = []
            objetDansExplosion.append(objetAtteint)
            objetDansExplosion.append(typeObjet)
            self.targetsAAttaquer.append(objetDansExplosion)










