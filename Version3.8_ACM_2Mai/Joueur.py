import random
from Vaisseau import *
from Batisses import *
import dictVaisseaux
import dictBatisses
       
class Joueur(object):
    def __init__(self,parent,nom,e,p,couleur):
        self.parent=parent
        self.nom=nom
        self.etoileMere=e
        self.planeteMere=p
        self.couleur=couleur
        #self.listePlaneteColoniser = []
        self.etoilesVisites=[e]
        self.artefactsDecouverts=[]
        self.flottes=[]
        self.listeAllie = [] #on a pas tirer dessus
        self.listeJoueursRencontres = []
        self.joueurVassal = [] #soumis
        self.listeEnnemi = []
        self.listeAttaquesFaites = []
        self.possessions={"station":[],#On va garder ca, c'est une bonne idee
                       "vaisseau":[VaisseauMissile(self,p)],
                       "planetesColonisees":[p],
                       "extracteur":[],
                       "balise":[],
                       "planetes":[],
                       "Gates":[]}
        
        self.actions={"bougerVaisseau":self.bougerVaisseau,
                      "attaquerSpecifique":self.attaquerSpecifique,
                      "poursuivreObjet":self.poursuivreObjet,
                      "reparerBatisse":self.reparerBatisse,
                      "sell":self.sell,
                      "shutDown":self.shutDown,
                      "construireBatisse":self.construireBatisse}
        
        self.id = ""
        self.nom = nom
        self.gas = 0
        self.minerai = 0
        self.energie = 0
        self.couleur = couleur
        self.listePlaneteExplorer = []
        self.race = ""
        self.moral = 0
        self.diplomatie = 0
        self.listeArtefacts = []
        self.paramConstructionBatisse = []
        self.compteurConstruciton = []
        self.Batisses ={"Capitale":Capitale,
                           "StationExtraction":StationExtraction,
                           "StationEnergie":StationEnergie,
                           "PortSpaciale":PortSpaciale,
                           "SatelliteDefense":SatelliteDefense,
                           "Radar":Radar,
                           "GenerateurChampForce":GenerateurChampForce,
                           "Laboratoire":Laboratoire,
                           "Temple":Temple,
                           "Colisee":Colisee}
        
    def construireBatisse (self,par):#quand c'est une station d'extraction, choisir le type
        b ={"Capitale":dictBatisses.Capitale,#besoin d'un diciton des classes des params, sinon ca marche pas
                           "StationExtraction":dictBatisses.StationExtraction,
                           "StationEnergie":dictBatisses.StationEnergie,
                           "PortSpaciale":dictBatisses.PortSpaciale,
                           "SatelliteDefense":dictBatisses.SatelliteDefense,
                           "Radar":dictBatisses.Radar,
                           "GenerateurChampForce":dictBatisses.GenerateurChampForce,
                           "Laboratoire":dictBatisses.Laboratoire,
                           "Temple":dictBatisses.Temple,
                           "Colisee":dictBatisses.Colisee}
        #ici on fait juste initialiser des params pour le joueur
        self.paramConstructionBatisse.append (par)
        self.compteurConstruciton.append(b[par[1]]["tempsConstrucion"])#va chercher le temps de construction ici
        self.minerai-= b[par[1]]["coutMinerai"]
        self.gas-= b[par[1]]["coutGas"]
        
    def reparerBatisse(self,par):
        pass
    
    def sell(self,par):
        pass    
    
    def shutDown(self,par):
        pass
        
    def vendreVaisseau (self, vaisseau):
        for i in range (0, self.listePlaneteColoniser.size()):
            if ( math.sqrt((self.listePlaneteColoniser[i].x - i.x)**2 + (self.listePlaneteColoniser[i].y - i.y)**2) ):
                break  
                       
    def bougerVaisseau(self,par):
        id,x,y=par
        for i in self.possessions["vaisseau"]:
            if int(id) == i.id:
                i.changeCible(x,y)
                
    def poursuivreObjet(self,par):
        idSuiveur, idSuivi = par
        objetSuivi = None
        typeObjetSuivi = None
        # Aller chercher le vaisseau (obj) a poursuivre
        for i in self.parent.joueurs:
            for j in self.parent.joueurs[i].possessions["vaisseau"]:
                 if int(idSuivi) == j.id:
                     objetSuivi = j
                     typeObjetSuivi = "vaisseau"
                     break
                 
        # Aller chercher la planete (obj) a poursuivre
        for i in self.parent.listeSystemesSolaires:
            for j in i.planetes:
                if int(idSuivi) == j.id:
                    objetSuivi = j
                    typeObjetSuivi = "planete"
                    break
                     
        for i in self.possessions["vaisseau"]:
             if int(idSuiveur) == i.id:
                 i.deplacementVersAutreVaisseau( objetSuivi, typeObjetSuivi )
                
    def attaquerSpecifique(self,par):
        idVaisseauEnnemi, idVaisseauAttaquant = par
        for i in self.possession["vaisseau"]:
            if int(idVaisseauAttaquant) == i.id:
                i.attaquerSpecifique(idVaisseauEnnemi)
                
    def prochaineAction(self):
        #print("NO ",self.parent.parent.cadre)
        print("gas",self.gas, "minerai", self.minerai)
        for i in self.possessions.keys():
            for j in self.possessions[i]:
                j.prochaineAction()
                
        for i in self.listeAttaquesFaites:
            i.prochaineAction()
        
        self.timerConstructionBatisse()
    
    def timerConstructionBatisse (self):
        print('le compeur!', self.compteurConstruciton)
        if len(self.compteurConstruciton)>0:
            n=0
            for i in self.compteurConstruciton:
                
                if i == 0:
                    idPlanete,batisse,paramBatisse = self.paramConstructionBatisse[n]#type du batiment
                    self.compteurConstruciton.remove(i)
                    for i in self.possessions["planetesColonisees"]:
                        if i.id == int(idPlanete):
                            i.batisses.append(self.Batisses[batisse](self,i,paramBatisse))
                else:
                    self.compteurConstruciton[n]-=1
                n+=1
            
    def detruireVaisseau(self, vaisseau):
        self.possessions["vaisseau"].remove(vaisseau)
    
    def detruireMissile(self, missile):
        self.listeAttaquesFaites.remove(missile)
