# -*- encoding: ISO-8859-1 -*-
import random
from Joueur import *
from SystemeSolaire import *
import dictVaisseaux
import dictBatisses
from Batisses import *
                        
class Modele(object):
    
    def __init__(self,parent):
        self.parent=parent#controleur
        self.paramPartie={"x_espace":200,
                          "y_espace":200,
                          "systemeSolaires":100,
                          "max_planetes":10,
                          "min_minerai":10000,
                          "max_minerai":10000,
                          "min_energie":10000,
                          "max_energie":10000,
                          }
        self.rdseed=0 # ATTENTION ! Ã§a semble marcher cependant
        self.joueurs={}
        self.listeJoueurs = []#utile?
        self.vaisseau=""
        self.vaisseaux=[]
        self.actions=[]
        self.actionsAFaire={}#actions a faire avec le cadre comme clef
        self.systemeSolaires=[]
        self.ids = 1
        self.unID=self.parent.uniqueId
        #########ACM
        self.largeur = 2000
        self.hauteur = 2000
        self.listeSystemesSolaires = []
        #########ACM
            
    def initPartie(self,listeNomsJoueurs):
        random.seed(self.rdseed) # initialisation du rdseed client
        self.listeJoueurs = listeNomsJoueurs#initialise la variable, est-ce utile?
        nbSystemesSolaires = len(listeNomsJoueurs)
        print(listeNomsJoueurs, 'la liste des joueurs et les sys solaires',nbSystemesSolaires)
        #Boucle selon le nombre de systemes solaires a creer
        for j in range (nbSystemesSolaires):
            #Si la liste de systemes solaires est vide
            if (self.listeSystemesSolaires == None):
                #Donne un x et y random
                x = random.randrange(15, self.largeur - 15)
                y = random.randrange(15, self.hauteur - 15)
                #Cree et ajoute un systeme solaire a la liste de systemes solaires
                self.ids = self.unID.nextId(self.ids)
                self.listeSystemesSolaires.append(SystemeSolaire (self,self.ids, x, y))
            #Si la liste des systemes solaires n'est pas vide
            else:
                valide = False
                #Continu a chercher une position tant que le systeme solaire ne
                #se trouve pas a au moins 30 pixels des autres systemes solaires
                while valide == False:
                    x = random.randrange(15, self.largeur - 15)
                    y = random.randrange(15, self.hauteur - 15)
                    valide = self.calculDistance(x, y)
                    if (valide):
                        self.ids = self.unID.nextId(self.ids)
                        systemeSolaire = SystemeSolaire (self,self.ids, x, y)
                        self.listeSystemesSolaires.append(systemeSolaire)
        couleurs=["red","blue","green","yellow","orange","purple"]
        n=0
        for j in listeNomsJoueurs:
            systemeSolaire = self.listeSystemesSolaires[n]
            saPlanete = systemeSolaire.planetes[0]
            saPlanete.proprio = j
            saPlanete.explore = True
            self.joueurs[j]= Joueur(self,j,systemeSolaire,saPlanete,couleurs[n])
            saPlanete.batisses.append(Capitale(self.joueurs[j],saPlanete,""))
            n=n+1
            
    def setRdseed(self, noSeed):
        self.rdseed = noSeed

        
    #===========================================================================
    # def creerVaisseau(self,):
    #     x=random.randrange(self.parent.largeur_espace) # je doute
    #     y=random.randrange(self.parent.hauteur_espace) # je doute
    #     self.actions.append(["creerVaisseau",[self.nom,x,y]])
    #===========================================================================

    def prochaineAction(self,cadre):
        if cadre in self.actionsAFaire:#on regarde on est rendu a quelle cadre, c'est la clef des actions du dictio
            for i in self.actionsAFaire[cadre]:#on ouvre le dictio avec le cadre comme clef
                #0 "nom Joueur", 1 "clef du dictio action du joueur", 2 param
                self.joueurs[i[0]].actions[i[1]](i[2])#onajoute une action au diction, nom : clef, action
            del self.actionsAFaire[cadre]#On supprime l'action a faire 
                
        for i in self.joueurs.keys():#on fait l'action du joueur suivant, c'est une fonction récursive
            self.joueurs[i].prochaineAction()#tous les joueurs font l'action
            
        for i in self.joueurs.keys():#on regarde ce que le joeur a fait
            pass
            #self.joueurs[i].evalueAction()
            
    def calculDistance (self, x, y): 
        for i in self.listeSystemesSolaires: 
            #Calcule distance entre les deux systemes solaires 
            distance = math.sqrt((x - i.x)**2 + (y - i.y)**2) 
            if (distance < 30): 
                return False
        return True
    
    def joueurPossedeVaisseauDiplomate(self,nomJoueur,missionD,qteRessources):
        # parcours la liste de vaisseaux pour voir 
        # s'il y a un vaisseau diplomate
        # pour pouvoir effectuer des missions.
        if nomJoueur not in self.joueurs[self.parent.nom].listeEnnemi and nomJoueur in self.joueurs[self.parent.nom].listeJoueursRencontres:
            for i in joueurs[nomJoueur].possessions["vaisseau"]:
                if i.type == "Diplomate":#ca prend un vaisseau diplomate
                    if missionD == 3:     #si on veut soumettre              
                        if  joueurs[self.parent.nom].diplomatie > 20000:
                            return True
                    elif missionD == 1:# si on veut donner du gas
                        if joueurs[self.parent.nom].diplomatie > 0 and joueurs[nomJoueur].gas > qteRessources :#si on veut juste trader
                            return True
                    elif missionD == 2:# si on veut donner du minerai
                        if joueurs[self.parent.nom].diplomatie > 0 and joueurs[nomJoueur].minerai > qteRessources :#si on veut juste trader
                            return True
            return False

    def ressourceDisponible(self,aconstruire):
        self.dictionnaireChoix = {"VaisseauLaser":dictVaisseau.xVaisseauLaser,
                "VaisseauMissile":dictVaisseaux.VaisseauMissile,
                "VaisseauMitraillette":dictVaisseaux.VaisseauMitraillette,
                "VaisseauCargo":dictVaisseaux.VaisseauCargo,
                "VaisseauCroiseur":dictVaisseaux.VaisseauCroiseur,
                "VaisseauDiplomate":dictVaisseaux.VaisseauDiplomate,
                "VaisseauScout":dictVaisseaux.VaisseauScout,
                "VaisseauFurtif":dictVaisseaux.VaisseauFurtif,
                "VaisseauSniper":dictVaisseaux.VaisseauSniper,
                "VaisseauBombardier":dictVaisseaux.VaisseauBombardier,
                "VaisseauAmiral":dictVaisseaux.VaisseauAmiral,
                "Capitale":dictVaisseaux.VaisseauAmiral,
                "StationExtraction":dictBatisses.StationExtraction,
                "StationEnergie":dictBatisses.StationEnergie,
                "PortSpaciale":dictBatisses.PortSpaciale,
                "SatelliteDefense":dictBatisses.SatelliteDefense,
                "Radar":dictBatisses.Radar,
                "GenerateurChampForce":dictBatisses.GenerateurChampForce,
                "Laboratoire":dictBatisses.Laboratoire,
                "Temple":dictBatisses.Temple,
                "Colisee":dictBatisses.Colisee}

        mineraiD = self.joueurs[self.parent.nom].minerai
        gasD = self.joueurs[self.parent.nom].gas
        #regarde le cout, retour true si on peut constuire le truc
        if self.dictionnaireChoix[aconstruire].coutMinerai <= mineraiD and self.dictionnaireChoix[aconstruire].coutGas <=gasD:
            return True
        else:
            return False

    def construireVaisseauPossible(self,vDemande,idPlanete,idBatisse):
        self.dicVessel= {"VaisseauLaser":1,
                        "VaisseauMissile":2,
                        "VaisseauMitraillette":3,
                        "VaisseauCargo":4,
                        "VaisseauCroiseur":5,
                        "VaisseauDiplomate":6,
                        "VaisseauScout":7,
                        "VaisseauFurtif":8,
                        "VaisseauSniper":9,
                        "VaisseauBombardier":10,
                        "VaisseauAmiral":11}
        # boucle pour trouver si l'upgrade est fait
        if self.dicVessel[vDemande] <=7:
            return True
        
        for i in self.joueurs.keys():
            for j in self.joueur[i].possessions["planetesColonisees"]:
                if int(j.id) == int(idPlanete):
                    for k in j.batisses:
                        if k.type == "Laboratoire": 
                            lab = k
                        if k.type == "PortSpaciale": 
                            port = k
                            
                            if self.dicVessel[vDemande] == 8:
                                if port.niveau>=2 and lab.niveau == 2:
                                    return True
                            elif self.dicVessel[vDemande]==9:
                                if port.niveau>=3 and lab.niveau == 3:
                                    return True
                            elif self.dicVessel[vDemande]==10:
                                if port.niveau>=4 and lab.niveau == 4:
                                    return True
                            elif self.dicVessel[vDemande]==11:
                                if port.niveau>=5 and lab.niveau == 5:
                                    return True
                    



