# -*- encoding: ISO-8859-1 -*-
import random
        
class Planete(object):
    def __init__(self,parent,x,y,t,id):
        self.parent=parent
        self.x=x
        self.y=y
        self.taille=t
        self.id = id
        self.minerai=random.randrange(self.taille)*10000
        self.energie=random.randrange(self.taille)*1000000
        self.artefacts={"batiment":[],
                       "extracteur":[]
                       }
        self.etat="vide"
        self.range=50
        self.dictPositions={}#pas besoin vu qu'on passe les x et y en param?
        self.proprio=""
        self.minerai=5000
        self.explore = False
        self.gas=5000
        self.artefact=0
        self.hp=30
        self.batisses=[]
        self.tag="planete"
        self.boulcierExistant = False
        
    def prochaineAction(self):#appel� par le joueur
        if len(self.batisses) > 0:
            for i in self.batisses:
                i.prochaineAction()
    
    def maxHp(self):
        pass    
    def repair(self):
        pass 
