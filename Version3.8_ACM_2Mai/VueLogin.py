# -*- encoding: ISO-8859-1 -*-

from tkinter import *
from tkinter import ttk
import random
#from tkinter.ttk import *
#import tkinter.ttk as Ttk



class VueLogin():

    def __init__(self, parent, serveurList, monip):
        self.parent = parent
        #JF changer pour faire reference au root de la VueControleur
        #self.root = Tk()
        self.root= self.parent.root
        self.root.title("Orion")
        self.largeur = self.root.winfo_screenwidth()-400
        self.hauteur = self.root.winfo_screenheight()-400
        self.root.geometry("%dx%d+0+0" % (self.largeur, self.hauteur))
        self.root.resizable(0, 0)
        self.cadreActif=0
        self.canevasCourant=None
        self.serveurList=serveurList
        self.monip = monip
        self.leServeur=None
        self.Civ=None
        #JF � revoir
        self.nom="testNom" + str(random.randrange(0, 100, 1))
        self.leip=" "
        self.cadreLogin= Frame(self.root)
        #############
        self.creeCadreLogin()
        
    #MarcAndre - Modification
    def creeCadreLogin(self):
        #JF tranf�rer dans l'init
        #self.cadreLogin= Frame(self.root)
        #liste des civilisations
        self.lCiv    = ('Jedi', 'Space squids', 'Robots')
        self.cadreActif = self.cadreLogin
        self.canevas=Canvas(self.cadreLogin, width=100, height=100,bg="blue")
        self.canevas.pack()
        self.root.title('Orion - Login')

        
        
        #Champ pour entrer le nom du joueur
        self.lf1 = LabelFrame(self.cadreLogin, text="�tape 1. Choix d'une nom (optionnel)", padx=10, pady=5, relief=GROOVE)
        self.labelChampNom = Label( self.lf1, text = "Votre nom de joueur :" )
        self.labelChampNom.pack(padx=5, pady=10, side=LEFT)
        self.champNomJoueur = Entry(self.lf1)
        self.champNomJoueur.pack(padx=5, pady=10, side=LEFT)   
        self.lf1.pack(fill=BOTH, side=LEFT, padx=10, pady=10)     
        
        #combobox des civilisations
        self.lf2 = LabelFrame(self.cadreLogin, text="�tape 2. Choix d'une civilisation", padx=10, pady=5, relief=GROOVE)
        self.lbChoixCiv = Listbox(self.lf2, height=5, width=20, selectmode=SINGLE)
        self.lbChoixCiv.pack(side=LEFT, fill=BOTH, expand=1)
        self.s2 = Scrollbar(self.lf2, orient=VERTICAL, command=self.lbChoixCiv.yview)
        self.s2.pack(side=RIGHT, fill=Y)
        self.lbChoixCiv['yscrollcommand'] = self.s2.set
        self.lf2.pack(fill=BOTH, expand=YES, side=LEFT, padx=10, pady=10)
        self.lbChoixCiv.bind('<<ListboxSelect>>', self.on_select_civilisation)
         
        #combobox des serveurs
        self.lf3 = LabelFrame(self.cadreLogin, text="Se connecter � un serveur", padx=10, pady=5, relief=GROOVE)
        self.lbServIP = Listbox(self.lf3, height=5, selectmode=SINGLE)
        self.lbServIP.pack(side=LEFT, fill=BOTH, expand=1)
        self.s = Scrollbar(self.lf3, orient=VERTICAL, command=self.lbServIP.yview)
        self.s.pack(side=RIGHT, fill=Y)
        self.lbServIP['yscrollcommand'] = self.s.set
        self.lf3.pack(fill=BOTH, expand=YES, side=LEFT, padx=10, pady=10)
        self.lbServIP.bind('<<ListboxSelect>>', self.on_select)
        
        # Bouton cr�er serveur
        self.demarrerPartie=Button(self.cadreLogin, text="D�marrer une partie", command=self.creerServeur, padx = 100, pady = 20)
        self.demarrerPartie.pack(padx=5, pady=10, side=BOTTOM)

        # Remplir le ListBox
        for i in self.serveurList:
            self.lbServIP.insert('end',  i)

        # Remplir le ListBox de civilisations
        for i in self.lCiv:
            self.lbChoixCiv.insert('end', i)

    def on_select(self, event):#on se connecte a un server
        self.parent.serverLocal=False
        iServ = event.widget.curselection()[0]
        Serv = self.lbServIP.get(iServ)
        self.leServeur = Serv
        self.leip = Serv
        print("la selection", Serv)
        if self.Civ:
            self.connecterServeur(Serv)

    def on_select_civilisation(self, event):
        print("Vue on_select_civilisation")
        iCiv = event.widget.curselection()[0]
        Civ = self.lbChoixCiv.get(iCiv)
        print ("Index: " + iCiv)
        print ("Valeur: " + Civ)
        self.Civ = Civ
    

    def creerServeur(self):#on est le server
        # Changer pour un nom random 
        self.parent.serverLocal=True
        #self.nom=("testNom" + str(random.randrange(0, 100, 1)))
        ip=self.monip
        self.leServeur = ip
        if self.Civ:
            self.parent.creerServeur()
            self.parent.creerLobby(self.monip)

    #MarcAndre - Modification
    def connecterServeur(self, ip):
        print("4-Login", ip)
 
        # Un nom personnalis�  (si le joueur en a �crit un)
        # Sinon, nom al�atoire
        if ( len(self.champNomJoueur.get()) > 0 ):
            self.nom = self.champNomJoueur.get()

        if self.nom:
            self.parent.inscritClient(ip)
            #self.parent.creerLobby(ip)

    # MarcAndre - Modification
    def popupCorrigerNom(self, parent, leip, civ):
        print("je rentre 1")
        self.popupCorriger = Toplevel()
        self.parentPopupCorriger = parent
        self.leIpASeRappeler = leip
        self.LaCivASeRappeler = civ
        
        self.f_popup = LabelFrame(self.popupCorriger, text = "Ce nom existe d�j� : Veuillez entrer un nouveau nom SVP")
        self.f_popup.grid()
        self.labelEntrerNom = Label(self.f_popup, text="Nouveau nom :")
        self.labelEntrerNom.grid( row = 0, column = 0 )
        
        self.champNom = Entry( self.f_popup )
        self.champNom.grid( row = 0, column = 1 )
        
        self.boutonOK = Button( self.f_popup , text = "Soumettre")
        self.boutonOK.grid( row = 1, column = 0)
        
        self.champNom.focus_get()
        
        self.boutonOK.bind("<Button-1>", self.envoyerNouvNom )
        print("je sors 1")
        
    #MarcAndre - Modification   
    def envoyerNouvNom(self, evt): 
        nom = self.champNom.get()
        self.parentPopupCorriger.inscritClient( nom, self.leIpASeRappeler, self.LaCivASeRappeler)
        self.popupCorriger.destroy()

        
