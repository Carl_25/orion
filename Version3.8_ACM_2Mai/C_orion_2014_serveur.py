# -*- encoding: ISO-8859-1 -*-
import Pyro4
import os
from threading import Timer
import sys
import socket
from random import randrange

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("gmail.com",80))
monip=s.getsockname()[0]
s.close()

daemon = Pyro4.core.Daemon(host=monip,port=54440) 
#Pyro4.socketutil.setReuseAddr(daemon)

class Client(object):
    
    def __init__(self,nom):
        #print ("S_Client")
        self.nom=nom
        self.cadreCourant=0
        self.cadreEnAttenteMax=0
        self.actionsEnAttentes={}

#Creer liste des joueurs avec leurs parametres
class ClientParam(object):
    def __init__(self,nom, couleur, civ):
        self.nom=nom
        self.couleur = couleur
        self.civ = civ
        self.cadreCourant=0
        self.cadreEnAttenteMax=0
        self.actionsEnAttentes={}
        
class ModeleService(object):
    
    def __init__(self,parent,rdseed):
        #print ("S_ModeleService")
        self.parent=parent
        self.etatJeu=0
        self.rdseed=randrange(0,400)
        self.cadreCourant=0
        self.cadreFutur=5
        self.clients={}
        self.clientsParam={}
        self.cadreDelta={}
        self.paramCouleur={0:"bleu",
                          1:"rouge",
                          2:"vert",
                           3:"orange",
                           4:"jaune",
                           5:"violet",
                           6:"magenta",
                           7:"bleu_azure",
                           8:"brun"
                          }
        
    def creeClient(self,nom, civ):
        #print ("S_ModeleService creeClient")
        if self.etatJeu==0:
            if nom in self.clients.keys():
                return [0,"Erreur de nom"]
            #print("lenght", len(self.clients))
            couleur=self.paramCouleur[len(self.clients)]
            #print("couleur serveur", couleur)
            c=Client(nom)
            self.clientParam (nom, couleur, civ)
            #cP=ClientParam(nom,couleur, civ)
            self.cadreDelta[nom]=0
            self.clients[nom]=c
            #self.clientsParam[nom]=cP
            return [1,"Bienvenue",self.rdseed]
        else:
            return [0,"Partie deja en cours"]

    
    def clientParam (self, nom, couleur, civ):
        self.clientsParam[nom]= [couleur,civ]
    
    def demarrePartie(self):
        #print ("S_ModeleService demarrePartie")
        if self.etatJeu==0:
            self.etatJeu=1
            for i in self.clients:
                self.clients[i].actionsEnAttentes[1]=[["demarrePartie",list(self.clients.keys())]]
            return 1
        else:
            return 0
    
    def faitAction(self,p):
        #print ("S_ModeleService faitAction")
        nom=p[0]
        cadre=p[1]
        if cadre>self.cadreCourant:
            self.cadreCourant=cadre
        if p[2]:
            cadreVise=self.cadreCourant+self.cadreFutur
            for i in self.clients:
                self.clients[i].cadreEnAttentesMax=cadreVise
                if cadreVise in self.clients[i].actionsEnAttentes.keys():
                    self.clients[i].actionsEnAttentes[cadreVise].append(p[2])
                else:
                    self.clients[i].actionsEnAttentes[cadreVise]=[p[2]]
                #print("ACTION ",self.clients[i].nom,self.clients[i].actionsEnAttentes)

        rep=[]
        
        self.cadreDelta[nom]=cadre
        mini=min(list(self.cadreDelta.values()))
        if cadre-3>mini:
            message="attend"
        else:
            message=""
            
        if self.clients[nom].actionsEnAttentes:
            #print("act attente", nom,self.clients[nom].actionsEnAttentes)
            if cadre<min(self.clients[nom].actionsEnAttentes.keys()):
                rep= self.clients[nom].actionsEnAttentes
                self.clients[nom].actionsEnAttentes={}
                rep= [1,message,rep]
            else:
                print("AYOYE") # ici on a un probleme car une action doit se produire dans le pass�
        else:
            #print("lobby attente", list(self.clients.keys()))
            rep= [0,message,list(self.clients.keys())]
        return rep
                
class ControleurServeur(object):
    
    def __init__(self):
        #print ("S_ControleurServeur")
        rand=os.urandom(8)
        self.modele=ModeleService(self,rand)
        
    def inscritClient(self,nom, civ):
        #print ("serveur inscritClient")
        rep=self.modele.creeClient(nom, civ)
        #print ("serveur inscritClient", rep[2], " voici mon rdseed - serveur")
        return rep
    
    def demarrePartie(self):
        #print ("S_ControleurServeur demarrePartie")
        rep=self.modele.demarrePartie()
        return rep

    def getClientParam (self):
        #print ("ListeParam", self.modele.clientsParam)
        reponse = self.modele.clientsParam
        return reponse
    
    def faitAction(self,p):
        #print ("S_ControleurServeur faitAction")
        rep=self.modele.faitAction(p)
        return rep
    
    def quitter(self):
        #print ("S_ControleurServeur quitter")
        t=Timer(1,self.fermer)
        t.start()
        return "ferme"
    
    def jeQuitte(self,nom):
        #print ("S_ControleurServeur jeQuitte")
        del self.modele.clients[nom]
        if not self.modele.clients:
            self.quitter()
        return 1
    
    def fermer(self):
        #print ("S_ControleurServeur fermer")
        daemon.shutdown()
    


controleurServeur=ControleurServeur()
daemon.register(controleurServeur, "controleurServeur")  
 
print("Serveur Pyro actif sous le nom \'controleurServeur\'")
daemon.requestLoop()
