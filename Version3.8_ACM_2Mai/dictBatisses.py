# -*- encoding: ISO-8859-1 -*-
################################################
#  Le petit Larousse du b�timent non illustr�  #
################################################

Capitale = {"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 200}

StationExtraction ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 50}

StationEnergie={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 2000}

PortSpaciale ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 2000}

SatelliteDefense ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000,
        "tempsConstrucion" : 2000}

Radar ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 2000}

GenerateurChampForce ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 2000}

Laboratoire ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 2000}

Temple ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 2000}

Colisee ={"coutMinerai":1000,
        "coutGas" : 1000,
        "coutEnergie" : 10,
        "vie" : 1000,
        "tempsPourUpgrader" :2000 ,
        "tempsConstrucion" : 2000}
