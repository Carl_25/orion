# -*- encoding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.tix as Tix

# -----------------------------------
# V�rifier les sections A_REVOIR
# -----------------------------------

class VueLobby():
    def __init__(self, parent, ipadress):
        # le parent -> Controleur
        self.parent = parent
        self.ipadress = self.parent.ipLocale
        self.ipserveur = ipadress
        
        
        # Changer l'apparence de la fen�tre
        self.parent.root.config(bg = "black")
        self.parent.root.title("Orion - Lobby")
        
        # le frame contenant ma fen�tre
        self.f_Lobby = CadreCustom(self.parent.root)
        self.f_Lobby.grid()

         
        #Mettre une image Logo pour l'ent�te
        self.img_Logo = PhotoImage(file="image/Lobby_Logo.gif")                              # Load image dans un photoImage
        self.l_logoMenu = EtiquetteCustom(self.f_Lobby, image = self.img_Logo)     # Mettre l'image dans le label
        self.l_logoMenu.configure(highlightthickness = 0)                           # Enlever la bordure blanche autour de l'image
        self.l_logoMenu.grid(row = 1, column = 1, columnspan = 2)
        
        # -----------------------------------------------------------------------------------------------------------------
        #Cr�ation de l'interface, c�t� gauche
        self.f_espaceGauche = CadreCustom(self.f_Lobby)
        self.f_espaceGauche.grid(row=1, column=0)
        
        #Cadre et ADRESSE IP du SERVEUR
        self.lf_AdresseServeur = CadreEtiquetteCustom(self.f_espaceGauche , text = "Adresse IP du serveur")
        self.lf_AdresseServeur.grid(row = 0, column = 0)
        # Adresse IP (Label)
        self.l_AdresseIP = EtiquetteCustom(self.lf_AdresseServeur, text = str(self.ipserveur))
        self.l_AdresseIP.grid()

        #Cadre et ADRESSE IP du Joueur
        self.lf_AdresseLocale = CadreEtiquetteCustom(self.f_espaceGauche , text = "Adresse IP du joueur")
        self.lf_AdresseLocale.grid(row = 1, column = 0)
        # Adresse IP (Label)
        self.l_AdresseIPLocale = EtiquetteCustom(self.lf_AdresseLocale, text = str(self.ipadress))
        self.l_AdresseIPLocale.grid()

        
        # -------------------------------------------
        #
        # Test
        #
        #
        # -------------------------------------------
        
            
        #Cadre et INFORMATIONS sur JOUEURSCONNECTES
        self.lf_JoueursConnectes = CadreEtiquetteCustom(self.f_espaceGauche, text = "Joueurs connect�s")
        self.lf_JoueursConnectes.grid(row = 2, column = 0)
        #Affichage des joueurs connect� non dynamique
        self.lbox_JoueurConnect = Listbox(self.lf_JoueursConnectes, height=15, selectmode=SINGLE)
        # Mettre La liste sur la page
        self.lbox_JoueurConnect.grid(row = 2, column = 0)

        # -------------------------------------------

        #JF Essaie Liste dynamique des joueurs connect�
        self.afficheListeJoueurs(self.parent.afficheListeJoueurs())


        # Bouton Pr�t / Lancer la partie
        self.b_demarrer = BoutonCustom(self.f_espaceGauche, text = "D�marrer la partie", width = 30,  command=self.demarrePartie)
        self.afficherBDemarrerSiServeur()
        
    
    # JF demarrer la partie bouton    
    def demarrePartie(self):
        self.parent.demarrePartie()
      
    # Le bouton demarrer est disponible seulement pour le joueur qui a d�marr� le serveur  
    def afficherBDemarrerSiServeur(self):
        etat = self.parent.parent.serveurLocal
        if (etat == 1):
            #print("vueLobby-bouton demarrer", self.parent.serveurLocal)
            self.b_demarrer.grid( row = 3, column = 0 , pady = 20, sticky = W)
 

    def afficheListeJoueurs (self, lj):
        self.lbox_JoueurConnect.delete(0,'end') 
        for i in lj:
            #print("VueLobby-liste des Joueurs", lj) 
            self.lbox_JoueurConnect.insert('end', i)   
                

        
# Les sous-classes pour une interface personnalis�e

class BoutonCustom(Button):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)
        
class EtiquetteCustom(Label):
    def __init__(self,parent, **kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"]=f
        kw["fg"]="white"
        kw["bg"]="black"
        Label.__init__(self,parent,**kw)
        
class CadreCustom(Frame):
    def __init__(self,parent,**kw):
        kw["bg"]="black"
        Frame.__init__(self,parent,**kw)
        
class CadreEtiquetteCustom(LabelFrame):
    def __init__(self,parent,**kw):
        f=Font(family = "Courrier New", size=8, slant="roman", weight="normal")
        kw["font"] = f
        kw["fg"]="white"
        kw["bg"]="black"
        LabelFrame.__init__(self,parent,**kw)

   
     
