# -*- encoding: ISO-8859-1 -*-
import random
from Vaisseau import *
from Batisses import *
import time
import dictVaisseaux
import dictBatisses
       
class Joueur(object):
    def __init__(self,parent,nom,SysSol,p,couleur):
        self.parent=parent
        self.nom=nom
        self.systemeSolaire=SysSol
        self.sysSolaireVue = SysSol.id
        self.planeteMere=p
        self.couleur=couleur
        self.gold = 0#A FAIRE SAUTER
        self.defaite = False
        #self.listePlaneteColoniser = []
        #self.etoilesVisites=[e]
        self.flottes=[]
        self.artefactsDecouverts = []
        self.listeAllie = [] #on a pas tirer dessus
        self.listeJoueursRencontres = []
        self.joueurVassal = [] #soumis
        self.listeEnnemi = []
        self.listeAttaquesFaites = []
        self.systemesSolairesEnvahis = [SysSol]#les sys sols visit�s
        self.possessions={"station":[],#On va garder ca, c'est une bonne idee
                       "vaisseau":[VaisseauMissile(self,p)],
                       "planetesColonisees":[p],
                       "extracteur":[],
                       "balise":[],
                       "planetes":[],
                       "Gates":[],
                       "Artefacts" : []}
        
        self.actions={"bougerVaisseau":self.bougerVaisseau,
                      "attaquerSpecifique":self.attaquerSpecifique,
                      "poursuivreObjet":self.poursuivreObjet,
                      "reparerBatisse":self.reparerBatisse,
                      "sell":self.sell,
                      "shutDown":self.shutDown,
                      "construireBatisse":self.construireBatisse,
                      "queuingVaisseau":self.queuingVaisseau,
                      "msgEnvoye":self.envoyerMsg,
                      "coloniserPlanete":self.coloniserPlanete}
        
        self.id = ""
        self.nom = nom
        self.gas = 10000
        self.minerai = 10000
        self.energie = 10000
        self.couleur = couleur
        self.listePlaneteExplorer = []
        self.race = ""
        self.moral = 0
        self.diplomatie = 0
        self.listeArtefacts = []
        self.paramConstructionBatisse = []
        self.compteurConstruciton = []
        self.Batisses ={"Capitale":Capitale,
                           "StationExtraction":StationExtraction,
                           "StationEnergie":StationEnergie,
                           "PortSpaciale":PortSpaciale,
                           "SatelliteDefense":SatelliteDefense,
                           "Radar":Radar,
                           "GenerateurChampForce":GenerateurChampForce,
                           "Laboratoire":Laboratoire,
                           "Temple":Temple,
                           "Colisee":Colisee}
        
    def construireBatisse (self,par):#quand c'est une station d'extraction, choisir le type
        b ={"Capitale":dictBatisses.Capitale,#besoin d'un diciton des classes des params, sinon ca marche pas
                           "StationExtraction":dictBatisses.StationExtraction,
                           "StationEnergie":dictBatisses.StationEnergie,
                           "PortSpaciale":dictBatisses.PortSpaciale,
                           "SatelliteDefense":dictBatisses.SatelliteDefense,
                           "Radar":dictBatisses.Radar,
                           "GenerateurChampForce":dictBatisses.GenerateurChampForce,
                           "Laboratoire":dictBatisses.Laboratoire,
                           "Temple":dictBatisses.Temple,
                           "Colisee":dictBatisses.Colisee}
        
        #ici on fait juste initialiser des params pour le joueur
        self.paramConstructionBatisse.append (par)
        self.compteurConstruciton.append(b[par[1]]["tempsConstrucion"])#va chercher le temps de construction ici
        self.minerai-= b[par[1]]["coutMinerai"]
        self.gas-= b[par[1]]["coutGas"]
        
    def queuingVaisseau (self, par):
        print (par, 'par vaisseau a faire')
        idPlanete,idBatisse,vaisseau = par
        for i in self.possessions["planetesColonisees"]:
            if int(i.id) == int(idPlanete):
                for j in i.batisses:
                    if j.type == "PortSpaciale":
                        j.queuingVaisseau(vaisseau)
                            
    def reparerBatisse(self,par):
        pass
    
    def sell(self,par):
        pass    
    
    def shutDown(self,par):
        pass
        
    def vendreVaisseau (self, vaisseau):
        for i in range (0, self.listePlaneteColoniser.size()):
            if ( math.sqrt((self.listePlaneteColoniser[i].x - i.x)**2 + (self.listePlaneteColoniser[i].y - i.y)**2) ):
                break  
                       
    def bougerVaisseau(self,par):
        id,x,y=par
        for i in self.possessions["vaisseau"]:
            if int(id) == i.id:
                i.changeCible(x,y)
                
    def poursuivreObjet(self,par):
        idSuiveur, idSuivi = par
        objetSuivi = None
        typeObjetSuivi = None
        # Aller chercher le vaisseau (obj) a poursuivre
        for i in self.parent.joueurs:
            for j in self.parent.joueurs[i].possessions["vaisseau"]:
                 if int(idSuivi) == j.id:
                     objetSuivi = j
                     typeObjetSuivi = "vaisseau"
                     break
                 
        # Aller chercher la planete (obj) a poursuivre
        for i in self.parent.listeSystemesSolaires:
            for j in i.planetes:
                if int(idSuivi) == j.id:
                    objetSuivi = j
                    typeObjetSuivi = "planete"
                    break
                     
        for i in self.possessions["vaisseau"]:
             if int(idSuiveur) == i.id:
                 i.deplacementVersAutreVaisseau( objetSuivi, typeObjetSuivi )
                
    def attaquerSpecifique(self,par):
        idVaisseauEnnemi, idVaisseauAttaquant = par
        for i in self.possession["vaisseau"]:
            if int(idVaisseauAttaquant) == i.id:
                i.attaquerSpecifique(idVaisseauEnnemi)
                
    def prochaineAction(self):
        #print("NO ",self.parent.parent.cadre)
        #print("gas",self.gas, "minerai", self.minerai)
        #print(self.possessions["planetesColonisees"][0].batisses)
        self.conditionsVictoires()
        for i in self.possessions.keys():
            for j in self.possessions[i]:
                j.prochaineAction()
                
        for i in self.listeAttaquesFaites:
            i.prochaineAction()
        
        self.timerConstructionBatisse()
    
    def timerConstructionBatisse (self):
        #print('le compeur!', self.compteurConstruciton)
        if len(self.compteurConstruciton)>0:
            n=0
            for i in self.compteurConstruciton:
                
                if i == 0:
                    idPlanete,batisse,paramBatisse = self.paramConstructionBatisse[n]#type du batiment
                    self.compteurConstruciton.remove(i)
                    for i in self.possessions["planetesColonisees"]:
                        if i.id == int(idPlanete):
                            i.batisses.append(self.Batisses[batisse](self,i,paramBatisse))
                            self.paramConstructionBatisse.pop()
                            
                else:
                    self.compteurConstruciton[n]-=1
                n+=1
    
    def envoyerMsg(self, par):
        message=par[0]
        temps = time.strftime("%H:%M:%S")
        messageParametree = "[" + temps + "] " + self.nom + " : " + message
        self.parent.messageJoueurs.append(messageParametree)
            
    def detruireVaisseau(self, vaisseau):
        self.possessions["vaisseau"].remove(vaisseau)#quand il a moins que 0 pts de vie
    
    def detruireMissile(self, missile):
        self.listeAttaquesFaites.remove(missile)
        
    def conditionsVictoires (self):
        #Quitte si perdu
        if (self.defaite == True):
            self.parent.parent.jeQuitte()
                
        #Gagne si tous les ennemis sont sous ton controle
        listeJoueurActif = self.chercherJoueursActifs()
        joueursNonControler = self.calculJoueursNonControler(listeJoueurActif)
        if (len(joueursNonControler) == 0 and self.defaite == False):
            listeNomJoueurs = self.parent.joueurs.keys()
            for i in listeNomJoueurs:
                #Traverse la liste de tous les joueurs
                j = self.joueur.parent.joueurs[i]
                if (j.nom != self.nom):
                    j.defaite = True
                    
                    
        #Gagne si le niveau de production est assez �lev�
        nombreBatiments = self.calculNbBatiments()
        if (nombreBatiments >= 200 and self.defaite == False):
            listeNomJoueurs = self.parent.joueurs.keys()
            for i in listeNomJoueurs:
                #Traverse la liste de tous les joueurs
                j = self.joueur.parent.joueurs[i]
                if (j.nom != self.nom):
                    j.defaite = True
        
    def calculNbBatiments (self):
        nombreBatiments = 0
        for i in self.possessions["planetesColonisees"]:
            for j in i.artefacts["batiment"]:
                nombreBatiments += 1
        return nombreBatiments
        
    def chercherJoueursActifs (self):
        listeJoueursActifs = []
        listeNomJoueurs = self.parent.joueurs.keys()
        for i in listeNomJoueurs:
            #Traverse la liste de tous les joueurs
            j = self.parent.joueurs[i]
            if (j.defaite == False):
                listeJoueursActifs.append(j)
        return listeJoueursActifs
    
    def calculJoueursNonControler (self, listeJoueurActif):
        joueursNonControler = []
        for i in listeJoueurActif:
            if (i.defaite == False):
                valide = True
                for j in self.joueurVassal:
                    if (i == j):
                        valide = False
                if valide:
                    joueursNonControler.append(i)
        return joueursNonControler
    
    def coloniserPlanete(self,idPlanete):
        for i in self.parent.listeSystemesSolaires:
            for j in i.planetes:
                if int(j.id) == int(idPlanete):
                    self.possessions["planetesColonisees"].append(j)
