# -*- encoding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.simpledialog as sd
import tkinter.messagebox as mb
import random
import time
from VueLogin import *
from VueLobby import *
from VuePartie import *


class Vue(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent):
        #print("Vue init")
        self.parent=parent#Controleur
        self.modele=self.parent.modele      # acces au modele
        self.ipLocale = self.parent.monip   # IP du client local
        self.vueLobbyExiste = 0             # Utile pour affiche dynamiquement la liste des joueurs
#UTILE???
        #####################################################
        #self.con=0                      # ??? Pas utilise ici
        #self.perspective=""             # ??? Pas utilise ici
        #self.perspectives={}            # ??? Pas utilise ici
        #self.perspectiveCourante=None   # ??? Pas utilise ici
        #self.canevasCourant=None        # ??? Pas utilise ici
        #self.selections=[]              # ??? Pas utilise ici
        #self.serveurLocal = 0   C'est rendu au controleur 
        ###################################################
        self.root=Tk()
        self.root.protocol('WM_DELETE_WINDOW', self.intercepteFermeture) #pour fermer ad�quatemment l'application
        self.cadreActif=None#cadre affiche
        self.cadreLogin=None#le cadrde c'Est le frame.pack() ou .grid()
        self.cadreLobby=None
        self.cadrePartie=None
        self.vueLobby=None
        self.vuePartie = None #la vue c'Est la classe vue
        self.creerLogin()                   # Creer la vue Login

# LES VUES
    def creerLogin(self):
        #Le frame de guy
        self.vueLogin = VueLogin(self, self.parent.serveurActif(), self.parent.monip)
        self.cadreLogin= self.vueLogin.cadreLogin
        self.afficherCardre(self.cadreLogin)
        
    def creerLobby(self, adresseIp):
        self.vueLobby = VueLobby(self, adresseIp)
        self.vueLobbyExiste = 1
        self.serveurLocal=self.parent.serveurLocal
        self.cadreLobby = self.vueLobby.f_Lobby
        self.afficherCardre(self.cadreLobby)
        
    def afficherCardre(self, frame):#ferme le frame affiche et affiche celui passe en param
        if self.cadreActif:
            self.cadreActif.forget()#va falloir arranger ca ici ACM
        self.cadreActif=frame
        frame.pack(fill=BOTH, expand=1)
      
    #MarcAndreBrodeur - Modification    
    # L'HOTE ET CLIENT    
    def creerServeur(self):
        # On prend le nom al�atoire
        nom=self.vueLogin.nom
        
        # S'il y a un nom d'entr�, on prend celui �crit par le cr�ateur du serveur
        if ( len(self.vueLogin.champNomJoueur.get()) > 0 ):
            nom = self.vueLogin.champNomJoueur.get()
        
        leip=self.parent.monip
        if nom:
            pid=self.parent.creerServeur()
            if pid:
                self.root.after(500,self.inscritClient(leip))#apres la creation du serveur, l'hote s'inscrit dans la partie
                
        else:
            mb.showerror(title="Besoin d'un nom",message="Vous devez inscrire un nom pour vous connecter.")

    #MarcAndreBrodeur - Modification
    def inscritClient(self,leip):
        nom=self.vueLogin.nom
        
        # S'il y a un nom d'entr�, on prend celui �crit par le cr�ateur du serveur
        if ( len(self.vueLogin.champNomJoueur.get()) > 0 ):
            nom = self.vueLogin.champNomJoueur.get()
        
        civ=self.vueLogin.Civ #recupere la civilisation choisi dans la vueLogin
        self.parent.inscritClient(nom,leip, civ)

# METHODE PARTIE
    def demarrePartie (self):
        self.vuePartie = VuePartie(self, 1280, 720, self.root)
        self.afficherCardre(self.vuePartie.pw_Planete)
        self.parent.demarrePartie()

    def intercepteFermeture(self):
        #self.parent.jeQuitte()
        self.parent.jeQuitte()
        self.root.destroy()	

# PARAMETRE JOUEUR(S)
    #pour afficher la liste des joueurs dans le lobby
    def afficheListeJoueurs(self):
        return self.parent.getListeJoueurs()
        
    def setNom(self,nom):
        self.parent.setNom(nom)

