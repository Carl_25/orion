import random
from helper import Helper

#Classe univers
class Univers():
    def __init__(self, parent, joueurs):
        self.parent = parent
        self.listeJoueurs = joueurs
        self.largeur = 800
        self.hauteur = 600
        self.nbSystemesSolaires = listeJoueurs.size() * 3
        self.listeSystemesSolaires = []
        
    def creerSystemesSolaires (self):
        #Boucle selon le nombre de systemes solaires � cr�er
        for i in range (self.nbSystemesSolaires):
            #Si la liste de systemes solaires est vide
            if (self.listeSystemesSolaires == None):
                #Donne un x et y random
                x = randrange(15, self.largeur - 15)
                y = randrange(15, self.hauteur - 15)
                #Cree et ajoute un systeme solaire a la liste de systemes solaires
                galaxy = SystemeSolaire (self, x, y)
                self.listeSystemesSolaires.append(galaxy)
            #Si la liste des systemes solaires n'est pas vide
            else:
                valide = False
                #Continu a chercher une position tant que le systeme solaire ne
                #se trouve pas � au moins 30 pixels des autres systemes solaires
                while (valide == False):
                    x = randrange(15, self.largeur - 15)
                    y = randrange(15, self.hauteur - 15)
                    valide = calculDistance(x, y)
                    if (valide):
                        galaxy = SystemeSolaire (self, x, y)
                        self.listeSystemesSolaires.append(galaxy)
    
    def calculDistance (self, x, y):
        for i in self.systemesSolaires:
            #Calcule distance entre les deux systemes solaires
            distance = math.sqrt((x - i.x)**2 + (y - i.y)**2)
            if (distance < 30):
                return False
        return True